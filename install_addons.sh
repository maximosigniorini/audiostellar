#!/bin/bash
PROJECT_DIR=$(dirname "$0")

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi

mkdir -p $PROJECT_DIR/bin/data 
ln -s ../../assets/ $PROJECT_DIR/bin/data/assets

git clone https://github.com/danomatika/ofxMidi $OF_ROOT/addons/ofxMidi
git -C $OF_ROOT/addons/ofxMidi checkout f9d85fd888ba23cf49207b362e2bcc8cfad352ed

git clone https://github.com/macramole/ofxJSON $OF_ROOT/addons/ofxJSON
git -C $OF_ROOT/addons/ofxJSON checkout 5a966ca

git clone https://github.com/genekogan/ofxConvexHull $OF_ROOT/addons/ofxConvexHull
git -C $OF_ROOT/addons/ofxConvexHull checkout 58a1a403e873189c9fb88d9b44988b11bc580821

git clone https://github.com/larsberg/ofxTweener $OF_ROOT/addons/ofxTweener
git -C $OF_ROOT/addons/ofxTweener checkout 0bccf54b3c073f325ecf7065e9ace2f5423cc78d

git clone https://github.com/macramole/ofxImGui $OF_ROOT/addons/ofxImGui
git -C $OF_ROOT/addons/ofxImGui checkout 9dc3e473be78ca480e2f39888630736d4399d7d1

git clone https://github.com/npisanti/ofxAudioFile $OF_ROOT/addons/ofxAudioFile
git -C $OF_ROOT/addons/ofxAudioFile checkout 3b4248d7772379aedbe7088ac239b9d376a7df55

git clone https://github.com/macramole/ofxPDSP $OF_ROOT/addons/ofxPDSP

echo "DONE"
