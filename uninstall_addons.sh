#!/bin/sh
PROJECT_DIR=$(dirname "$0")

if [ -z "${OF_ROOT}" ]; then
  OF_ROOT=$PROJECT_DIR/../../..
fi


rm -rf $PROJECT_DIR/bin/data/assets

rm -rf $OF_ROOT/addons/ofxMidi
rm -rf $OF_ROOT/addons/ofxJSON
rm -rf $OF_ROOT/addons/ofxConvexHull
rm -rf $OF_ROOT/addons/ofxTweener
rm -rf $OF_ROOT/addons/ofxImGui
rm -rf $OF_ROOT/addons/ofxAudioFile
rm -rf $OF_ROOT/addons/ofxPDSP

echo "DONE"
