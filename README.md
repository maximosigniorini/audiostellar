# AudioStellar - http://audiostellar.xyz

Open source data-driven musical instrument for latent sound structure discovery and music experimentation

![logo](http://audiostellar.xyz/img/forLink.jpg)

Visualize a collection of short audio samples in an interactive 2D point map which enables to analyze 
resulting groups and play given samples in a novel way using various innovative music composition modes.


## Downloads

* [Linux AppImage v1.0.0beta](http://audiostellar.xyz/downloads/1.0.0beta/AudioStellar-1.0.0beta.AppImage)
* [MacOS 64 bits v1.0.0beta](http://audiostellar.xyz/downloads/1.0.0beta/AudioStellar-1.0.0beta.dmg)
* [Windows v1.0.0beta](http://audiostellar.xyz/downloads/1.0.0beta/audiostellar-1.0.0beta-win64.zip)


## Machine learning pipeline

![Pipeline](https://gitlab.com/ayrsd/audiostellar/raw/master/data-analysis/proceso.png)

## Community

* [Facebook Group](https://www.facebook.com/groups/3296184277128735)
* Mailing list: audiostellar [at] googlegroups.com. [Subscribe](https://groups.google.com/group/audiostellar/subscribe)

## Contribute

[Buy me a coffee !](https://www.buymeacoffee.com/audiostellar)

#### I'm a musician
* Make music with it
* Make your own sound map
* Give us feedback

#### I'm a programmer
* Fork it
* Hack it
* Browse our issues
* Open new issues
* Make it your own
* We love pull requests

### License

GNU/GPL v3

### How to compile

1. Download [openFrameworks](https://openframeworks.cc/) 0.11.x .
2. Follow [openFramework's install instructions](https://openframeworks.cc/download/) and compile an example.
3. Place this project in apps/myApps. (advanced users can set OF_ROOT enviroment variable to point where you want instead)
4. From a terminal run install_addons.sh. This will download all the addons needed.

#### Linux

5a. Make setWindowIcon public in libs/openFrameworks/app/ofAppGLFWWindow.h or comment lines 12-17 in main.cpp

5b. Just use make from a terminal or use [QTCreator](https://openframeworks.cc/setup/qtcreator/)

#### Mac

5. We are providing an [XCode](https://openframeworks.cc/setup/xcode/) project you can use

#### Windows

5. We are providing a Visual Studio project you can use

#### Finally

At this point AudioStellar should compile and execute properly but you won't be able to create your own sound map. For achieving that :

6. Follow [data-analysis README instructions](https://gitlab.com/ayrsd/audiostellar/tree/master/data-analysis) for compiling python's machine learning process.

## OSC Support

* [OSC documentation](https://gitlab.com/ayrsd/audiostellar/-/blob/units/OSC_Documentation.md)
* Check out our [osc-examples](https://gitlab.com/ayrsd/audiostellar/tree/units/osc_examples)

We are providing examples for:
* Python
* Puredata
* Max
* Touch osc
