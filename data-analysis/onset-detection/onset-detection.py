#!/usr/bin/env python

import librosa
import numpy as np
import scipy.io.wavfile
from pydub import AudioSegment, effects  
import os
import json
import time

"""
Usage examples:

Find onsets in a file:

{"state": "onsetCut",
 "audioFilePath": "/path/to/file.wav",
 "params": {"window_max": 0.03,
            "window_avg": 0.1,
            "delta": 0.07,
            "backtrack": true
            }
}

Slice all files in a directory into onsets:

{"state": "onsetCut",
 "audioFilePath": "/path/to/dir",
 "folder": "allSamples",
 "params": {"window_max": 0.2,
            "window_avg": 0.5,
            "delta": 0.1,
            "backtrack": false
            },
 "save": {"fade": 1000,
          "normalize": true
         }
}

Slice in fixed time an audio file:

{"state": "timeCut",
 "audioFilePath": "/path/to/file.mp3",
 "folder": "500ms_sample",
 "time": 500,
 "save": true
}
"""

def findOnsetsAndCut( window_max, window_avg, delta, backtrack ):
  global onset_frames, onset_times, times
  
  hop_length= 512
  o_env = librosa.onset.onset_strength(segmento, sr=sr)
  times = librosa.times_like(o_env, sr=sr)

  window_max_final = window_max * sr//hop_length
  window_avg_final = window_avg * sr//hop_length

  onset_frames = librosa.onset.onset_detect(onset_envelope=o_env, sr=sr, hop_length= hop_length,
                                            backtrack=True,
                                            delta=delta, 
                                            # wait=0, 
                                            pre_avg= window_avg_final, post_avg= window_avg_final + 1, 
                                            pre_max = window_max_final, post_max = window_max_final + 1)

  onset_times = librosa.frames_to_time(onset_frames, sr)


def loadFile(filename):
  global x, sr, segmento

  target = filename
  if not os.path.isfile(target):
    return
  else:
    x, sr = librosa.load(target, sr=44100)

  segmento = x
def fadeNormalizeSave(folder, fadeSamples = 1000, normalize = True):
  
  segmentoFade = np.copy(segmento)


  for f in range(1,len(onset_frames)):
      sampleFinish = librosa.core.frames_to_samples( onset_frames[f] )

      j = fadeSamples
      for i in np.arange(sampleFinish-fadeSamples, sampleFinish+1):
          segmentoFade[i] *= j/fadeSamples
          j-=1
  
  filePath = os.path.dirname(audioFilePath)
  carpetaFinal = f"{filePath}/{folder}"
  if not os.path.exists(carpetaFinal):
      os.mkdir(carpetaFinal)
  
  for i in range(len(onset_frames)-1):
      timeF = librosa.core.frames_to_samples( onset_frames[i] )
      timeT = librosa.core.frames_to_samples( onset_frames[i+1] )
      filename, ext = os.path.splitext(audioFile)
      archivoAGuardar = f"{carpetaFinal}/{filename}_{i}.wav"

      scipy.io.wavfile.write( archivoAGuardar ,sr, segmentoFade[timeF:timeT])
      if normalize:
          rawsound = AudioSegment.from_wav(archivoAGuardar)  
          normalizedsound = effects.normalize(rawsound)  
          normalizedsound.export(archivoAGuardar, format="wav")

def timeSlice(filePath, time):
    global y, sr, cutSeconds
    y, sr = librosa.load(filePath, sr = 44100)

    duration = librosa.get_duration(y=y, sr=sr)*1000
    timeLine = np.arange(0, duration, 1).tolist()
    timeLine.append(duration)

    cutTimes = timeLine[::time]
    cutSeconds = []
    for e in cutTimes:
        e = e/1000
        cutSeconds.append(e)

def saveTime(folder):
    filePath = os.path.dirname(audioFilePath)
    carpetaFinal = f"{filePath}/{folder}/"
    if not os.path.exists(carpetaFinal):
        os.mkdir(carpetaFinal)

    for i in range(len(cutSeconds)-1):
        timeF = librosa.time_to_samples( cutSeconds[i] )
        timeT = librosa.time_to_samples( cutSeconds[i+1] )
        filename, ext = os.path.splitext(audioFile)
        archivoAGuardar = f"{carpetaFinal}{filename}_{i}.wav"
        scipy.io.wavfile.write( archivoAGuardar ,sr, y[timeF:timeT])

def segment(audioToProcess):
    global state

    if state == "onsetCut":
        loadFile(audioToProcess)
        findOnsetsAndCut(window_max, window_avg, delta, backtrack)
        onsetSamples = librosa.core.frames_to_samples(onset_frames).tolist()
        resultado.append(onsetSamples)
        if save:
            fadeNormalizeSave(folder, fade, normalize)

    elif state == "timeCut":
        timeSlice(audioToProcess, fixedTime)
        timeSamples = librosa.time_to_samples(cutSeconds).tolist()
        resultado.append(timeSamples)
        if save:
            saveTime(folder)


json_file = {
    "state": "waiting..."
}

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

json_name = f"{__location__}/onsets-interprocess.json"


with open( json_name, 'w') as fp:
    json.dump(json_file, fp)

save = False
supportedFileTypes = ["wav", "mp3"]

while True:
    with open(json_name) as jsonFile:
        try:
            data = json.load(jsonFile)
            state = data["state"]
            if state == "onsetCut" or state == "timeCut":
                audioFilePath = data["audioFilePath"]

                if "params" in data:
                    window_max = (data["params"]["window_max"])
                    window_avg = (data["params"]["window_avg"])
                    delta = (data["params"]["delta"])
                    backtrack = data["params"]["backtrack"]
                if "time" in data:
                    fixedTime = data["time"]

                if "save" in data:
                    save = True
                    folder = data["folder"]
                    if state == "onsetCut":
                            fade = (data["save"]["fade"])
                            normalize = data["save"]["normalize"]

                resultado = []
                if os.path.isdir(audioFilePath):
                    if audioFilePath[-1] != "/":
                        audioFilePath += "/"
                    for audioFile in os.listdir(audioFilePath):
                        if not os.path.isdir(audioFile):
                            if os.path.splitext(audioFile)[1][1:].lower() in supportedFileTypes:
                                finalPath = f"{audioFilePath}{audioFile}"
                                segment(finalPath)

                else:
                    audioFile = os.path.basename(audioFilePath)
                    segment(audioFilePath)


                if save:
                    state = "doneAndSave"
                else:
                    state = "done"

                    
                time.sleep(5)
                json_file = {
                    "state": state,
                    "results": resultado
                }
                
            with open(json_name, 'w') as file:
                json.dump(json_file, file)
            if state == "doneAndSave":
                break
        except:
            pass

    time.sleep(1)
