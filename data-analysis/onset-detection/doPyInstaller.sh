#!/bin/bash
rm -R build/ dist/ audiostellar-onset-detection.spec
pyinstaller --onefile --name audiostellar-onset-detection --additional-hooks-dir=. onset-detection.py && 
cp dist/audiostellar-onset-detection ../../bin/ &&
printf "\n\nBinary has been built and copied to ../bin/ successfully.\n\n"
