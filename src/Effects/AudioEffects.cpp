#include "AudioEffects.h"
#include "./Effects/VAFilterUI.h"
#include "./Effects/BasiVerbUI.h"
#include "./Effects/DimensionChorusUI.h"

void AudioEffects::patch()
{
    if ( effects.size() == 0 ) {
        nodeIn0.disconnectOut();
        nodeIn1.disconnectOut();

        nodeIn0 >> nodeOut0;
        nodeIn1 >> nodeOut1;
    } else {
        nodeIn1.disconnectOut();
        nodeIn0.disconnectOut();
        nodeOut0.disconnectIn();
        nodeOut1.disconnectIn();


        nodeIn0 >> effects[0]->channelIn(0);
        nodeIn1 >> effects[0]->channelIn(1);

        for ( unsigned int i = 0 ; i < effects.size() - 1 ; i++ ) {
            effects[i]->disconnectOut();
            effects[i]->channelOut(0) >> effects[i+1]->channelIn(0);
            effects[i]->channelOut(1) >> effects[i+1]->channelIn(1);
        }

        effects[ effects.size()-1 ]->channelOut(0) >> nodeOut0;
        effects[ effects.size()-1 ]->channelOut(1) >> nodeOut1;
    }
}

AudioEffects::AudioEffects() : pdsp::Patchable()
{
    addModuleInput("0", nodeIn0);
    addModuleInput("1", nodeIn1);

    nodeIn0 >> nodeOut0;
    nodeIn1 >> nodeOut1;

    addModuleOutput("0", nodeOut0);
    addModuleOutput("1", nodeOut1);
}

AudioEffects::~AudioEffects()
{
    for ( auto effect : effects ) {
        effect->disconnectAll();
        delete effect;
    }

    nodeIn0.disconnectAll();
    nodeIn1.disconnectAll();
    nodeOut0.disconnectAll();
    nodeOut1.disconnectAll();
}

void AudioEffects::addEffect(EffectUI *newEffect)
{
    effects.push_back(newEffect);

    if ( this->indexPrefix != "" ) {
        setIndexesToEffects(this->indexPrefix);
    }

    AudioEngine::getInstance()->engine.stop();
    patch();
    AudioEngine::getInstance()->engine.start();
}

void AudioEffects::removeEffect(EffectUI *effect)
{
    std::vector<EffectUI *>::iterator it;

    it = find (effects.begin(), effects.end(), effect);
    if (it != effects.end()) {
        effect->disconnectAll();
        effects.erase(it);
        delete effect;
        patch();
    }
}

Json::Value AudioEffects::save()
{
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < effects.size(); i++) {
        EffectUI * effect = effects[i];
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = effect->getName();
        jsonUnit["params"] = effect->save();
        root.append(jsonUnit);
    }
    return root;
}

void AudioEffects::load(Json::Value jsonAudioEffects)
{
    for ( unsigned int i = 0 ; i < jsonAudioEffects.size() ; i++ ) {
        Json::Value jsonEffect = jsonAudioEffects[i];

        if ( jsonEffect["name"].asString() == "VAFilter" ) {
            VAFilterUI * effect = new VAFilterUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "BasiVerb" ) {
            BasiVerbUI * effect = new BasiVerbUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "DimensionChorus" ) {
            DimensionChorusUI * effect = new DimensionChorusUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "Saturator" ) {
            SaturatorUI * effect = new SaturatorUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "Delay" ) {
            DelayUI * effect = new DelayUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else {
            ofLog() << "AudioEffects load: Unknown effect '" << jsonEffect["name"].asString() << "'";
        }
    }
}

void AudioEffects::setIndexesToEffects(string indexPrefix)
{
    this->indexPrefix = indexPrefix;

    for ( unsigned int i = 0 ; i < effects.size() ; i++ ) {
        effects[i]->setIndex(i, indexPrefix);
    }
}

void AudioEffects::drawGui()
{
    int i = 0;
    for ( EffectUI * effect : effects ) {
        ImGui::TextDisabled( "%s",  effect->getName().c_str() );
        ImGui::SameLine();
        string id = "-##" + ofToString(i);
        if ( ImGui::SmallButton(id.c_str()) ) {
            removeEffect(effect);
        } else {
            effect->drawGui();
        }
        ImGui::NewLine();
        i++;
    }
}
