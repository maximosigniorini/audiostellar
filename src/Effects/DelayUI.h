#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"


class DelayUI : public EffectUI
{
private:
    pdsp::Delay delayL;
    pdsp::Delay delayR;

    pdsp::Parameter time;
    pdsp::Parameter feedback;
    pdsp::Parameter damping;

    pdsp::PatchNode inL;
    pdsp::PatchNode inR;
    pdsp::PatchNode outL;
    pdsp::PatchNode outR;

    const float MAX_DELAY_TIME = 1000;
public:
    DelayUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
