#include "BasiVerbUI.h"

BasiVerbUI::BasiVerbUI()
{
    parameters.push_back(&mix);

    parameters.push_back( new UberSlider(rt60.set("RT60",3.33f,0.f,20.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(density.set("Density",0.5f,0.f,1.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(damping.set("Damping",0.5f,0.f,1.f), &Tooltip::NONE) );
//    parameters.push_back( new UberSlider(hicut.set("Hi-cut",5000.f,0.f,22000.f), &Tooltip::NONE) );

    parameters.push_back( new UberSlider(lfoFreq.set("Mod speed",0.5f,0.f,200.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(lfoAmount.set("Mod amount",0.f,0.f,1.f), &Tooltip::NONE) );

    rt60 >> reverb.in_time();
    density >> reverb.in_density();
    damping >> reverb.in_damping();
//    hicut >> reverb.in_hi_cut();
    lfoFreq >> reverb.in_mod_freq();
    lfoAmount >> reverb.in_mod_amount();

    0.5f >> crossfader.in_fade();

    inL >> crossfader.in_A();
    inL >> stereoAmpDry.ch(0);
    inR >> crossfader.in_B();
    inR >> stereoAmpDry.ch(1);

    //im using the crossfader just for converting to mono
    //since this reverb has mono input
    crossfader >> reverb;
                  reverb.ch(0) >> stereoAmpWet.ch(0);
                  reverb.ch(1) >> stereoAmpWet.ch(1);

    stereoAmpDry.ch(0) + stereoAmpWet.ch(0) >> outL;
    stereoAmpDry.ch(1) + stereoAmpWet.ch(1) >> outR;

    //this is mix:
    parameters[0] = new UberSlider(stereoAmpWet.set("Mix", 0.5f, 0.f, 1.f), &Tooltip::NONE);
    stereoAmpDry.set(0.5f);
    parameters[0]->addListener(this, &BasiVerbUI::onMix, OF_EVENT_ORDER_AFTER_APP);
}

string BasiVerbUI::getName()
{
    return "BasiVerb";
}

void BasiVerbUI::onMix(float &v)
{
    stereoAmpDry.set(1.f - v);
}

pdsp::Patchable &BasiVerbUI::channelIn(size_t index)
{
    if ( index == 0 ) {
        return inL;
    } else if ( index == 1 ) {
        return inR;
    } else {
        throw "BasiVerbUI has stereo input only";
    }
}

pdsp::Patchable &BasiVerbUI::channelOut(size_t index)
{
    if ( index == 0 ) {
        return outL;
    } else if ( index == 1 ) {
        return outR;
    } else {
        throw "BasiVerbUI has stereo output only";
    }
}
