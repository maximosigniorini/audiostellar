#include "SaturatorUI.h"

SaturatorUI::SaturatorUI()
{
    parameters.push_back( new UberSlider(inGain.set("Input Gain", 0.f, -40.f, 100.f),
                                         &Tooltip::NONE) );
    parameters.push_back( new UberSlider(outGain.set("Output Gain", 0.f, -40.f, 100.f),
                                         &Tooltip::NONE) );

    inGain.ch(0) >> saturatorL >> outGain.ch(0);
    inGain.ch(1) >> saturatorR >> outGain.ch(1);
}

string SaturatorUI::getName()
{
    return "Saturator";
}

pdsp::Patchable &SaturatorUI::channelIn(size_t index)
{
    return inGain.ch(index);
}

pdsp::Patchable &SaturatorUI::channelOut(size_t index)
{
    return outGain.ch(index);
}
