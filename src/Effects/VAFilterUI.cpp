#include "VAFilterUI.h"
#include "../GUI/UberSlider.h"

void VAFilterUI::parseLoadedParams()
{
    Json::Value jsonAudioEffectsParams = *loadedParams;
    if ( jsonAudioEffectsParams[ mode_current.getName() ] != Json::nullValue ) {
        mode_current = jsonAudioEffectsParams[ mode_current.getName() ].asInt();
        mode_values[mode_current] >> in_mode();
    } else {
        ofLog() << "WARNING: Parameter '"<< mode_current.getName() <<"' not found for VAFilter";
    }

    EffectUI::parseLoadedParams();
}

VAFilterUI::VAFilterUI() : VAFilter() {
    channels(2);

    parameters.push_back( new UberSlider(cutoff.set("Cutoff",128.f,0.f,128.f),
                                         &Tooltip::NONE) );
    parameters.push_back( new UberSlider(resonance.set("Resonance",0.f,0.f,1.f),
                                         &Tooltip::NONE) );
    cutoff >> in_cutoff();
    resonance >> in_reso();
}

string VAFilterUI::getName()
{
    return "VAFilter";
}

Json::Value VAFilterUI::save()
{
    Json::Value root = EffectUI::save();
    root[mode_current.getName()] = mode_current.get();
    return root;
}

void VAFilterUI::setIndex(int i, string indexPrefix)
{
    string name = "Mode##" + indexPrefix + "Effect" + ofToString(i);
    mode_current.setName(name);

    EffectUI::setIndex(i, indexPrefix);
}

//since this is a multichannel effect, this is easy
pdsp::Patchable &VAFilterUI::channelIn(size_t index)
{
    return ch(index);
}

//since this is a multichannel effect, this is easy
pdsp::Patchable &VAFilterUI::channelOut(size_t index)
{
    return ch(index);
}

void VAFilterUI::drawGui()
{
    if ( ImGui::Combo(mode_current.getName().c_str(),
         (int *)&mode_current.get(), mode_items, IM_ARRAYSIZE(mode_items)) ) {
        mode_values[mode_current] >> in_mode();
    }

    for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
        parameters[i]->draw();
    }
}
