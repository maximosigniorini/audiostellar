#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"

#include "../GUI/UberSlider.h"

class BasiVerbUI : public EffectUI {
private:
    pdsp::Parameter rt60;
    pdsp::Parameter density;
    pdsp::Parameter damping;
//    pdsp::Parameter hicut; // this doesn't seem to be working

    pdsp::Parameter lfoFreq;
    pdsp::Parameter lfoAmount;

    pdsp::LinearCrossfader crossfader;
    pdsp::BasiVerb reverb;

    pdsp::ParameterAmp stereoAmpWet;
    pdsp::ParameterAmp stereoAmpDry;
    UberSlider mix;
    void onMix(float &v);

    pdsp::PatchNode inL;
    pdsp::PatchNode inR;
    pdsp::PatchNode outL;
    pdsp::PatchNode outR;
public:
    BasiVerbUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
