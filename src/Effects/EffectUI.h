#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxJSON.h"

#include "../GUI/UberControl.h"

class EffectUI : public pdsp::Patchable {
protected:
    int index = -1;

    vector<UberControl *> parameters;
    Json::Value * loadedParams = nullptr;
    //this will parse only whats inside parameters
    //each effect should take care of custom params overriding this function
    virtual void parseLoadedParams() {
        Json::Value jsonAudioEffectsParams = *loadedParams;
        for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
            UberControl * param = parameters[i];
            if ( jsonAudioEffectsParams[ param->getName() ] != Json::nullValue ) {
                param->set( jsonAudioEffectsParams[ param->getName() ].asFloat() );
            } else {
                ofLog() << "WARNING: Parameter '" << param->getName() << "' not found for " << getName();
            }
        }

        delete loadedParams;
        loadedParams = nullptr;
    }
public:
    EffectUI() : pdsp::Patchable() {}
    virtual ~EffectUI() {
        for ( UberControl * p : parameters ) {
            delete p;
        }
        parameters.clear();
    }

    virtual string getName() = 0;

    // if the effect is only floats that can be put in variable parameters
    // then you don't have to implement save
    virtual Json::Value save() {
        Json::Value root = Json::Value( Json::objectValue );
        for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
            UberControl * param = parameters[i];
            root[ param->getName() ] = param->get();
        }
        return root;
    }
    void load(Json::Value jsonAudioEffectsParams) {
        //I'll save this data and parseLoadedParams() later when I have the real ids.
        loadedParams = new Json::Value(jsonAudioEffectsParams);
    }

    virtual void setIndex(int i, string indexPrefix) {
        string name;
        for ( unsigned int j = 0 ; j < parameters.size() ; j++ ) {
            name = parameters[j]->originalName + "##" + indexPrefix + "Effect" + ofToString(i);
            parameters[j]->setName(name);
        }

        //first time to set index I'll need to set the saved params
        if ( loadedParams != nullptr ) parseLoadedParams();
    }

    virtual pdsp::Patchable & channelIn(size_t index) = 0; //pure virtual
    virtual pdsp::Patchable & channelOut(size_t index) = 0; //pure virtual
    virtual void drawGui() {
        for ( unsigned int i = 0 ; i < parameters.size() ; i++ ) {
            parameters[i]->draw();
        }
    }
};
