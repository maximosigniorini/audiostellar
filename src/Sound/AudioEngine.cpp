#include "AudioEngine.h"
#include "Voices.h"

AudioEngine * AudioEngine::instance = nullptr;

AudioEngine * AudioEngine::getInstance() {
    if (instance == nullptr) {
        instance = new AudioEngine();
    }
    return instance;
}

AudioEngine::AudioEngine() {
    refreshDeviceList();
}

void AudioEngine::setup(int deviceID, int sampleRate, int bufferSize) {
    
	if (deviceID >= devices.size()) {
		ofLog() << "Critical error: No sound card found.";
		return;
	}
	
	int numChannels = getAudioOutNumChannels(deviceID);
    
    engine.setOutputDeviceID( devices[deviceID].deviceID );
    engine.setChannels(0, numChannels);
    engine.setup(sampleRate, bufferSize, 3);
    
    selectedAudioOutDeviceID = deviceID;
    selectedSampleRate = sampleRate;
    selectedBufferSize = bufferSize;
    selectedAudioOutNumChannels = numChannels;
    
    started = true;

    createOutputs();

    ofNotifyEvent(soundCardInit, deviceID );
}

void AudioEngine::stop()
{
    engine.stop();
}

bool AudioEngine::hasStarted()
{
    return started;
}

void AudioEngine::selectAudioOutDevice(int deviceID) {
    
	if (deviceID >= devices.size()) {
		ofLog() << "Critical error: No sound card found.";
		return;
	}
	
    bool supportedSampleRate = false;

    // check if current sample rate is supported by the device
    for (int i = 0; i < devices[deviceID].sampleRates.size(); i++) {
        if (devices[deviceID].sampleRates[i] == selectedSampleRate) {
            supportedSampleRate = true;
            break;
        }
    }
    
    if (supportedSampleRate) {
        setup(deviceID, selectedSampleRate, selectedBufferSize);
    }
    else {
        setup(deviceID, devices[deviceID].sampleRates[0], selectedBufferSize);
    }
}

bool AudioEngine::selectAudioOutDevice(string deviceName) {
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == deviceName) {
            selectAudioOutDevice(i);
            return true;
        }
    }

    ofLogWarning () << "Audio device not found. Switching to default device.";
    selectAudioOutDevice(getDefaultAudioOutDeviceIndex());
    return false;
}

void AudioEngine::selectSampleRate(int sampleRate) {
    selectedSampleRate = sampleRate;
    selectAudioOutDevice(selectedAudioOutDeviceID);
}

void AudioEngine::selectBufferSize(int bufferSize) {
    selectedBufferSize = bufferSize;
    selectAudioOutDevice(selectedAudioOutDeviceID);
}

//void AudioEngine::start(){
//
//    //engine.setup();
//    engine.start();
//
//}

vector<ofSoundDevice> AudioEngine::listDevices()
{
    return devices;
}

void AudioEngine::refreshDeviceList()
{
    devices.clear();
    vector<ofSoundDevice> unfilteredDevices = engine.listDevices();
    for ( auto device : unfilteredDevices ) {
        if ( device.outputChannels > 0 ) {
            ofLog() << "Using: " << device.name;
            devices.push_back( device );
        }
    }
}

const vector<string> AudioEngine::getEnabledOutputs()
{
    vector<string> enabledOutputs;

    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            enabledOutputs.push_back( outputs[i].label );
        }
    }

    return enabledOutputs;
}

AudioEngine::Output AudioEngine::getEnabledOutput(int idx)
{
    int currentIdx = -1;

    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            currentIdx++;

            if ( currentIdx == idx ) {
                return outputs[i];
            }
        }
    }

    return {"error",-1,-1,false,"error"};
}

int AudioEngine::getDefaultAudioOutDeviceIndex() {
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == "default") {
            return i;
        }
    }

    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].isDefaultOutput) {
            return i;
        }
    }
    
    return 0; //ok first will be fine... or not ? :O
}

int AudioEngine::getAudioOutNumChannels(int deviceID) {
    
    if ( deviceID < devices.size() ) {
        int numChannels = devices[deviceID].outputChannels;

        // we should check numChannels != -1 ??
        if (numChannels > PDSP_MAX_OUTPUT_CHANNELS) {
//            ofLogWarning() << "Selected device has " << numChannels << " output channels. Setting output channels to " << PDSP_MAX_OUTPUT_CHANNELS <<".";
            numChannels = PDSP_MAX_OUTPUT_CHANNELS;
        }
        return numChannels;
    }

    return -1;
}

int AudioEngine::getSelectedDeviceAudioOutNumChannels()
{
	if (selectedAudioOutDeviceID == -1) {
		return -1;
	}
	return getAudioOutNumChannels(selectedAudioOutDeviceID);
}

string AudioEngine::getSelectedDeviceName()
{
	if (selectedAudioOutDeviceID == -1) {
		return "";
	}
	return devices[selectedAudioOutDeviceID].name;
}

void AudioEngine::createOutputs() {

    outputs.clear();
    
    for (int i = 0; i < selectedAudioOutNumChannels; i+=2) {
        Output output;
        output.type = "stereo";
        output.channel0 = i;
        output.channel1 = i + 1;
        output.enabled = true;
        output.label = ofToString(i + 1) + " - " + ofToString(i + 2);
        outputs.push_back(output);
    }

//    no mono outputs for now

//    for (int i = 0; i < selectedAudioOutNumChannels; i++) {
//        Output output;
//        output.type = "mono";
//        output.channel0 = i;
//        output.channel1 = i;
//        output.enabled = false;
//        output.label = ofToString(i + 1);
//        outputs.push_back(output);
//    }
    
    if (outputs.size() > 0) {
        outputs[0].enabled = true;
    }
}

Json::Value AudioEngine::save() {
    Json::Value root = Json::Value(Json::objectValue);
    
	if (selectedAudioOutDeviceID != -1) {
		root["selectedAudioOut"] = devices[selectedAudioOutDeviceID].name;
		root["selectedSampleRate"] = selectedSampleRate;
		root["selectedBufferSize"] = selectedBufferSize;
	}
    
    vector<int> selectedAudioOutChannels;
    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            root["selectedAudioOutChannels"].append(i);
        }
    }
    
    return root;
}

void AudioEngine::load(Json::Value json) {
    
    Json::Value audioSettings = json["audioSettings"];
    
    if (audioSettings["selectedSampleRate"] != Json::nullValue) {
        selectedSampleRate = audioSettings["selectedSampleRate"].asInt();
    }

    if (audioSettings["selectedBufferSize"] != Json::nullValue) {
        selectedBufferSize = audioSettings["selectedBufferSize"].asInt();
    }
    

    bool successfulSelectedDevice = false;
    if (audioSettings["selectedAudioOut"] != Json::nullValue) {
        string selectedAudioOut = audioSettings["selectedAudioOut"].asString();
        successfulSelectedDevice = selectAudioOutDevice(selectedAudioOut);
    } else {
        selectAudioOutDevice( getDefaultAudioOutDeviceIndex() );
    }

    // this should be readded in the future
//    if (successfulSelectedDevice && audioSettings["selectedAudioOutChannels"] != Json::nullValue) {
//        for ( unsigned int i = 0 ; i < audioSettings["selectedAudioOutChannels"].size() ; i++ ) {
//            int selectedChannel = audioSettings["selectedAudioOutChannels"][i].asInt();
//            if ( selectedChannel < outputs.size() ) {
//                outputs[selectedChannel].enabled = true;
//            }
//        }
//    }
}

void AudioEngine::exit() {
    engine.stop();
}
