#include "Voices.h"

void Voices::die()
{
    ofRemoveListener(ofEvents().update, this, &Voices::update);
    voiceMap.clear();

    for ( auto &v : voices ) {
        v.die();
    }
}

Voices::Voices() {
    voices.resize(numVoices);

    loadedVoices = 0;

    for (unsigned int i = 0; i < voices.size(); ++i) {
        voices[i].patch();
    }

    // init voice map
    vector <shared_ptr<Sound>> sounds = Sounds::getInstance()->getSounds();
    for(unsigned int i = 0; i < sounds.size(); i++) {
        voiceMap[ sounds[i] ] = vector <Voice *>();
    }

    ofAddListener(ofEvents().update, this, &Voices::update);
}

//This tells each sound if it is playing or not. It is used for the playing animation
void Voices::update(ofEventArgs &args)
{
    for (auto &m : voiceMap)  {
        bool isPlaying = false;
        if ( m.second.size() > 0 ) {
            for ( Voice * v : m.second ) {
                if ( v->isPlaying() ) {
                    isPlaying = true;
                    break;
                }
            }
            m.first->updatePlaying(isPlaying);
        }
    }
}

Voices::~Voices()
{
    ofRemoveListener(ofEvents().update, this, &Voices::update);
}

Voice * Voices::getNonPlayingVoice(shared_ptr<Sound> sound) {
    for (unsigned int i = 0; i < voiceMap[sound].size(); i++) {
        if ( !voiceMap[sound][i]->isPlaying() ) { // voice not playing
            return voiceMap[sound][i];
        }
    }
    return nullptr;
}

Voice * Voices::getFreeVoice() {
    
    if (loadedVoices < numVoices) {
        Voice * voice = &voices[loadedVoices];
        loadedVoices++;
        return voice;
    }
    else {
        for (int i = 0; i < voices.size(); i++) {
            if ( !voices[i].isPlaying() ) {
                return &voices[i];
            }
        }
    }
    return nullptr;
}

Voice * Voices::getReplaceableVoice() {
    float playerPosition = 0;
    Voice * voice = nullptr;
    // get the voice that is nearer to finish
    // isn't this resource heavy??
    for (unsigned int i = 0; i < voices.size(); i++) {
        if (voices[i].getPlayerPosition() > playerPosition) {
            playerPosition = voices[i].getPlayerPosition();
            voice = &voices[i];
        }
    }
    return voice;
}

Voice *Voices::getVoice(shared_ptr<Sound> sound)
{
    Voice * voice;

    voice = getNonPlayingVoice(sound); //voices allocated for each sound
    if (voice == nullptr) { // no voice allocated or all allocated voices are playing
        voice = getFreeVoice();
        if (voice == nullptr) { // no free voice, all voices are playing
            voice = getReplaceableVoice();
            takeOverVoice(sound, voice);
        }
        else { // voice not playing found
            takeOverVoice(sound, voice);
        }
    }
    voice->play();
    return voice;
}

void Voices::muteSound(shared_ptr<Sound> s)
{
    for ( auto &v : voices ) {
        if ( v.getSound() == s ) {
            v.stop();
        }
    }
}

void Voices::takeOverVoice(shared_ptr<Sound> sound, Voice * voice) {
    if (voice->getSound() != nullptr) {
        vector <Voice *> * voices = &voiceMap[voice->getSound()];
        voices->erase(remove(voices->begin(), voices->end(), voice), voices->end());
    }
    voiceMap[sound].push_back(voice);
    voice->setSound(sound);
    voice->load( sound->getFileName() );
}
