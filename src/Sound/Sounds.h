#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "../GUI/ColorPaletteGenerator.h"
#include "Sound.h"
#include "ofxJSON.h"
#include "ofxConvexHull.h"
#include "../ML/DBScan.h"
#include "ofxImGui.h"
#include "Tooltip.h"
#include "../Servers/OscServer.h"
#include "AudioEngine.h"
#include "Sound.h"
#include "QuadTree.h"
#include "../Servers/tidalOscListener.h"

//class Sound;
class Voices;

struct soundPosition {
    shared_ptr<Sound> sound;
    ofVec2f position;
};

class Sounds {
private:
#define CLUSTERNAME_MAX_LENGTH 64
#define CLUSTERNAME_SHOW_SHORTCUT 'c'
    
    Sounds();
    static Sounds * instance;
    
    vector< shared_ptr<Sound> > sounds;
    shared_ptr<Sound> hoveredSound = nullptr;
    
    ofBuffer tsvFile;

    bool useOriginalPositions = true;
    bool useOriginalClusters = false;
    string currentSoundLabel = "";
    
    int epsDbScan;
    int minPts = 5;

    void setSpaceLimits(vector<string> soundsCoords);
    ofVec3f spaceLimits[2];
    const int spacePadding = 30;
    
    // Clusters
    set<int> clusterIds;
    vector<bool> clustersShow;
    int nClusters;
    int selectedCluster;

    bool showClusterNames = false;
    void deleteClusterNames();
    bool areAnyClusterNames();
    float clusterNamesOpacity = 0;
    float clusterNamesSpeed = 5;
    void findConvexHulls();
    vector<ofPolyline> hulls;

    int idCount = 0;
    
    ofTrueTypeFont font;
    
#define BG_TRANSLATION 0.3
#define BG_ALPHA_MIN 30
#define BG_ALPHA_MAX 50
#define BG_ALPHA_STEP 0.3
#define BG_GLOW_SIZE 50

    int windowOriginalWidth;
    int windowOriginalHeight;
    ofFbo fboBackground;
    float backgroundAlpha = BG_ALPHA_MIN;
    int backgroundDirection = 1;
    
    //overrided Positions
    vector<soundPosition> soundPositions;
    void updateSoundPosition(shared_ptr<Sound> sound);

    //GUI
    bool isContextMenuHovered = false;

    //Clustering Options GUI
    bool haveToDrawClusteringOptionsScreen = false;

public:
    
    static Sounds* getInstance();

    bool showSoundFilenamesTooltip = false;
    bool soundIsBeingMoved = false; //when moving sounds dragging with right click
    
    //MAIN
    void loadSounds(ofxJSONElement &jsonFile);
    void update();
    void draw();
    void drawGui();
    void reset();
    
    //DRAW
    void drawBackground();
    void generateBackground();
    void animateClusterNames();
    void drawClusteringOptionsScreen();
    void setClusteringOptionsScreenFlags();
    void drawClusterNames();
    
    //SESSION
    Json::Value save();
    void load( Json::Value jsonData );
    
    //CLUSTERING
    void onClusterToggle(int clusterIdx);
    void doClustering();
    
    //MOUSE
    void mouseMoved(int x, int y);
    void mouseDragged(ofVec2f p, int button);
    void mousePressed(int x, int y , int button);
    void mouseReleased(int x, int y , int button);

    //KEYBOARD
    void keyPressed(ofKeyEventArgs &e);
    void keyReleased(int key);

    void allSoundsSelectedOff();
    void allSoundsHoveredOff(shared_ptr<Sound> except = NULL);
    
    void revertToOriginalPositions();
    void sequenceUnitReprocess();
    
    //SETTERS
    void setFilenameLabel(string fileName);
    void setDBScanClusters( vector<dbscanPoint> points );
    void setUseOrigPos(bool v);
    void setShowSoundFilenames(bool v);

    //GETTERS
    shared_ptr<Sound> getNearestSound(ofVec2f position);
    unsigned int getSoundCount();
    shared_ptr<Sound> getHoveredSound();
    shared_ptr<Sound> getSoundByFilename(string filename);
    shared_ptr<Sound> getSoundById(int id);
    const vector<shared_ptr<Sound>> & getSounds();
    vector<shared_ptr<Sound>> getSoundsByCluster(int clusterId);
    vector<shared_ptr<Sound>> getSoundsByCluster(string clusterNameBeingEdited);
    vector<shared_ptr<Sound>> getNeighbors(shared_ptr<Sound> s, float threshold);
    vector<shared_ptr<Sound>> getNeighbors(ofVec2f position, float threshold);
    vector<ofPoint> getSoundsAsPoints( bool originalPositions = true );
    bool getUseOriginalPositions();
    bool getUseOriginalClusters();
    bool getShowSoundFilenames();
    int getInitialWindowWidth();
    int getInitialWindowHeight();
    int getHoveredClusterID();
    ofRectangle getClusterBB( int clusterID );    

    //COORDINATES
    ofVec3f camToSoundCoordinates(ofVec3f camCoordinates);
    ofVec3f camToSoundCoordinates(ofVec2f camCoordinates);
    ofVec3f soundToCamCoordinates(ofVec3f soundCoordinates);
    
    string selectFolder();
    void exportFiles();
    
    char clusterNameBeingEdited[CLUSTERNAME_MAX_LENGTH] = "\0";
    vector<string> clusterNames;
    void nameCluster(unsigned int clusterID);

    //QuadTREE
    Quad mainQuadTree;
};
