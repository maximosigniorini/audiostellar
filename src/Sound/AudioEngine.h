#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

class Voices;

class AudioEngine {
    
private:
    
    AudioEngine();
    static AudioEngine * instance;

    vector <ofSoundDevice> devices;
    bool started = false;
    
public:
    
    static AudioEngine * getInstance();
    
    struct Output {
        string type;
        int channel0;
        int channel1;
        bool enabled;
        string label;
    };

    pdsp::Engine engine;
    ofEvent<int> soundCardInit;
    
    void setup(int deviceID, int sampleRate, int bufferSize);
	void stop();
    bool hasStarted();

    int getDefaultAudioOutDeviceIndex();
    int getAudioOutNumChannels(int deviceID);
    int getSelectedDeviceAudioOutNumChannels();
    string getSelectedDeviceName();
    
    void selectAudioOutDevice(int deviceID);
    bool selectAudioOutDevice(string deviceName);
    void selectSampleRate(int sampleRate);
    void selectBufferSize(int bufferSize);

    vector <ofSoundDevice> listDevices();
    void refreshDeviceList();
    
    int selectedAudioOutDeviceID = -1;
    int selectedAudioOutNumChannels = -1;
    
    vector <Output> outputs; //this should be private with public methods for UI
    const vector<string> getEnabledOutputs();
    Output getEnabledOutput(int idx); //this idx is the enabled output index, not the output
    
    const vector<int> bufferSizes {32, 64, 128, 256, 512, 1024, 2048, 4096};
    
    int selectedSampleRate = 44100;
    int selectedBufferSize = 512;
    
    void createOutputs();
    
    Json::Value save();
    void load(Json::Value json);
    
    void exit();
    
};
