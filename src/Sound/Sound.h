#pragma once

#include "ofMain.h"
#include "Tweener.h"

#define SIZE_ORIGINAL 2
#define SIZE_GLOW 24

#define TSV_FIELD_X 0
#define TSV_FIELD_Y 1
#define TSV_FIELD_Z 2
#define TSV_FIELD_CLUSTER 3
#define TSV_FIELD_NAME 4

class Voice;

class Sound {
    
private:
    string name = "";
    bool hide = false;
    int cluster = -1;
    
    float sizeInc = 0.2; //animación de envolvente de sonido
    ofVec2f originalPosition;
    
    bool isPlaying = false;
    bool unglowing = false;
    
    TWEEN::Manager tweenManager;
    shared_ptr<TWEEN::Tween> currentTween = NULL;
    int glowOpacity = 0;
    
    void startGlow();
    void stopGlow();
    
    const int OPACITY_MIN = 60;
    float colorOpacity = 0;
    
public:
    Sound (string _name, ofVec3f _position, int _cluster);
    Sound (string _name);
    ~Sound();

//    static int deadSounds;
    
    int id;
    ofVec3f position;
    float size = SIZE_ORIGINAL;
    
    bool hovered = false;
    bool clusterIsHovered = true;
    bool selected = false;
    static ofColor colorSelected;
    
    void setHovered(bool b);
    bool getHovered();
    
    static ofImage imgGlow;
    
    void drawGlow();
    void draw();
    void update();
    void updatePlaying(bool isPlaying);
    // void mouseMoved(int x, int y);
    
    string getFileName();
    ofVec2f getPosition();
    ofVec2f getOriginalPosition();
    void useOriginalPosition();

    void setCluster(int cluster);
    int getCluster();
    bool getHide();
    void setHide(bool h);
    void toggleHide();

    void glow();
    
    string fileName = "";
    string hoveredSoundFolderPath = "";
    void drawGui();

    float volume = 1.0;
    
    bool mute = false;
    
};
