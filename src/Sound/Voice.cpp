#include "Voice.h"

Voice::Voice()
{

}

Voice::~Voice()
{
    die();
    unpatch();
    unload();
}

void Voice::die()
{
    if ( sound != nullptr ) {
        sound->updatePlaying(false);
    }
}

void Voice::patch() {
    if ( isPatched ) return;

    addModuleOutput( "0", volume0 );
    addModuleOutput( "1", volume1 );

    addModuleInput("speed", sampler0.in_pitch());
    addModuleInput("speed", sampler1.in_pitch());

    sampleTrigger >> sampler0 >> volume0;
    sampleTrigger >> sampler1 >> volume1;

    //envTrigger >> env >> amp0.in_mod();
    //env >> amp1.in_mod();

    sampler0.addSample( &sample, 0 );
    sampler1.addSample( &sample, 1 );

    volume0.set(0.0f);
    volume1.set(0.0f);

    isPatched = true;
    //    sample.setVerbose(true);
}

void Voice::unpatch()
{
    volume0.disconnectAll();
    volume1.disconnectAll();

    sampler0.disconnectAll();
    sampler1.disconnectAll();

    sampleTrigger.disconnectAll();

    isPatched = false;
}

void Voice::load(string path) {
    stop();
    sample.load(path);
    if (sample.channels == 1) {
        sampler1.setSample(&sample, 0, 0);
    } else {
        sampler1.setSample(&sample, 0, 1);
    }
}

void Voice::unload() {
    //I don't think any of these is needed
    stop();
    sample.unLoad();
}

void Voice::play(float volume) {
    setVolume(volume);
    envTrigger.trigger(1.0f); //this isn't connected yet
    sampleTrigger.trigger(1.0f);

    isStopped = false;
}

void Voice::stop() {
    setVolume(0.0f);
    envTrigger.off(); //this isn't connected yet

    // this does nothing
    // triggering 1.0 will play sample but 0.0 won't stop it
    sampleTrigger.off();

    isStopped = true;

    // i think this isn't needed with isStopped
//    if ( sound != nullptr ) {
//        sound->updatePlaying(false);
//    }
}

bool Voice::isLoaded() {
    return sample.loaded();
}

bool Voice::isPlaying() {
    if ( isStopped ) return false;

    // this can return false if sample
    // hasn't been stopped but the sample finished
    return sampler0.is_playing();
}

float Voice::getPlayerPosition() {
    return sampler0.meter_position();
}

void Voice::setVolume(float volume) {
    volume0.set(volume);
    volume1.set(volume);
}

void Voice::setOutputs(int channel0, int channel1) {
    out("0").disconnectOut();
    out("1").disconnectOut();
}

void Voice::setSound(shared_ptr<Sound> s)
{
    if ( sound != nullptr ) {
        sound->updatePlaying(false);
    }
    sound = s;
}

shared_ptr<Sound> Voice::getSound()
{
    return sound;
}
