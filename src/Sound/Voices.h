#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sounds.h"
#include "ofxJSON.h"
#include "Voice.h"

class Voices {
private:
    vector <Voice> voices;
    map < shared_ptr<Sound>, vector <Voice *> > voiceMap;

    void die();

    friend class Unit;
public:
    Voices();
    ~Voices();


    void update(ofEventArgs & args);

    Voice * getNonPlayingVoice(shared_ptr<Sound> sound);
    Voice * getFreeVoice();
    Voice * getReplaceableVoice();
    void takeOverVoice(shared_ptr<Sound> sound, Voice * voice);

    int numVoices = 32; //default voices per unit
    
    int loadedVoices = 0;

    Voice * getVoice(shared_ptr<Sound> sound);

    void muteSound(shared_ptr<Sound> s);
};

