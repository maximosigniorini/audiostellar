#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sound.h"

class Voice : public pdsp::Patchable {

public:

    Voice();
    Voice(const Voice & other) { /* this is needed */};
    ~Voice();

    void die();

    AudioEngine * audioEngine = AudioEngine::getInstance();

    void patch();
    void unpatch();

    void load(string path);
    void unload();
    void play(float volume = 1.0f);
    void stop();
    bool isLoaded();
    bool isPlaying();
    float getPlayerPosition();
    void setVolume(float volume);
    void setOutputs(int channel0, int channel1);
    void setOutputs();

    void setSound(shared_ptr<Sound> s);
    shared_ptr<Sound> getSound();

private:
    shared_ptr<Sound> sound = nullptr;

    pdsp::Sampler sampler0;
    pdsp::Sampler sampler1;
    pdsp::ADSR env;
    pdsp::Amp amp0;
    pdsp::Amp amp1;
    pdsp::Amp volume0;
    pdsp::Amp volume1;
    pdsp::TriggerControl sampleTrigger;
    pdsp::TriggerControl envTrigger;

    pdsp::SampleBuffer sample;
    bool isPatched = false;

    // current implementation of sampler can't be stopped
    // instead of changing that sensitive part of code
    // we "emulate stop". yes, this should change.
    bool isStopped = false;
};
