#include "Sound.h"
#include "../Utils/Utils.h"
#include "../GUI/ColorPaletteGenerator.h"
#include "../Utils/Tooltip.h"
#include "Voices.h"

ofImage Sound::imgGlow;
//ofImage Sound::imgGlowBig;

ofColor Sound::colorSelected = ofColor(232, 11, 85);
// TWEEN::Manager Sound::manager;
//int Sound::deadSounds = 0;

Sound::Sound(string _name, ofVec3f _position, int _cluster) {
    
    name = _name;
    position = _position;
    originalPosition = _position;
    cluster = _cluster;
    
    if ( !Sound::imgGlow.isAllocated() ) {
        Sound::imgGlow.load("assets/star0_24.png");
        TWEEN::setTimeFunc( ofGetElapsedTimef );
    }
}

Sound::Sound(string _name) {
    name = _name;
    position.set(ofRandom(ofGetWidth() / 3, ofGetWidth() * 2 / 3),
                 ofRandom(ofGetHeight() / 3, ofGetHeight() * 2 / 3));
}

Sound::~Sound()
{
    // it looks like around 20 sounds never die for some reason
    // should research more on this
    // my guesses are sequence unit

//    deadSounds++;
//    ofLog() << "dead " << deadSounds;
}

void Sound::update() {
    if ( isPlaying && size < SIZE_ORIGINAL * 2 ) {
        size += sizeInc;
    } else if ( !isPlaying ) {
        if ( size > SIZE_ORIGINAL ) {
            size -= sizeInc;
            unglowing = true;
        } else {
            if ( unglowing ) {
                unglowing = false;
                if ( !hovered ) {
                    stopGlow();
                }
            }
        }
    }
}
 void Sound::updatePlaying(bool isPlaying)
{
     this->isPlaying = isPlaying;
}

void Sound::drawGlow() {
    if ( glowOpacity > 0 ) {
        int glowSize = size*12;
        if ( !selected ) {
            ofSetColor( ColorPaletteGenerator::getColor(cluster), glowOpacity);
        } else {
            ofSetColor(colorSelected.r, colorSelected.g, colorSelected.b, glowOpacity);
        }

        if ( glowSize == SIZE_GLOW ) {
            Sound::imgGlow.draw(position.x - glowSize / 2,
                                position.y - glowSize / 2);
        } else {
            Sound::imgGlow.draw(position.x - glowSize / 2,
                                position.y - glowSize / 2,
                                glowSize,
                                glowSize);
        }

    }
}

void Sound::draw() {
    
    ofFill();
    drawGlow();
    
    if (mute) {
        ofColor clusterColor = ColorPaletteGenerator::getColor(cluster);
        ofSetColor(clusterColor, 10);
    }
    else {
        if ( !selected ) {
            ofColor clusterColor = ColorPaletteGenerator::getColor(cluster);
            clusterColor.a = floor(colorOpacity);
            ofSetColor( clusterColor );
            
            if ( clusterIsHovered ) {
                if ( colorOpacity < 255 ) {
                    colorOpacity += ofGetLastFrameTime() * 500;
                }
            } else {
                if ( colorOpacity > OPACITY_MIN ) {
                    colorOpacity -= ofGetLastFrameTime() * 500;
                }
            }
            
            colorOpacity = ofClamp(colorOpacity, 0, 255);
            
        } else {
            ofSetColor(colorSelected, 255);
        }
    }
    
    ofDrawCircle(position.x, position.y, size );
}

void Sound::startGlow() {
    //i think this is slow and must be reimplemented
    //maybe ofxEasing
    currentTween = tweenManager.addTween( glowOpacity, 100, 0.2 );
    currentTween->start();
}
void Sound::stopGlow() {
    if ( currentTween != NULL ) {
        tweenManager.remove(currentTween);
    }
    tweenManager.addTween( glowOpacity, 0, 0.2 )->start();
}
void Sound::setHovered(bool b) {
    if ( b ) {
        startGlow();
    } else {
        if ( !isPlaying ) {
            stopGlow();
        }
    }
    
    hovered = b;
}
bool Sound::getHovered() {
    return hovered;
}

ofVec2f Sound::getPosition() {
    return position;
}

ofVec2f Sound::getOriginalPosition()
{
    return originalPosition;
}

void Sound::setCluster(int cluster)
{
    this->cluster = cluster;
}
string Sound::getFileName() {
    return name;
}
int Sound::getCluster() {
    return cluster;
}
bool Sound::getHide() {
    return hide;
}
void Sound::setHide(bool h) {
    hide = h;
}

void Sound::toggleHide(){
    hide = !hide;
}

void Sound::glow() {
    if ( !hovered ) {
        startGlow();
    }
}

void Sound::useOriginalPosition() {
    position = originalPosition ;
}

void Sound::drawGui() {
    fileName = getFileName();

    string tooltipTitle = "Sound #" + ofToString( id ) + " - ";
    if ( cluster >= 0 ) {
        tooltipTitle += "Cluster #" + ofToString(cluster);
    } else {
        tooltipTitle += "No cluster";
    }

    Tooltip::tooltip x = { tooltipTitle, fileName.c_str() };
    Tooltip::setTooltip(x);
}
