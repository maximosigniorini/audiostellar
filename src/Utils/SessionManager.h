#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "Units/Units.h"
#include "Sound/Sounds.h"
#include "../Servers/MidiServer.h"
#include "ofxImGui.h"
#include "../Servers/OscServer.h"
#include "../GUI/UI.h"
#include "AudioEngine.h"
#include "Voices.h"

class Units;
class MidiServer;

class SessionManager {

private:
    SessionManager();
    static SessionManager* instance;


    Units * units = nullptr;
    Sounds * sounds = nullptr;
    MidiServer * midiServer = nullptr;
    AudioEngine * audioEngine = nullptr;
//    Voices * voices = nullptr;

    const string STRING_ERROR = "ERROR";

    //SESSION
    struct datasetResult{
        bool success;
        string status;
        ofxJSONElement data;
    };

    string jsonFilename = "";
    ofxJSONElement * jsonFile = nullptr;

    string userSelectJson();
    datasetResult * validateDataset(string datasetPath);

    bool sessionLoaded = false;

    //STELLAR FILE
    string SETTINGS_FILENAME = "stellar.1-0.json"; //path will change depending on OS on constructor
    ofxJSONElement settingsFile;
    bool settingsFileExist();
    void createSettingsFile();
    void loadSettings();
    void saveSettingsFile( bool crashCheck = false );
   

    //RECENT PROJECTS
    ofFile recentProjectsFile;
    vector<string> recentProjects;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);
    const string DEFAULT_SESSION = "assets/default_session/default.session.json";
    const string DEFAULT_SESSION_DIRECTORY = "assets/default_session/drumkits.mp3/";
    const string DEFAULT_SESSION_PLACEHOLDER = "DEFAULT_SESSION";

    //LAST MIDI DEV
    string lastSavedMidiDevice = "";

    void setWindowTitleWithSessionName();

    bool lastCrashed = false;

public:
    static SessionManager* getInstance();
    string sessionAudioDeviceName = "";

    struct datasetLoadOptions{
        string method;
        string path;
    };

    void loadInitSession();
    void loadSession(datasetLoadOptions opts);
    void loadDefaultSession(bool showWelcomeScreen = true);
    void saveSession();
    bool isDefaultSession = false;
    void startAudioWithCurrentConfiguration();
    void resetSettings();

    void saveAsNewSession();
    void exit();

    bool areAnyRecentProjects(vector<string> recentProjects);

    //Getters
    bool getSessionLoaded();
    vector<string> getRecentProjects();

    void keyPressed(ofKeyEventArgs & e);
};
