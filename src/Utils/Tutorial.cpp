#include "Tutorial.h"

Tutorial::Tutorial()
{
    TutorialPage page1;
    page1.title = "The sound map - 1/6";
    page1.content = "What you are watching right now is a sound map made from more than 2000 drumkit sounds. \n\n"
                    "Each point is a short audio clip. \n\n"
                    "Press left mouse button while moving the mouse to hear whatever sound is behind the pointer. \n\n"
                    "Scroll up and down for zooming in and out.\n\n"
                    "Press middle mouse button, <space> or <alt> while moving the mouse to pan the camera. \n\n"
                    "Take your time to explore how similar sounds share a color and are clustered together in latent space. "
                    "Hold 'c' on your keyboard to see cluster names. \n\n"
                    "Hit 'Next' when you are done. \n\n";

    TutorialPage page2;
    page2.title = "Units? - 2/6";
    page2.content = "On the left panel you will find all your units. Right now you only have an Explorer Unit.\n\n"
                    "Units looks like tracks on a DAW, you can adjust their volume, pan, mute, solo and output channel.\n\n"
                    "On the right panel you will find the selected unit configuration. Each unit has different parameters as they "
                    "behave differently, although you will also find they share functionalities as well.\n\n"
                    "Click 'Next' to learn about Explorer unit. \n\n";

    TutorialPage page3;
    page3.title = "Explorer unit - 3/6";
    page3.content = "Explorer unit was primarily crafted for exploring your sound map but it can do more than that.\n\n"
                    "Let's create a trajectory. On the right panel, click Trajectory -> Create trajectory and then click and drag "
                    "on the map in a dense area with sounds.\n\n"
                    "Congratulations, you've created your first space loop. \n\n"
                    "Let's add effects. On the same panel, click the '+ Add' button under 'Effects' and choose BasiVerb. Now "
                    "your loop has reverb applied. Experiment with the different parameters of the reverb and adjust the volume "
                    "of the unit if needed. \n\n";

    TutorialPage page4;
    page4.title = "Particle unit - 4/6";
    page4.content = "Add a Particle unit by hitting the '+' button below Explorer unit. \n\n"
                    "Clicking and dragging on the map will now cast particles instead. They will play everything they touch.\n\n"
                    "These are autonomous agents that are governed by a set of parameters you can tweak using the panel on the right. \n\n"
                    "Select 'Explosion' and click anywhere on the map to try a different particle model. \n\n"
                    "Note that every fader in AudioStellar has a helpful tooltip that will appear when hovering, use them "
                    "to learn more about each parameter.\n\n";

    TutorialPage page5;
    page5.title = "Sequence unit - 5/6";
    page5.content = "Add a Sequence unit hitting the '+' button below the Swarm unit you've just created.\n\n"
                    "Clicking on the points on the map will create a constellation and selected samples will be play in a sequence.\n\n"
                    "The rhythm will be created according to the distance between points.\n\n"
                    "You can change points locations by dragging them using the mouse right button.\n\n"
                    "Adjust BPM using the fader at the top, MIDI clock can also be synced with other applications and hardware.\n\n";


    TutorialPage page6;
    page6.title = "Have fun! - 6/6";
    page6.content = "There is much more of AudioStellar than this: MIDI, OSC and JACK/Loopback/Voicemeeter support allows to connect with other applications "
                    "and interfaces. Note that right clicking on any fader will let you map it to your MIDI controller.\n\n"
                    "The real adventure begins when creating your own sound map, try it using the File menu and clicking New.\n\n"
                    "We are an open source community with our HQ on AudioStellar Facebook group, join us to share your experience and discuss new features.\n\n";

    pages.push_back(page1);
    pages.push_back(page2);
    pages.push_back(page3);
    pages.push_back(page4);
    pages.push_back(page5);
    pages.push_back(page6);
}

Tutorial::TutorialPage * Tutorial::getNextPage()
{
    currentPage++;

    if ( currentPage < pages.size() ) {
        return &(pages[currentPage]);
    }

    return nullptr;
}

bool Tutorial::isLastPage()
{
    return currentPage == pages.size() -1;
}

Tutorial::TutorialPage * Tutorial::start()
{
    currentPage = 0;
    return &(pages[currentPage]);
}
