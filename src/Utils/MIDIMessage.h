#pragma once

#include "ofMain.h"
#include "ofxMidiConstants.h"

struct MIDIMessage {
  MidiStatus status;
  int pitch;
  int cc;
  int ccVal;
  int beats = -1;
  double bpm = -1;
};
