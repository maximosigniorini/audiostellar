#pragma once

#include "ofMain.h"
#include "../Sound/Sound.h"

/*
        Implementación de un QUAD-TREE para calcular distancias entre dos puntos.
        La idea general es dividir un espacio 2D en 4 cuadrantes y checkear si
        hay n o más puntos dentro de un cuadrante; si hay ese cuadrantes si divide
        en otros 4 cuadrantes, y así recursivamente.

        La estructura en si es un árbol.

 */

class Quad{

        vector< shared_ptr<Sound> > sounds;
        int MAX_SIZE = 3;

        void insertChilds(shared_ptr<Sound> sound);
        void subdivide();
        bool inBoundary(ofVec2f p);

        void calculateMainQuadSize(const vector< shared_ptr<Sound> > &sounds);

        ofVec2f topLeft;
        ofVec2f botRight;

        //Hijos de este árbol
        Quad *topLeftTree;
        Quad *topRightTree;
        Quad *botLeftTree;
        Quad *botRightTree;

public:
        Quad(ofVec2f topL = ofVec2f(0,0), ofVec2f botR = ofVec2f(0,0));
        void insert(shared_ptr<Sound> sound);
        vector<shared_ptr<Sound>> * search(ofVec2f p);

        void draw();

        void reset();
        void reprocess(const vector< shared_ptr<Sound> > & sounds);
};
