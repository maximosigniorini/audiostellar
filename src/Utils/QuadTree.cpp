#include "QuadTree.h"

Quad::Quad(ofVec2f topL, ofVec2f botR){

    topLeftTree  = nullptr;
    topRightTree = nullptr;
    botLeftTree  = nullptr;
    botRightTree = nullptr;

    topLeft = topL;
    botRight = botR;
}


//Insertar un node en el árbol dado por sus corners (dos Point)
void Quad::insert(shared_ptr<Sound> sound){

    if(sound == nullptr) return;

    if(!inBoundary(sound->position)) return;

    if(sounds.size() < MAX_SIZE && topLeftTree == nullptr){
       sounds.push_back(sound);
       return;
    }

    if(sounds.size() >= MAX_SIZE){
       //Dividir en 4
       //e inserto el node en los 4
        subdivide();
        insertChilds(sound);
        for(int i = 0; i <  sounds.size(); i++){
           insertChilds(sounds[i]);
        }
        sounds.clear();
        return;
    }

    insertChilds(sound);

}

void Quad::insertChilds(shared_ptr<Sound> sound){
    topLeftTree->insert(sound);
    botLeftTree->insert(sound);
    topRightTree->insert(sound);
    botRightTree->insert(sound);
}

void Quad::subdivide(){
    topLeftTree = new Quad(
                ofVec2f(topLeft.x, topLeft.y),
                ofVec2f((topLeft.x + botRight.x) / 2,
                      (topLeft.y + botRight.y)/2)
                );

    botLeftTree = new Quad(
                ofVec2f(topLeft.x, (topLeft.y + botRight.y) /2),
                ofVec2f((topLeft.x + botRight.x) / 2, botRight.y)
                );

    topRightTree = new Quad(
                ofVec2f((topLeft.x + botRight.x) / 2, topLeft.y),
                ofVec2f(botRight.x, (topLeft.y + botRight.y) / 2)
                );

    botRightTree = new Quad(
                ofVec2f((topLeft.x + botRight.x) / 2,
                      (topLeft.y + botRight.y) / 2),
                ofVec2f(botRight.x, botRight.y)
                );

}

vector<shared_ptr<Sound>> * Quad::search(ofVec2f p) {
    //El quad en el que estoy no contiene el punto
    if(!inBoundary(p)) return nullptr;

    if(topLeftTree == nullptr){
        return &sounds;
    }

    vector< shared_ptr<Sound> > *v1 = topLeftTree->search(p);
    if(v1 != nullptr){
       return v1;
    }
    vector< shared_ptr<Sound> > *v2 = botLeftTree->search(p);
    if(v2 != nullptr){
       return v2;
    }
    vector< shared_ptr<Sound> > *v3 = topRightTree->search(p);
    if(v3 != nullptr){
       return v3;
    }
    vector< shared_ptr<Sound> > *v4 = botRightTree->search(p);
    if(v4 != nullptr){
       return v4;
    }

    return nullptr;
}

bool Quad::inBoundary(ofVec2f p){
    return (p.x >= topLeft.x &&
            p.x < botRight.x &&
            p.y >= topLeft.y &&
            p.y < botRight.y);
}

void Quad::calculateMainQuadSize(const vector< shared_ptr<Sound> > &sounds)
{
    if ( sounds.empty() ) return;

    shared_ptr<Sound> firstSound = sounds[0];
    topLeft = botRight = firstSound->getPosition();

    for ( shared_ptr<Sound> s : sounds ) {
        ofVec2f pos = s->getPosition();

        if ( pos.x < topLeft.x ) {
            topLeft.x = pos.x;
        }
        if ( pos.x > botRight.x ) {
            botRight.x = pos.x;
        }
        if ( pos.y < topLeft.y ) {
            topLeft.y = pos.y;
        }
        if ( pos.y > botRight.y ) {
            botRight.y = pos.y;
        }
    }

    //Add a little padding for edge cases
    topLeft -= ofVec2f(0.001f, 0.001f);
    botRight += ofVec2f(0.001f, 0.001f);
}

void Quad::draw(){

    ofSetRectMode(OF_RECTMODE_CORNER);
    ofNoFill();

    glm::vec2 p;
    p.x = topLeft.x;
    p.y = topLeft.y;
    glm::vec2 p2;
    p2.x = botRight.x;
    p2.y = botRight.y;

    ofDrawRectangle(p,p2.x - p.x, p2.y - p.y);

    if(topLeftTree != nullptr) topLeftTree->draw();
    if(botLeftTree != nullptr) botLeftTree->draw();
    if(topRightTree != nullptr)topRightTree->draw();
    if(botRightTree != nullptr)botRightTree->draw();

}

void Quad::reset()
{
    sounds.clear();

    if ( topLeftTree != nullptr ) {
        topLeftTree->reset();
        botLeftTree->reset();
        topRightTree->reset();
        botRightTree->reset();
    }

    topLeftTree = nullptr;
    botLeftTree = nullptr;
    topRightTree = nullptr;
    botRightTree = nullptr;
}

void Quad::reprocess(const vector< shared_ptr<Sound> > &sounds)
{
    vector< shared_ptr<Sound> > newSounds(sounds);
    reset();
    calculateMainQuadSize(sounds);

    for ( shared_ptr<Sound> s : newSounds ) {
        insert(s);
    }
}

