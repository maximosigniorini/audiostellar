#include "SessionManager.h"

SessionManager* SessionManager::instance = nullptr;

SessionManager::SessionManager(){

#ifdef TARGET_LINUX
    SETTINGS_FILENAME = ofFilePath::getUserHomeDir() + "/.config/" + SETTINGS_FILENAME;
#else
    SETTINGS_FILENAME = ofToDataPath(SETTINGS_FILENAME);
#endif

    units = Units::getInstance();
    sounds = Sounds::getInstance();
    midiServer = MidiServer::getInstance();
    audioEngine = AudioEngine::getInstance();
//    voices = Voices::getInstance();
    
    loadSettings();

}

void SessionManager::loadInitSession()
{
    if ( lastCrashed ) {
        loadDefaultSession(false);
        lastCrashed = false;
        return;
    }

    //Load last recent project
    if(areAnyRecentProjects(recentProjects)){
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = recentProjects[recentProjects.size() - 1];
        loadSession(opts);
    }

    if ( !getSessionLoaded() ) {
        loadDefaultSession();
    }
}

void SessionManager::loadSettings(){
    if(!settingsFileExist()){
      createSettingsFile();
    }

    //LOAD FILE
    ofxJSONElement file;
    bool loaded = file.open(SETTINGS_FILENAME);

    //VALIDATION
    if(!loaded){
    ofLogNotice("ERROR", "error loading settings json file");
    }

    if(file["recentProjects"]       == Json::nullValue ||
       file["lastMidiDevice"]       == Json::nullValue ||
       file["useOriginalPositions"] == Json::nullValue ||
       file["showSoundFilenames" ]  == Json::nullValue ||
       file["audioSettings" ]       == Json::nullValue) {

        ofLogNotice("ERROR",
                    "corrupted settings file. Deleting and creating new...");

        ofFile::removeFile(SETTINGS_FILENAME);
        createSettingsFile();

    } else {
        settingsFile = file;
    }

    //SET SETTINGS ACCORDING TO FILE

    //recentProjects
    recentProjects = loadRecentProjects();

    //set lastMidiDevice
    if(settingsFile["lastMidiDevice"] != ""){
      lastSavedMidiDevice = settingsFile["lastMidiDevice"].asString();
      midiServer->setCurrentMidiDevice(lastSavedMidiDevice);
    }

    //Show sound filenames
    bool showFilenames = settingsFile["showSoundFilenames"].asBool();
    sounds->setShowSoundFilenames(showFilenames);

    //OSC
    if ( settingsFile["OSC_receivePort"].asInt() > 0 ) {
        OscServer::setReceivePort( settingsFile["OSC_receivePort"].asInt() );
    }
    if ( settingsFile["OSC_sendPort"].asInt() > 0 ) {
        OscServer::setSendPort( settingsFile["OSC_sendPort"].asInt() );
    }
    if ( settingsFile["OSC_sendHost"].asString() != "" ) {
        OscServer::setSendHost( settingsFile["OSC_sendHost"].asString() );
    }
    if ( settingsFile["OSC_enable"].asBool() ) {
        OscServer::getInstance()->start();
    }
    
    Gui::getInstance()->setShowStats( settingsFile["show_stats"].asBool() );

    //Crash check
    if ( settingsFile["crashCheck"] != Json::nullValue ) {
        if ( settingsFile["crashCheck"].asBool() == true ) {
            Gui::getInstance()->showCrashMessage();
            lastCrashed = true;
            return;
        }
    }
    saveSettingsFile(true);


    //Audio settings
    startAudioWithCurrentConfiguration();
    
//    if ( settingsFile["numVoices"].asInt() > 0 ) {
//        voices->numVoices = settingsFile["numVoices"].asInt();
//    }

}

void SessionManager::loadSession(datasetLoadOptions opts){
    string datasetPath;

    //OPENING
    if(opts.method == "GUI"){
        datasetPath = userSelectJson();
        if(datasetPath == "CANCELLED"){
            return;
        }
    } else if(opts.method == "TERMINAL" ||
             opts.method == "RECENT"){
        datasetPath = opts.path;
    }

    isDefaultSession = opts.path == ofToDataPath(DEFAULT_SESSION, true);

    //VALIDATION
    datasetResult * loadingDatasetResult = validateDataset(datasetPath);

    //SHOW ERROR IN CASE THERE IS ONE
    if(!loadingDatasetResult->success) {
        Gui::getInstance()->showMessage(
                    loadingDatasetResult->status.c_str(),
                    STRING_ERROR.c_str()
                    );
    } else {
        //SUCCESSFULL LOADING
        AudioEngine::getInstance()->engine.stop();

        if ( sessionLoaded == true ) {
            Units::getInstance()->reset();
            Sounds::getInstance()->reset();
            MasterClock::getInstance()->play();
            sessionLoaded = false;
        }

       sessionLoaded = true;
       ofxJSONElement &file = loadingDatasetResult->data;
       string audioFilesPath = file["audioFilesPath"].asString();

       if ( audioFilesPath == DEFAULT_SESSION_PLACEHOLDER ) {
           audioFilesPath = ofToDataPath( DEFAULT_SESSION_DIRECTORY, true );
       }

       //Safety measure with paths
       file["audioFilesPathAbsolute"] = Utils::resolvePath
                                    (datasetPath,
                                     audioFilesPath);

       jsonFile = &file;
       jsonFilename = datasetPath;

       if (!isDefaultSession) {
           saveToRecentProject(datasetPath);
       }

       sessionAudioDeviceName = file["audioDeviceName"].asString();

       //LOAD SOUNDS
       sounds->loadSounds(file);
       sounds->load(file["sounds"]);

       midiServer->reset();
       midiServer->load(file["midiMapping"]);

       OscServer::getInstance()->reset();
       OscServer::getInstance()->load(file["oscMapping"]);

       if ( file["global"] != Json::nullValue && file["global"]["BPM"] != Json::nullValue ) {
           MasterClock::getInstance()->setBPM( file["global"]["BPM"].asInt() );
       }

       //RESET AND LOAD MODES DATA (MIDI MAPPINGS AND SUCH)
       Json::Value jsonUnits = file["units"];
       if ( jsonUnits != Json::nullValue ) {
           units->load(jsonUnits);
       }
       Units::getInstance()->initUnits();

       //SET WIN TITLE
       setWindowTitleWithSessionName();

       saveSettingsFile(true);

       AudioEngine::getInstance()->engine.start();
    }

}

string SessionManager::userSelectJson(){
    string defaultPath = "";
    if ( !isDefaultSession && !jsonFilename.empty() ) {
        ofFile file(jsonFilename);
        defaultPath = file.getEnclosingDirectory();
    }

    string filename;
    ofFileDialogResult selection = ofSystemLoadDialog("Select JSON file", false, defaultPath);

    if(selection.bSuccess){
        filename = selection.getPath();
    }else{
        filename = "CANCELLED";
    }
    return filename;
}

SessionManager::datasetResult * SessionManager::validateDataset(string datasetPath){
   datasetResult * res = new datasetResult;
   string extension = ofFilePath::getFileExt(datasetPath);

   //Check extension
   if(extension != "json"){
       res->success = false;
       res->status = "This is not a JSON file";
       return res;
   }

   //Check if valid json
   bool successLoading = res->data.open(datasetPath);
   if(!successLoading){
      res->success = false;
      res->status = "Invalid or corrupted JSON file";
      return res;
   }

   //Check if minimum fields exist
   Json::Value soundFilesPath = res->data["audioFilesPath"];
   Json::Value map = res->data["tsv"];

   if(soundFilesPath == Json::nullValue || map == Json::nullValue){
       res->success = false;
       res->status = "Invalid JSON file. Not a valid sound map.";
       return res;
   }

   res->success = true;
   res->status = "OK";
   return res;

}


void SessionManager::loadDefaultSession( bool showWelcomeScreen ){
    if ( ofFile::doesFileExist( ofToDataPath( DEFAULT_SESSION, true ) ) ) {
        datasetLoadOptions opts;
        opts.method = "RECENT";
        opts.path = ofToDataPath(DEFAULT_SESSION, true);
        loadSession(opts);

        if ( showWelcomeScreen ) {
            Gui::getInstance()->showWelcomeScreen();
        }
    } else {
        Gui::getInstance()->showMessage(
                    "Default session not found at " +
                    ofToDataPath( DEFAULT_SESSION, true ) +
                    ".\n\n"
                    "If you've compiled from source, make sure its inside bin/data/",
                    "Warning" );
    }

}


void SessionManager::saveSettingsFile(bool crashCheck){
  Json::Value root = Json::Value(Json::objectValue);

   //Recent Projects
   Json::Value recent = Json::Value(Json::arrayValue);
   for(unsigned int i = 0 ; i < recentProjects.size(); i++){
       recent[i] = recentProjects[i];
   }

   root["recentProjects"] = recent;

    //Last midi device
    string currentMidiDevice = midiServer->getCurrentMidiDevice();

    if (currentMidiDevice != "Not set"){
        root["lastMidiDevice"] = currentMidiDevice;
    } else if(lastSavedMidiDevice != ""){
        root["lastMidiDevice"] = lastSavedMidiDevice;
    }else{
        root["lastMidiDevice"] = "";
    }

   //Use original positions
   int useOrigPos = sounds->getUseOriginalPositions() ? 1 : 0;
   root["useOriginalPositions"] = useOrigPos;

   //Show sound filenames
   int showSoundFilenames = sounds->getShowSoundFilenames() ? 1 : 0;
   root["showSoundFilenames"] = showSoundFilenames;

   //OSC
   root["OSC_enable"] = OscServer::enable;
   root["OSC_receivePort"] = OscServer::getReceivePort();
   root["OSC_sendPort"] = OscServer::getSendPort();
   root["OSC_sendHost"] = OscServer::getSendHost();

   root["show_stats"] = Gui::getInstance()->getShowStats();

   root["crashCheck"] = crashCheck;

   //Audio settings
   root["audioSettings"] = audioEngine->save();
    
//    root["numVoices"] = voices->numVoices;
    
   settingsFile = root;
   settingsFile.save(SETTINGS_FILENAME, true);
}

bool SessionManager::settingsFileExist(){
  return ofFile::doesFileExist(SETTINGS_FILENAME);
}

void SessionManager::createSettingsFile(){
  Json::Value value = Json::Value (Json::objectValue);

  value["recentProjects"] = Json::Value(Json::arrayValue);
  value["lastMidiDevice"] = "";
  value["useOriginalPositions"] = false;//Json::Value(Json::booleanValue);
  value["showSoundFilenames"] = true; //Json::Value(Json::booleanValue);

  value["OSC_enable"] = false;//Json::Value(Json::booleanValue);
  value["OSC_receivePort"] = Json::Value(Json::intValue);
  value["OSC_sendPort"] = Json::Value(Json::intValue);
  value["OSC_sendHost"] = Json::Value(Json::stringValue);
    
  value["audioSettings"] = Json::Value(Json::objectValue);
  value["numVoices"] = Json::Value(Json::intValue);

  value["show_stats"] = false;

  //seteamos la global
  settingsFile = value;
  settingsFile.save(SETTINGS_FILENAME, true);
}

vector<string> SessionManager::loadRecentProjects(){
    vector<string> r;
    Json::Value recent = Json::Value(Json::arrayValue);

    recent = settingsFile["recentProjects"];
    for(unsigned int i = 0; i < recent.size(); i ++){
      if(recent[i] != Json::nullValue){
        r.push_back(recent[i].asString());
      }
    }

    return r;

}

void SessionManager::saveToRecentProject(string path){
    //Si no hay recientes
    if(recentProjects.size() == 0){
        recentProjects.push_back(path);
    } else {

        for(unsigned int i = 0 ; i < recentProjects.size(); i++){
            if(path == recentProjects[i]){
                recentProjects.erase( recentProjects.begin()+i );
                break;
            }
        }

        recentProjects.push_back(path);
    }
}


void SessionManager::saveSession() {
    ofLog() << "Saving...";

    if ( jsonFile == nullptr ) {
        jsonFile = new ofxJSONElement();
    }

    (*jsonFile)["audioDeviceName"] = AudioEngine::getInstance()->getSelectedDeviceName();

    (*jsonFile)["sounds"] = sounds->save();
    (*jsonFile)["units"] = units->save();
    (*jsonFile)["midiMapping"] = midiServer->save();
    (*jsonFile)["oscMapping"] = OscServer::getInstance()->save();

    (*jsonFile)["global"] = Json::objectValue;
    (*jsonFile)["global"]["BPM"] = MasterClock::getInstance()->getBPM();

    // I don't want to save the absolute path, I remove it and then add again
    Json::Value audioFilesPathAbsolute = (*jsonFile)["audioFilesPathAbsolute"];
    jsonFile->removeMember("audioFilesPathAbsolute");

    jsonFile->save(jsonFilename, true);

    (*jsonFile)["audioFilesPathAbsolute"] = audioFilesPathAbsolute;

    ofLog() << "Done saving";
    Gui::getInstance()->showMessage("Session saved successfully", "Information", true);
}

void SessionManager::startAudioWithCurrentConfiguration()
{
    audioEngine->load(settingsFile);
}

void SessionManager::resetSettings()
{
    createSettingsFile();
    loadSettings();
}

void SessionManager::saveAsNewSession(){
  ofFileDialogResult result = ofSystemSaveDialog(
              ofFilePath::getEnclosingDirectory(jsonFilename) + "/new_session.json", "Save");

  if(result.bSuccess){
    string path = result.getPath();
    
    if(!ofIsStringInString(path, ".json")){
        path = path + ".json";
    }

    jsonFilename = path;

    if ( isDefaultSession ) {
        (*jsonFile)["audioFilesPath"] = DEFAULT_SESSION_PLACEHOLDER;
    } else {
        (*jsonFile)["audioFilesPath"] = (*jsonFile)["audioFilesPathAbsolute"];
    }

    isDefaultSession = false;
    saveSession();
    setWindowTitleWithSessionName();
    saveToRecentProject(jsonFilename);
  }
}


vector <string> SessionManager::getRecentProjects(){
        vector<string> invertedRecent;
        for(int i = recentProjects.size() - 1; i >= 0; i --){
                invertedRecent.push_back(recentProjects[i]);
        }
        return invertedRecent;
}

bool SessionManager::getSessionLoaded(){
    return sessionLoaded;
}

bool SessionManager::areAnyRecentProjects(vector<string> recentProjects){
   return recentProjects.size() > 0 &&
           recentProjects[recentProjects.size() - 1] != "";
}

void SessionManager::exit(){
   saveSettingsFile(false);
}

void SessionManager::setWindowTitleWithSessionName(){
    ofSetWindowTitle("AudioStellar // " +
                     ofFilePath::getFileName( jsonFilename ) +
                     " (" + ofToString(sounds->getSounds().size()) + " sounds)" );
}

SessionManager *SessionManager::getInstance()
{
    if ( instance == nullptr ) {
        instance = new SessionManager();
    }

    return instance;
}

void SessionManager::keyPressed(ofKeyEventArgs & e) {
}
