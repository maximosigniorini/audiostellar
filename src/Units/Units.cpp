#include "Units.h"
#include "ExplorerUnit.h"
#include "SequenceUnit.h"
#include "ParticleUnit.h"
#include "OscUnit.h"
#include "../GUI/ExtraWidgets.h"

Units* Units::instance = nullptr;

Units::Units() {
    masterVolume.format = VOLUME_FORMAT;

    ofAddListener( AudioEngine::getInstance()->soundCardInit, this, &Units::onSoundCardInit );
}

void Units::selectUnit(shared_ptr<Unit> unit)
{
    if ( unit == selectedUnit ) return;

    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i] == selectedUnit ) {
            currentUnits[i]->onUnselectedUnit();
            break;
        }
    }
    selectedUnit = unit;
    unit->onSelectedUnit();

}

void Units::setIndexesToUnits()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->setIndex(i);

        OscServer::getInstance()->mixerChangedVolume(i, currentUnits[i]->volume.get());
        OscServer::getInstance()->mixerChangedPan(i, currentUnits[i]->pan.get());
        OscServer::getInstance()->mixerChangedSolo(i, currentUnits[i]->isSolo());
        OscServer::getInstance()->mixerChangedMute(i, currentUnits[i]->isMuted());
    }
}

void Units::loadUnit(shared_ptr<Unit> unit, const Json::Value &jsonUnit)
{
    unit->load(jsonUnit["params"]);
    unit->effects.load(jsonUnit["effects"]);

    unit->volume.set( jsonUnit["volume"].asFloat() );
    unit->pan.set( jsonUnit["pan"].asFloat() );

    unit->setSolo( jsonUnit["solo"].asBool() );
    unit->setMute( jsonUnit["mute"].asBool() );

    // was session saved with the same device that is selected?
    if ( SessionManager::getInstance()->sessionAudioDeviceName ==
         AudioEngine::getInstance()->getSelectedDeviceName() &&
         jsonUnit["output"].asInt() != 0 ) {

        unit->selectedOutput = jsonUnit["output"].asInt();
    }

    unit->loadBehaviors( jsonUnit["behaviors"] );

    if ( jsonUnit["restrictToCluster"] != Json::nullValue ) {
        int restrictToCluster = jsonUnit["restrictToCluster"].asInt();
        if ( restrictToCluster != -1 ) {
            unit->restrictToCluster = restrictToCluster;
            unit->restrictedClusterBB = Sounds::getInstance()->getClusterBB(restrictToCluster);
        }
    }

    currentUnits.push_back(unit);
}

void Units::beforeDraw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->beforeDraw();
        }

        currentUnits[i]->beforeDraw();
    }
}

bool Units::needToShowConfig()
{
    return selectedUnit != nullptr;
}
void Units::draw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->draw();
        }
        currentUnits[i]->draw();
    }
}

void Units::drawGui()
{
    if ( currentUnits.size() > 0 ) {
        float scrollHeight = min( (UNITS_UNIT_HEIGHT+5)*currentUnits.size(),
                                  ofGetHeight() * 0.78f);

        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;

        ImGui::BeginChild("unitsScroll",
                          ImVec2(UNITS_WINDOW_WIDTH,scrollHeight),
                          false, window_flags);

        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            bool selected = selectedUnit == unit;
            string unitMenuName = "unitMenu" + ofToString(i);

    //        ImGui::BeginGroup();
            if (selected)
                ImGui::PushStyleColor(ImGuiCol_ChildBg,  UNITS_UNIT_BGCOLOR );

            ImGui::BeginChild(("##unit" + ofToString(i)).c_str(),
                              ImVec2(UNITS_WINDOW_WIDTH, UNITS_UNIT_HEIGHT),
                              true);

            if (selected)
                ImGui::Text("%s", unit->getUnitName().c_str());
            else
                ImGui::TextDisabled("%s", unit->getUnitName().c_str());


            if ( ImGui::IsItemClicked(1) ) {
                ImGui::OpenPopup( unitMenuName.c_str() );
            }

            if (ImGui::BeginPopup(unitMenuName.c_str())) {
                bool removedUnit = false;

                if (ImGui::MenuItem("Remove Unit")) {
                    removeUnit(unit);
                    removedUnit = true;
                    ImGui::CloseCurrentPopup();
                }
                ImGui::EndPopup();

                if ( removedUnit ) {
                    if ( selected ) {
                        ImGui::PopStyleColor();
                    }
                    ImGui::EndChild();
                    break;
                }
            }

            ImGui::SameLine(ImGui::GetWindowWidth() - 41);
            //mute
            unit->uberMute.draw();
            ImGui::SameLine(ImGui::GetWindowWidth() - 23);
           //solo
            unit->uberSolo.draw();

            if ( unit->needsToProcessSolo ) {
                processSoloMute();
                unit->needsToProcessSolo = false;
            }

            ImGui::BeginGroup();
                float volumeWidth = UNITS_WINDOW_WIDTH * 0.86f;
                ImGui::PushItemWidth( volumeWidth );

                unit->volume.draw();

                if ( ImGui::IsItemClicked() ) {
                    selectUnit(unit);
                }

                //outputs
                ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(8.f,0.6f));
    //            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 0.7f);
                ImGui::PushItemWidth( UNITS_WINDOW_WIDTH * 0.4f );
    //            ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .8f, .8f, 1.f));
    //            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(1.f, .0f, .0f, 1.f));
                if ( AudioEngine::getInstance()->hasStarted() && unit->selectedOutput > -1 ) {
                    ofxImGui::AddCombo( unit->selectedOutput, AudioEngine::getInstance()->getEnabledOutputs() );
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT );
                } else {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
                    ImGui::Text("No out!");
                    ImGui::PopStyleColor();
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT_NO_OUT );
                }

                ImGui::PopStyleVar(1);
    //            ImGui::PopStyleColor();

                //pan
                ImGui::SameLine( UNITS_WINDOW_WIDTH * 0.46f );
                ImGui::PushItemWidth( UNITS_WINDOW_WIDTH * 0.4f );

                unit->pan.draw();
            ImGui::EndGroup();

            ImGui::SameLine( volumeWidth + 11.f);
            ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
                                   unit->vuPeakPreL.meter_output(),
                                   ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
            ImGui::SameLine(0.f, 1.f);
            ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
                                   unit->vuPeakPreR.meter_output(),
                                   ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );

            ImGui::EndChild();

            if ( ImGui::IsItemClicked() ) {
                selectUnit(unit);
            }

            if (selected) {
                ImGui::PopStyleColor();
            }

    //        ImGui::EndGroup();
        }

        if ( updateScroll ) {
            ImGui::SetScrollHereY( 1.0f );
            updateScroll = false;
        }

        ImGui::EndChild();
    } else {
        ImGui::BeginChild("unitsScrollDummy",
                          ImVec2(UNITS_WINDOW_WIDTH,20));

        ImGui::Text("Add a unit to start.");

        ImGui::EndChild();
    }

    // Add unit
    if ( ImGui::Button("+ Add") ) {
        ImGui::OpenPopup("addUnit");
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_ADD );

    if (ImGui::BeginPopup("addUnit")) {
        if (ImGui::MenuItem("Explorer Unit")) {
            addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
            ImGui::CloseCurrentPopup();
        }

        if (ImGui::MenuItem("Sequence Unit")) {
            addUnit( shared_ptr<Unit>(new SequenceUnit()) );
            ImGui::CloseCurrentPopup();
        }

        if (ImGui::MenuItem("Particle Unit")) {
            addUnit( shared_ptr<Unit>(new ParticleUnit()) );
            ImGui::CloseCurrentPopup();
        }

        if (ImGui::MenuItem("OSC Unit")) {
            addUnit( shared_ptr<Unit>(new OscUnit()) );
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void Units::update() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->update();
        }
        currentUnits[i]->update();
    }
}

void Units::reset()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->die();
    }
    currentUnits.clear();

//    for(unsigned int i = 0; i < removedUnits.size(); i++) {
//        removedUnits[i]->die();
//    }
//    removedUnits.clear();
}

void Units::drawSelectedUnitSettings(){
    if ( selectedUnit == nullptr ) return;

    selectedUnit->drawGui();
    drawBehaviors();
    drawRestrictToCluster();
    drawEffectsSettings();
}

void Units::drawRestrictToCluster()
{
    if ( ExtraWidgets::Header("Cluster", false) ) {
        ImGui::Text("Restrict to cluster:");
        ImGui::SameLine();

        if ( selectedUnit->restrictToCluster >= 0 ) {
            ImGui::Text( "%s", ofToString( selectedUnit->restrictToCluster ).c_str() );
            ImGui::SameLine();
            if ( ImGui::SmallButton("x") ) {
                selectedUnit->restrictToCluster = -1;
                selectedUnit->restrictedClusterBB = ofRectangle();
            }
        } else {
            if ( !selectedUnit->restrictToClusterSelecting ) {
                if ( ImGui::SmallButton("Select") ) {
                    selectedUnit->restrictToClusterSelecting = true;
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_CLUSTER_RESTRICT);

            } else {
                ImGui::Text("...");
            }
        }

        ImGui::NewLine();
    }
}

void Units::drawEffectsSettings()
{
    if ( selectedUnit == nullptr ) return;

    if ( ExtraWidgets::Header("Effects") ) {
        if ( ImGui::Button("+ Add") ) {
            ImGui::OpenPopup("addEffect");
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_EFFECT_ADD);
        ImGui::NewLine();
        selectedUnit->effects.drawGui();

        if (ImGui::BeginPopup("addEffect")) {
            if (ImGui::MenuItem("VAFilter")) {
                addEffect( new VAFilterUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("BasiVerb")) {
                addEffect( new BasiVerbUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("Delay")) {
                addEffect( new DelayUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("DimensionChorus")) {
                addEffect( new DimensionChorusUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("Saturator")) {
                addEffect( new SaturatorUI() );
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
    }
}

void Units::drawBehaviors()
{
    if ( selectedUnit == nullptr ) return;

    if ( !selectedUnit->behaviors.empty() ) {
        bool allGUISeparate = true;

        for ( auto * b : selectedUnit->behaviors ) {
            if ( b->drawGUISeparate ) {
                b->drawGui();
            } else {
                allGUISeparate = false;
            }
        }

        // Quick fix: I'm putting "OSC" here instead of "Extra" since
        // for now is the only "extra behavior"
        if ( !allGUISeparate && ExtraWidgets::Header("OSC", false) ) {
            for ( auto * b : selectedUnit->behaviors ) {
                if ( !b->drawGUISeparate ) {
                    b->drawGui();
                }
            }

            ImGui::NewLine();
        }
    }
}

void Units::drawMasterVolume()
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowPos = ImVec2(ofGetWidth() - 148, -8);
    mainSettings.lockPosition = true;

    if ( ofxImGui::BeginWindow("masterVolume", mainSettings, window_flags) ) {
        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());
//        ImGui::Text("Master");
//        ImGui::SameLine();

//        ImGui::SameLine( volumeWidth + 11.f);
//        ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
//                               unit->vuPeakPreL.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
//        ImGui::SameLine(0.f, 1.f);
//        ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
//                               unit->vuPeakPreR.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );

        if ( AudioEngine::getInstance()->hasStarted() ) {
            ImGui::PushItemWidth( 100.f );

            ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(.13f, .24f, .17f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImVec4(.17f, .35f, .24f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_FrameBgActive, ImVec4(.13f, .49f, .32f, 1.f));

            ImGui::PushStyleColor(ImGuiCol_SliderGrab, ImVec4(.0f, .6f, .38f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_SliderGrabActive, ImVec4(.0f, .67f, .42f, 1.f));
            masterVolume.draw();
            ImGui::PopStyleColor(5);
        } else {
            ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
            ImGui::Dummy(ImVec2(1.f, 0.5f));
            ImGui::Text("Audio disabled");
            ImGui::PopStyleColor();
        }
    }
    ImGui::PopStyleColor(2);
    ofxImGui::EndWindow(mainSettings);
}

Units *Units::getInstance()
{
    if(instance == nullptr){
        instance = new Units();
    }
    return instance;
}

void Units::initUnits() {
    if ( currentUnits.empty() ) {
        addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
    }

    selectUnit(currentUnits[0]);
}

void Units::addUnit(shared_ptr<Unit> newUnit)
{
    currentUnits.push_back(newUnit);
    selectUnit(newUnit);
    setIndexesToUnits();

    updateScroll = true;
}

void Units::removeUnit(shared_ptr<Unit> unit)
{
    if ( unit != nullptr ) {
        bool wasSelected = false;
        std::vector<shared_ptr<Unit>>::iterator position =
                std::find(currentUnits.begin(), currentUnits.end(), unit);

        if (position != currentUnits.end()) {
            wasSelected = *position == selectedUnit;
//            (*position)->die();
            //instead of deleting unit for real, put it in another vector
            //this is much faster and can be used for ctrl-z in the future
            removedUnits.push_back( unit );
            currentUnits.erase(position);
            unit->remove();

            setIndexesToUnits();

            if ( wasSelected ) {
                if ( !currentUnits.empty() ) {
                    if ( position != currentUnits.begin() ) {
                        selectUnit(*(position-1));
                    } else {
                        selectUnit(currentUnits[0]);
                    }
                }
                else {
                    selectedUnit = nullptr;
                }
            }
        }
    }
}

void Units::addEffect(EffectUI *newEffect)
{
    if ( selectedUnit == nullptr ) return;

//    selectedUnit->unpatch();
    selectedUnit->effects.addEffect(newEffect);
//    selectedUnit->patch();
}

UberSlider * Units::getUnitVolume(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->volume);
    }

    return nullptr;
}

UberSlider *Units::getUnitPan(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->pan);
    }

    return nullptr;
}

UberToggle *Units::getUnitMute(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberMute);
    }

    return nullptr;
}

UberToggle *Units::getUnitSolo(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberSolo);
    }

    return nullptr;
}

void Units::setUnitVolume(unsigned int index, float v)
{
    if ( index < currentUnits.size() ) {
        currentUnits[index]->setVolume(v);
    } else {
        ofLog() << "OSC Error: Unit index does not exist";
    }
}

void Units::muteSound(shared_ptr<Sound> s)
{
    for ( auto &u : currentUnits ) {
        u->muteSound(s);
    }
}

Json::Value Units::save()
{
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = currentUnits[i]->getUnitName();
        jsonUnit["volume"] = currentUnits[i]->volume.get();
        jsonUnit["mute"] = currentUnits[i]->mute;
        jsonUnit["solo"] = currentUnits[i]->solo;
        jsonUnit["output"] = currentUnits[i]->selectedOutput.get();
        jsonUnit["pan"] = currentUnits[i]->pan.get();
        jsonUnit["params"] = currentUnits[i]->save();
        jsonUnit["effects"] = currentUnits[i]->effects.save();
        jsonUnit["behaviors"] = currentUnits[i]->saveBehaviors();
        jsonUnit["restrictToCluster"] = currentUnits[i]->restrictToCluster;

        root.append(jsonUnit);
    }
    return root;
}

void Units::load(Json::Value jsonUnits)
{
    for ( int i = 0 ; i < jsonUnits.size() ; i++ ) {
        Json::Value jsonUnit = jsonUnits[i];

        if ( jsonUnit["name"].asString() == "Explorer Unit" ) {
            loadUnit(shared_ptr<Unit>(new ExplorerUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Sequence Unit" ) {
            loadUnit(shared_ptr<Unit>(new SequenceUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Particle Unit" ) {
            loadUnit(shared_ptr<Unit>(new ParticleUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "OSC Unit" ) {
            loadUnit(shared_ptr<Unit>(new OscUnit()), jsonUnit);
        }
    }
    setIndexesToUnits();
}

void Units::mousePressed(int x, int y, int button) {

    if ( selectedUnit == nullptr ) return;

    //this happens only when a popup is opened but the mouse is not hovering it
    if (! (Gui::getInstance()->isMouseHoveringGUI() && !ImGui::IsAnyWindowHovered()) ) {
        if ( !selectedUnit->restrictToClusterSelecting ) {
            for ( auto *behavior : selectedUnit->behaviors ) {
                behavior->mousePressed( ofVec2f(x,y), button );
            }
            selectedUnit->mousePressed( ofVec2f(x,y), button );
        } else {
            int selectedCluster = Sounds::getInstance()->getHoveredClusterID();

            if ( selectedCluster >= 0 ) {
                selectedUnit->restrictToCluster = selectedCluster;
                selectedUnit->restrictedClusterBB = Sounds::getInstance()->getClusterBB(selectedCluster);
            }

            selectedUnit->restrictToClusterSelecting = false;
        }
    }
}

void Units::keyPressed(ofKeyEventArgs & e)
{
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->keyPressed(e);
    }
    selectedUnit->keyPressed(e);
}

void Units::mouseDragged(int x, int y, int button){
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseDragged(ofVec2f(x,y), button);
    }
    selectedUnit->mouseDragged(ofVec2f(x,y), button);
}

void Units::mouseReleased(int x, int y , int button) {
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseReleased(ofVec2f(x,y));
    }
    selectedUnit->mouseReleased(ofVec2f(x,y));
}

void Units::mouseMoved(int x, int y) {
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseMoved(ofVec2f(x,y));
    }
    selectedUnit->mouseMoved(ofVec2f(x,y));
}

void Units::midiMessage(MIDIMessage m) {
    //Si está en midiLearn sólo le manda mensaje al modo activo
    if(MidiServer::getInstance()->getMIDILearn()){
        if ( selectedUnit == nullptr ) return;
        selectedUnit->midiMessage(m);
    }else{
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            currentUnits[i]->midiMessage(m);
        }
    }
}

shared_ptr<Unit> Units::getSelectedUnit() {
    return selectedUnit;
}

string Units::getSelectedUnitName() {
    return selectedUnit->getUnitName();
}

void Units::processSoloMute() {
    bool isAnyUnitSoloed = false;
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->isSolo() ) {
            isAnyUnitSoloed = true;
            break;
        }
    }
    if ( isAnyUnitSoloed ) {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->changeMute(!unit->isSolo(), true);
        }
    } else {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->changeMute(false, true);
        }
    }
}

void Units::onSoundCardInit(int &deviceID)
{
    masterGain.disconnectOut();
    unsigned int countOutputs = AudioEngine::getInstance()->getAudioOutNumChannels(deviceID);
    for ( unsigned int i = 0 ; i < countOutputs ; i++ ) {
        masterGain.ch(i) >> AudioEngine::getInstance()->engine.audio_out(i);
    }

    //Soundcard has changed for one with less outputs. Unit has a selected output that exists no more.
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->selectedOutput >= countOutputs/2 ) {
            currentUnits[i]->selectedOutput = 0;
        }
    }
}
