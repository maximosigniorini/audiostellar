#include "ExplorerUnit.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"

ExplorerUnit::ExplorerUnit() {
    sounds = Sounds::getInstance();

    behaviors.push_back( new UnitBehaviorOSC(this) );
    behaviors.push_back( new UnitBehaviorDragToPlay(this) );

    trajectory = new UnitBehaviorTrajectory(this);
    trajectory->onlyEvent = true;
    ofAddListener(trajectory->onTrajectory, this, &ExplorerUnit::onTrajectory);
    behaviors.push_back( trajectory );
}

ExplorerUnit::~ExplorerUnit()
{
    ofRemoveListener(trajectory->onTrajectory, this, &ExplorerUnit::onTrajectory);
}

void ExplorerUnit::reset() {
    if(!midiMappings.empty()) {
        midiMappings.clear();
    }
}

void ExplorerUnit::mousePressed(ofVec2f p, int button) {
    shared_ptr<Sound> hoveredSound = sounds->getHoveredSound();
    
    if ( hoveredSound != nullptr ) {
        if ( button == 0 ) {
            playSound(hoveredSound);
        }
    }
}

void ExplorerUnit::mouseDragged(ofVec2f p, int button){

}

void ExplorerUnit::midiMessage(MIDIMessage m)
{
//    if ( m.status == "Note On" ) {
//        if ( MidiServer::midiLearn && sounds->lastPlayedSound != nullptr ) {
//            midiMappings[ m.pitch ] = sounds->lastPlayedSound->id;
//            MidiServer::midiLearn = false;
//        }
        
//        if ( midiMappings.find(m.pitch) != midiMappings.end() ) {
//            midiTriggeredSound = sounds->getSoundById( midiMappings[ m.pitch ] );
//        }
    //    }
}

void ExplorerUnit::onTrajectory(ofVec2f &position)
{
    shared_ptr<Sound> nearestSound = sounds->getNearestSound(position);
    if (nearestSound != nullptr) {
        if (nearestSound != lastPlayedTrajectory) {
            if ( ofRandom(1.f) <= trajectory->probability.get() ) {
                playSound(nearestSound);
            }

            lastPlayedTrajectory = nearestSound;
        }
    }
    else {
        lastPlayedTrajectory = nullptr;
    }
}

Json::Value ExplorerUnit::save()
{
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value jsonMidiMappings = Json::Value( Json::objectValue );
    
    for ( auto midiMapping : midiMappings) {
        jsonMidiMappings[ ofToString(midiMapping.first) ] = midiMapping.second;
    }
    
    root["midiMappings"] = jsonMidiMappings;
    return root;
}

void ExplorerUnit::load(Json::Value jsonData)
{
    Json::Value jsonMidiMappings = jsonData["midiMappings"];
    if ( jsonMidiMappings != Json::nullValue ) {
        for( Json::Value::iterator itr = jsonMidiMappings.begin() ; itr != jsonMidiMappings.end() ; itr++ ) {
            ofLog() << itr.key() << ":" << *itr;
            midiMappings[ ofToInt( itr.key().asString() ) ] = (*itr).asInt();
        }
    }
}

void ExplorerUnit::drawGui() {

}
