#pragma once

#define DEFAULT_TEMPO 120
#define QTY_BARS 6
#define QTY_STEPS 16

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxJSON.h"

#include "Unit.h"
#include "../Sound/Sounds.h"
#include "../Utils/Utils.h"
#include "../Servers/MidiServer.h"
#include "../Servers/OscServer.h"
#include "../Servers/MasterClock.h"
#include "../GUI/UI.h"
#include "../GUI/UberSlider.h"

class MidiServer;


class SequenceUnit : virtual public Unit {
private:
    const vector<string> strBars {"1/4 bar", "1/2 bar","1 bar", "2 bars", "3 bars", "4 bars"};
    static int intBars[QTY_BARS];
    ofParameter<int> selectedBars { "Bars", 2, 0, QTY_BARS }; // = 2;

    UberSlider probability {"Probability",1.f,0,1.f, &Tooltip::UNIT_SEQUENCE_PROBABILITY};
    UberSlider offset {"Offset",0,0,15.f, &Tooltip::UNIT_SEQUENCE_OFFSET};
//    ofParameter<int> offset {"Offset",0,0,15};

    vector<shared_ptr<Sound>> secuencia;
    shared_ptr<Sound> * secuenciaEnTiempo = nullptr;
    void onNeedToProcess(float & a);
    void onNeedToProcessInt(int & a);

    ofPolyline polyLine;
    bool isProcessing = false;
    bool playing = true;

    int currentStep = -1;

public:
    SequenceUnit();
//    ~SequenceUnit();

    string getUnitName() { return "Sequence Unit"; }

    void setIndexExtra(int i);

    Json::Value save();
    void load( Json::Value jsonData );

    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);

    void reset();
    void die();
    void beforeDraw();
    void draw();
    void drawGui();
    void midiMessage( MIDIMessage m );

    void onTempo(int & frame);

    void processSequence();
    void processSequenceOffset();
    void clearSequence();
    int getCantSteps();

    void toggleSound( shared_ptr<Sound> sound, bool doProcessSequence = true );

    void onSelectedUnit();
    void onUnselectedUnit();
    void setSelectAllSounds(bool selected = true);
};
