#pragma once

#include "ofMain.h"
#include "Unit.h"
#include "Sounds.h"
#include "Voices.h"

class OscUnit: virtual public Unit{
private:
    ofVec2f oscPosition;
    int oscCursorOpacity = 255;

    void oscPlay(float volume);

    const string OSC_PLAY = "/play/xy";
    const string OSC_PLAY_X = "/play/x";
    const string OSC_PLAY_Y = "/play/y";
    const string OSC_PLAY_ID = "/play/id";
    const string OSC_PLAY_CLUSTER = "/play/cluster";
    const string OSC_PLAY_CLUSTER_BY_SOUND_ID = "/play/clusterBySoundId";

    char prefix[64] = "";
public:
    OscUnit();

    string getUnitName() { return "OSC Unit"; }

    void onOSCMessage(ofxOscMessage &m);

    void update();
    void draw();
    void drawGui();
    void die();

    Json::Value save();
    void load( Json::Value jsonData );
};
