#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

#include "./Unit.h"
#include "../Utils/Utils.h"
#include "../Utils/Tooltip.h"
#include "../GUI/ExtraWidgets.h"
#include "../GUI/UberToggle.h"

#include "../Effects/VAFilterUI.h"
#include "../Effects/BasiVerbUI.h"
#include "../Effects/DimensionChorusUI.h"
#include "../Effects/SaturatorUI.h"
#include "../Effects/DelayUI.h"

#include "../GUI/UI.h"

#define UNITS_WINDOW_WIDTH 200.0f
#define UNITS_UNIT_HEIGHT 71.0f
#define UNITS_UNIT_BGCOLOR ImVec4(0.0f, 0.0f, 0.04f, 1.f)

#define VOLUME_MIN -47.99f
#define VOLUME_MAX 12.f
#define VOLUME_FORMAT "%.2fdB"

class MidiServer;
class Unit;

class Units {
private:
    static Units* instance;
    Units();

    shared_ptr<Unit> selectedUnit = nullptr;

    bool showOutputMenu = false;
    bool updateScroll = false; //move units scroll to bottom when needed

    void selectUnit( shared_ptr<Unit> unit );
    void setIndexesToUnits();
    void loadUnit(shared_ptr<Unit> unit, const Json::Value & jsonUnit);
public:
    static Units* getInstance();

    vector< shared_ptr<Unit> > currentUnits;
    vector< shared_ptr<Unit> > removedUnits; //removing the units for real takes too much time

    pdsp::ParameterGain masterGain;
    UberSlider masterVolume { masterGain.set("OUT##MasterVolume", 0.f, VOLUME_MIN, VOLUME_MAX),
                        &Tooltip::UNITS_MASTER_VOLUME };

    void initUnits();
    void addUnit( shared_ptr<Unit> unit );
    void removeUnit( shared_ptr<Unit> unit );
    void addEffect( EffectUI *newEffect );
    UberSlider * getUnitVolume(unsigned int index);
    UberSlider * getUnitPan(unsigned int index);
    UberToggle * getUnitMute(unsigned int index);
    UberToggle * getUnitSolo(unsigned int index);
    void setUnitVolume(unsigned int index, float v);
//    void setUnitPan(unsigned int index, float v);
//    void setUnitMute(unsigned int index, float v);
//    void setUnitSolo(unsigned int index, float v);
    void muteSound( shared_ptr<Sound> s );

    Json::Value save();
    void load(Json::Value jsonUnits);

    bool needToShowConfig();
    void beforeDraw();
    void draw();
    void drawGui();
    void drawSelectedUnitSettings();
    void drawRestrictToCluster();
    void drawEffectsSettings();
    void drawBehaviors();
    void drawMasterVolume();
    void update();
    void reset();

    void mousePressed(int x, int y, int button);
    void keyPressed(ofKeyEventArgs & e);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y , int button);
    void mouseMoved(int x, int y);

    void midiMessage(MIDIMessage m);
    shared_ptr<Unit> getSelectedUnit();
    string getSelectedUnitName();
    
    void processSoloMute();

    void onSoundCardInit( int & deviceID );
};
