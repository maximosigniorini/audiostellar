#include "ParticleUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorTrajectory.h"

ParticleUnit::ParticleUnit(){
    allowExplorerModeDrag = false;
    atModel = 0;// default is walk model
    density.format = "%.0f";

    parameters.push_back(&lifespan);
    parameters.push_back(&velocityX);
    parameters.push_back(&velocityY);
    parameters.push_back(&spreadAmount);
//    parameters.push_back(&volumeSpread);
    parameters.push_back(&emitRate);
    parameters.push_back(&emit);
    parameters.push_back(&emitAreaSize);
    parameters.push_back(&speed);
    parameters.push_back(&density);

    parameters.push_back(&probability);

    behaviors.push_back( new UnitBehaviorOSC(this) );
    behaviors.push_back( new UnitBehaviorTrajectory(this) );

    emit.addListener( this, &ParticleUnit::emitButtonListener );
}

void ParticleUnit::update(){
    if( emitButtonPressed ) {
        if ( emitRandomPosition && emitAreaSize > 0 ) {
            ofVec2f randomDisplacement( ofRandom(-emitAreaSize/2, emitAreaSize/2),
                                        ofRandom(-emitAreaSize/2, emitAreaSize/2) );

            castParticleWithRate( currentCentroid + randomDisplacement );
        } else {
            castParticleWithRate( currentCentroid );
        }
    }

    particles.remove_if([this](const SwarmParticle& p) {
        return p.age <= 0;
    });

    float randomNumber = ofRandom(1.f);

    for ( SwarmParticle &particle : particles ) {
        //Update params of particle
        particle.update();
        particle.lifeSpan = lifespan.get()*10.0;

        if(swarmModels[atModel] == MODEL_SWARM){
          particle.velocity.x = velocityX.get();
          particle.velocity.y = velocityY.get();
          particle.spreadAmount = spreadAmount.get();
        }else if(swarmModels[atModel] == MODEL_EXPLOSION){
          //For gui params that need to be updated every frame
        }

        ofVec2f pos = particle.getPosition();

        if ( emitAreaSize > 0 &&
             ofDist( pos.x, pos.y,
                     currentCentroid.x, currentCentroid.y ) >
             emitAreaSize.get()/2 ) {
            particle.kill();
            continue;
        }

        //Get near sound
        shared_ptr<Sound> soundNearParticle = Sounds::getInstance()->getNearestSound(pos);

        //If exists, play it
        if( soundNearParticle != nullptr &&
            soundNearParticle != particle.lastPlayedSound) {

            particle.lastPlayedSound = soundNearParticle;
            if ( probability == 1.f || randomNumber <= probability ) {
                playSound(soundNearParticle);
            }
        }
    }
}

void ParticleUnit::updateParticlesSize(){
    for ( SwarmParticle &particle : particles ) {
        particle.setRandomSize();
    }
}

void ParticleUnit::draw(){
    if ( selected ) drawCentroid();
    drawArea();

    for ( SwarmParticle &particle : particles ) {
        if ( selected ) {
            particle.draw( ofColor( PARTICLE_COLOR_R, PARTICLE_COLOR_G, PARTICLE_COLOR_B ) );
        } else {
            particle.draw( ofColor( PARTICLE_COLOR_INACTIVE_R, PARTICLE_COLOR_INACTIVE_G, PARTICLE_COLOR_INACTIVE_B ) );
        }
    }
}

void ParticleUnit::drawCentroid()
{
    ofNoFill();
    ofSetColor(255,255,255, 100);
    ofDrawLine((currentCentroid.x), (currentCentroid.y - 4.5f),
               (currentCentroid.x), (currentCentroid.y + 4.5f));
    ofDrawLine((currentCentroid.x - 4.5f), (currentCentroid.y),
               (currentCentroid.x + 4.5f), (currentCentroid.y));
}

void ParticleUnit::drawArea()
{
    if ( emitAreaSize.get() == 0.f ) return;
    ofNoFill();

    if ( selected ) {
        ofSetColor(PARTICLE_COLOR_R,PARTICLE_COLOR_G,PARTICLE_COLOR_B, 255);
    } else {
        ofSetColor(PARTICLE_COLOR_INACTIVE_R,PARTICLE_COLOR_INACTIVE_G,PARTICLE_COLOR_INACTIVE_B, 100);
    }

    ofDrawEllipse( currentCentroid, emitAreaSize.get(), emitAreaSize.get() );
}

void ParticleUnit::drawGui(){

    if (ExtraWidgets::Header("Emitter settings")) {
        emitRate.draw();
        emitAreaSize.draw();
        ImGui::NewLine();
        emit.draw();

        if ( emitAreaSize.get() > 0.f ) {
            ImGui::SameLine();
            ImGui::Checkbox("Randomize position", &emitRandomPosition);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_PARTICLE_EMIT_RANDOM );
        }

        ImGui::NewLine();
    }

    if (ExtraWidgets::Header("Particles behavior")) {
        // Draw model selector
        // TODO: should be an uber radio button...
        for(int i = 0; i < NUM_MODELS; i++){
           if(ImGui::RadioButton(swarmModels[i].c_str(), &atModel , i)){
               particles.clear();
               atModel = i;
           }

           if(i != NUM_MODELS - 1){
               ImGui::SameLine();
           }
        }
        ImGui::NewLine();

        lifespan.draw();
        if(swarmModels[atModel] == MODEL_SWARM){
          ImGui::PushItemWidth(97);
              velocityX.draw();
              ImGui::SameLine();
              velocityY.draw();
              ImGui::SameLine();
              ImGui::Text("Velocity");
          ImGui::PopItemWidth();
          spreadAmount.draw();
        }else if(swarmModels[atModel] == MODEL_EXPLOSION){
          speed.draw();
          density.draw();
        }

        ImGui::NewLine();

        probability.draw();

        ImGui::NewLine();
    }

//    if ( volumeSpread.draw() ) updateParticlesSize();
}

void ParticleUnit::reset(){

}

void ParticleUnit::emitButtonListener(float &v)
{
    emitButtonPressed = v > 0;
}

void ParticleUnit::mousePressed(ofVec2f p, int button){
    if(button == 0){
        currentCentroid = p;
        castParticle(p);
    }
}

void ParticleUnit::mouseDragged(ofVec2f p, int button){
    if(button == 0){
        castParticleWithRate(p);
    }
}

Json::Value ParticleUnit::save()
{
    Json::Value params = Json::Value( Json::objectValue );

    params["lifespan"] = lifespan.get();
    params["velocityX"] = velocityX.get();
    params["velocityY"] = velocityY.get();
    params["spreadAmount"] = spreadAmount.get();
//    params["volumeSpread"] = volumeSpread.get();

    params["emitRate"] = emitRate.get();
    params["emitAreaSize"] = emitAreaSize.get();

    params["density"] = density.get();
    params["speed"] = speed.get();

    params["probability"] = probability.get();

    params["currentCentroid_x"] = currentCentroid.x;
    params["currentCentroid_y"] = currentCentroid.y;

    params["selectedModel"] = atModel;
    params["emitRandomPosition"] = emitRandomPosition;

    Json::Value jsonParticles = Json::Value( Json::arrayValue );

    for ( SwarmParticle &particle : particles ) {
        Json::Value jsonParticle = Json::Value( Json::objectValue );

        jsonParticle["positionX"] = particle.position.x;
        jsonParticle["positionY"] = particle.position.y;

        jsonParticles.append( jsonParticle );
    }
    params["particles"] = jsonParticles;

    return params;
}

void ParticleUnit::load(Json::Value jsonData){
    lifespan.set( jsonData["lifespan"].asFloat() );
    velocityX.set( jsonData["velocityX"].asFloat() );
    velocityY.set( jsonData["velocityY"].asFloat() );
    spreadAmount.set( jsonData["spreadAmount"].asFloat() );
//    volumeSpread.set( jsonData["volumeSpread"].asFloat() );

    emitRate.set( jsonData["emitRate"].asFloat() );
    emitAreaSize.set( jsonData["emitAreaSize"].asFloat() );

    density.set(jsonData["density"].asFloat());
    speed.set(jsonData["speed"].asFloat());

    if ( jsonData["probability"] != Json::nullValue ) {
        probability.set(jsonData["probability"].asFloat());
    }

    if ( jsonData["particles"] != Json::nullValue ) {
        Json::Value jsonParticles = jsonData["particles"];
        for( unsigned int i = 0 ; i < jsonParticles.size() ; i++ ) {
            ofVec2f pos;
            pos.x = jsonParticles[i]["positionX"].asFloat();
            pos.y = jsonParticles[i]["positionY"].asFloat();

            castParticle(pos);
        }
    }

    currentCentroid.x = jsonData["currentCentroid_x"].asFloat();
    currentCentroid.y = jsonData["currentCentroid_y"].asFloat();

    atModel = jsonData["selectedModel"].asInt();
    emitRandomPosition = jsonData["emitRandomPosition"].asBool();
}

void ParticleUnit::castParticle(ofVec2f pos){
  if(swarmModels[atModel] == MODEL_SWARM){
    SwarmParticle p (pos, MODEL_SWARM);
//    p.volumeSpread = volumeSpread.get();
    particles.push_front(p);
  }else if(swarmModels[atModel]  == MODEL_EXPLOSION){
    for(int i = 0; i< density.get(); i++) {
        SwarmParticle p (pos, MODEL_EXPLOSION);
        p.speed = speed.get();
        p.setup();
//        p.volumeSpread = volumeSpread.get();
        particles.push_front(p);
    }
  }
}

void ParticleUnit::castParticleWithRate(ofVec2f pos)
{
    //not exactly "rate" but close enough
    if ( ofGetElapsedTimeMillis() - millisLastEmit > (emitRate.getMax()-emitRate.get()) ) {
        castParticle(pos);
        millisLastEmit = ofGetElapsedTimeMillis();
    }
}


/* -------------- SWARM PARTICLE -------------------- */

ParticleUnit::SwarmParticle::SwarmParticle(ofVec2f position, string model){
    //this->volume = (this->size/sizeScaler) - defaultSize;
    setRandomSize();
    this->position = position;
    lifeSpan = 10.0f;

    this->model = model;
    
    if(model == MODEL_SWARM){
      velocity = ofVec2f(0,0);
      spreadAmount = 2.9f;
    }else if(model == MODEL_EXPLOSION){
      velocity.x = ofRandom(-0.02,0.02);
      velocity.y = ofRandom(-0.02,0.02);
    }
}

void ParticleUnit::SwarmParticle::setup(){
  if(model == MODEL_SWARM){
  /*
   */
  }else if(model == MODEL_EXPLOSION){
    acceleration.x = velocity.x * speed;
    acceleration.y = velocity.y * speed;
  }
}

void ParticleUnit::SwarmParticle::customUpdate(){
    if(model == MODEL_SWARM){

      velocity.x = spreadAmount * ofRandom(-1.0,1.0) + velocity.x;
      velocity.y = spreadAmount * ofRandom(-1.0,1.0) - velocity.y;
      position += velocity;

    } else if(model == MODEL_EXPLOSION){
      velocity += acceleration;
      position += velocity;
    }

    if(sizeAnimStep < 1.0){
        sizeAnimStep += 0.01;
        size = ofLerp(size, tempSize, sizeAnimStep);
//        volume = size/SIZE_SCALER;
    }
}

void ParticleUnit::SwarmParticle::setRandomSize(){
    float r;
    tempSize = DEFAULT_SIZE;
    sizeAnimStep = 0;
    if(volumeSpread > 0.0f){
        r = ofRandom(0, SwarmParticle::SIZE_SCALER);
        tempSize *= r;
    }
}

