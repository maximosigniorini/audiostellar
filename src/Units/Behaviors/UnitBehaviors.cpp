#include "UnitBehaviors.h"

void UnitBehaviors::add(UnitBehavior &behavior) {
    behaviors.push_back(behavior);
}

void UnitBehaviors::beforeDraw() {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.beforeDraw();
    }
}
void UnitBehaviors::draw() {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.draw();
    }
}
void UnitBehaviors::drawGui(){
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.drawGui();
    }
}
void UnitBehaviors::update() {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.update();
    }
}
void UnitBehaviors::mousePressed(ofVec2f p, int button) {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.mousePressed(p, button);
    }
}
void UnitBehaviors::mouseDragged(ofVec2f p, int button) {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.mouseDragged(p, button);
    }
}
void UnitBehaviors::mouseReleased(ofVec2f p) {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.mouseReleased(p);
    }
}
void UnitBehaviors::mouseMoved(ofVec2f p) {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.mouseMoved(p);
    }
}
void UnitBehaviors::keyPressed(ofKeyEventArgs & e) {
    for ( UnitBehavior& behavior : behaviors ) {
        behavior.keyPressed(e);
    }
}
