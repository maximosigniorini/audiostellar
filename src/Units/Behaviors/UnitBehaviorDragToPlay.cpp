#include "UnitBehaviorDragToPlay.h"

UnitBehaviorDragToPlay::UnitBehaviorDragToPlay(Unit *parentUnit) : UnitBehavior(parentUnit)
{
    drawGUISeparate = true;
}

void UnitBehaviorDragToPlay::mouseDragged(ofVec2f p, int button) {
    if(button == 0 || button == -1){
        shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getHoveredSound();

        // -1 is sent if "mouse" is actually OSC
        if ( button == -1 ) {
            hoveredSound = Sounds::getInstance()->getNearestSound(p);
        }

        if ( hoveredSound != nullptr ) {
            if ( hoveredSound != lastPlayedSound ) {
                parent->playSound(hoveredSound);
                lastPlayedSound = hoveredSound;
            }
        } else {
            lastPlayedSound = nullptr;
        }
    } else {
        lastPlayedSound = nullptr;
    }
}
