#pragma once

#include "ofMain.h"
#include "UnitBehavior.h"

class UnitBehaviorTrajectory: public UnitBehavior, ofThread {
private:
    bool timerStarted = false;
    bool recording = false;

    vector <ofVec2f> positions;
    ofVec2f currentPosition;
    int n = 0;

    void recordStroke();
    void playStroke();
    void clearStroke();

    ofTimer timer;
public:
    bool enabled = false;
    UberSlider speed {"Speed", 1.f, 0.01f, 4, &Tooltip::UNIT_BEHAVIOR_TRAJECTORY_SPEED};
    UberSlider probability {"Probability", 1.f, 0.f, 1.0f, &Tooltip::UNIT_BEHAVIOR_TRAJECTORY_PROBABILITY};

    ofEvent<ofVec2f> onTrajectory;
    bool onlyEvent = false; //true if you want to receive just the event and not the mousedrag

    UnitBehaviorTrajectory(Unit * parentUnit);
    ~UnitBehaviorTrajectory();
    void die();

    const string getName() { return "UnitBehaviorTrajectory"; }

    void onTimer();
    void speedListener(float &v);

    Json::Value save();
    void load( Json::Value jsonData );

    void mouseReleased(ofVec2f p);
    void mouseDragged(ofVec2f p, int button);

    void update();
    void draw();
    void drawGui();

    void setSpeed(float multiplier) {
        timer.setPeriodicEvent( 1000000 * 10 / multiplier);
    }
    void startTimer() {
        startThread();
    }
    void stopTimer() {
        stopThread();
    }
    void threadedFunction() {
        while(isThreadRunning()) {
             timer.waitNext();
             onTimer();
        }
    }

//    Json::Value save();
//    void load( Json::Value jsonData );
};
