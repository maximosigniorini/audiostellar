#include "UnitBehaviorTrajectory.h"
#include "ofxImGui.h"
#include "../../GUI/ExtraWidgets.h"

UnitBehaviorTrajectory::UnitBehaviorTrajectory(Unit * parentUnit) :
    UnitBehavior (parentUnit)
{
    speed.format = "x %.2f";
    setSpeed(speed.get());
    drawGUISeparate = true;

    speed.addListener(this, &UnitBehaviorTrajectory::speedListener);
}

UnitBehaviorTrajectory::~UnitBehaviorTrajectory()
{

}

void UnitBehaviorTrajectory::die()
{
    clearStroke();
    waitForThread();
}

void UnitBehaviorTrajectory::onTimer() {
    if (recording) {
        recordStroke();
    }
    else {
        if (positions.size() != 0) {
            playStroke();
        }
    }
}

void UnitBehaviorTrajectory::speedListener(float &v)
{
    setSpeed( speed.get() );
}

void UnitBehaviorTrajectory::recordStroke() {
    positions.push_back(currentPosition);
}

void UnitBehaviorTrajectory::playStroke() {
    ofVec2f position = positions[n];

    if ( !onlyEvent ) {
        if ( ofRandom(1.f) <= probability.get() ) {
            parent->mouseDragged(position, 0);
        }
    }
    onTrajectory.notify(this, position);

    n++;
    n %= positions.size();
}

void UnitBehaviorTrajectory::mouseReleased(ofVec2f position) {
    recording = false;
}

void UnitBehaviorTrajectory::mouseDragged(ofVec2f position, int button) {

    if ( !enabled ) return;

    if ( button == 0 ) {
        if ( !recording && positions.empty() ) {
            positions.clear();
            speed.set(1);
            setSpeed(speed.get());
            recording = true;
            n = 0;
        }

        currentPosition = position;
    }
}

void UnitBehaviorTrajectory::update() {

    if ( Units::getInstance()->getSelectedUnit().get() == (Unit*)this->parent ) {
        if (!timerStarted) {
            startTimer();
            timerStarted = true;
        }
    }
    else {
        if (positions.size() == 0) {
            if (timerStarted) {
                stopTimer();
                timerStarted = false;
            }
        }
    }
}

void UnitBehaviorTrajectory::draw() {

    if (positions.size() != 0) {

        bool selected = Units::getInstance()->getSelectedUnit().get() == (Unit*)this->parent;

        if (positions.size() != 0) {
            if (!recording) {

                ofVec2f position = positions[n];

                if (selected) {
                    ofFill();
                    ofSetColor(232,11,85,255);
                    ofDrawCircle(position.x, position.y, 1.5);
                } else {
                    ofFill();
                    ofSetColor(255,255,255);
                    ofDrawCircle(position.x, position.y, 1.5);
                }
            }
        }
    }
}

void UnitBehaviorTrajectory::drawGui() {
    if ( ExtraWidgets::Header("Trajectory", false) ) {
        if ( !enabled ) {
            if ( ImGui::Button("Create trajectory") ) {
                enabled = true;
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_TRAJECTORY_CREATE );
        } else {
            if ( !recording && positions.empty() ) {
                ImGui::Text("Awaiting coordinates...");
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_TRAJECTORY_AWAITING);
            } else if (recording) {
                ImGui::Text("Recording coordinates...");
            } else {
                speed.draw();
                probability.draw();

                if ( positions.size() > 0 ) {
                    if ( ImGui::Button("Clear trajectory") ) {
                        clearStroke();
                        enabled = false;
                    }
                }
            }
        }

        ImGui::NewLine();
    }
}

void UnitBehaviorTrajectory::clearStroke() {
    positions.clear();
    speed.set(1);
}


Json::Value UnitBehaviorTrajectory::save() {

    Json::Value root = Json::Value( Json::objectValue );
    Json::Value json_positions = Json::Value( Json::arrayValue );

    for (int i = 0; i < positions.size(); i++) {

        Json::Value position = Json::Value( Json::arrayValue );
        position.append( positions[i].x );
        position.append( positions[i].y );

        json_positions.append(position);

    }

    root["positions"] = json_positions;
    root["speed"] = speed.get();
    root["probability"] = probability.get();
    return root;

}

void UnitBehaviorTrajectory::load(Json::Value jsonData) {

    positions.clear();

    Json::Value json_positions = jsonData["positions"];

    if ( json_positions != Json::nullValue ) {
        for (int i = 0; i < json_positions.size(); i++) {
            Json::Value position = json_positions[i];
            ofVec2f p;
            p.x = position[0].asFloat();
            p.y = position[1].asFloat();

            positions.push_back(p);
        }

        if(positions.size() != 0) {
            enabled = true;
            startTimer();
            timerStarted = true;
            playStroke();
        }
    }

    Json::Value json_speed = jsonData["speed"];
    if ( json_speed != Json::nullValue ) {
        speed.set(json_speed.asFloat());
        setSpeed(speed.get());
    }

    if ( jsonData["probability"] != Json::nullValue ) {
        probability.set( jsonData["probability"].asFloat() );
    }
}

