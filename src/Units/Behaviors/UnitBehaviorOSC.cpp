#include "UnitBehaviorOSC.h"

UnitBehaviorOSC::UnitBehaviorOSC(Unit * parentUnit)
    : UnitBehavior(parentUnit)
{
    ofAddListener( OscServer::getInstance()->oscEvent, this, &UnitBehaviorOSC::onOSCMessage );
}

UnitBehaviorOSC::~UnitBehaviorOSC()
{
    ofRemoveListener( OscServer::getInstance()->oscEvent, this, &UnitBehaviorOSC::onOSCMessage );
}

void UnitBehaviorOSC::onOSCMessage(ofxOscMessage &m)
{
    if ( !enabled ) return;

    string oscPlay;
    string oscPlayX;
    string oscPlayY;

    if ( !strncmp(prefix, "", 64) ) {
        oscPlay = OSC_PLAY;
        oscPlayX = OSC_PLAY_X;
        oscPlayY = OSC_PLAY_Y;
    } else {
        string p = string(prefix);

        oscPlay = "/";
        oscPlay.append(p);
        oscPlay.append(OSC_PLAY);

        oscPlayX = "/";
        oscPlayX.append(p);
        oscPlayX.append(OSC_PLAY_X);

        oscPlayY = "/";
        oscPlayY.append(p);
        oscPlayY.append(OSC_PLAY_Y);
    }

    if(m.getAddress() != oscPlay &&
       m.getAddress() != oscPlayX &&
       m.getAddress() != oscPlayY ) {
        return;
    }

    //volume not implemented yet
//    float volume = 1.0f;
    ofRectangle bb = parent->getRestrictedClusterBB();

    if(m.getAddress() == oscPlay) {
        if ( m.getNumArgs() >= 2 ) {
            float x = m.getArgAsFloat(0);
            float y = m.getArgAsFloat(1);

            if ( bb.isZero() ) {
                oscPosition = ofVec2f(x * Sounds::getInstance()->getInitialWindowWidth(),
                                      y * Sounds::getInstance()->getInitialWindowHeight());
            } else {
                oscPosition = ofVec2f(ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth()),
                                      ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight()));
            }

//            if (m.getNumArgs() >= 3 ) {
//                volume = m.getArgAsFloat(2);
//            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == oscPlayX ) {
        if ( m.getNumArgs() >= 1 ) {
            float x = m.getArgAsFloat(0);

            if ( bb.isZero() ) {
                x *= Sounds::getInstance()->getInitialWindowWidth();
            } else {
                x = ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth());
            }

            oscPosition = ofVec2f(x, oscPosition.y);

//            if (m.getNumArgs() >= 2) {
//                volume = m.getArgAsFloat(1);
//            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    } else if ( m.getAddress() == oscPlayY ) {
        if ( m.getNumArgs() >= 1 ) {
            float y = m.getArgAsFloat(0);

            if ( bb.isZero() ) {
                y *= Sounds::getInstance()->getInitialWindowHeight();
            } else {
                y = ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight());
            }

            oscPosition = ofVec2f(oscPosition.x, y);

//            if (m.getNumArgs() >= 2) {
//                volume = m.getArgAsFloat(1);
//            }
        } else {
            ofLog() << "No arguments for OSC: " << m.getAddress();
        }
    }

    parent->mouseDragged(oscPosition, -1);
    for ( UnitBehavior * behavior : parent->behaviors ) {
        behavior->mouseDragged(oscPosition, -1);
    }
    oscCursorOpacity = 255;
}

Json::Value UnitBehaviorOSC::save()
{
    Json::Value oscConfig = Json::Value( Json::objectValue );
    oscConfig["enabled"] = enabled;
    oscConfig["prefix"] = prefix;
    return oscConfig;
}

void UnitBehaviorOSC::load(Json::Value jsonData)
{
    if ( jsonData["enabled"] != Json::nullValue ) {
        enabled = jsonData["enabled"].asBool();
    }
    if ( jsonData["prefix"] != Json::nullValue ) {
        strncpy(prefix, jsonData["prefix"].asCString(), 64);
    }
}

void UnitBehaviorOSC::update()
{
    if ( oscCursorOpacity > 0 ) oscCursorOpacity -= 2;
}

void UnitBehaviorOSC::draw()
{
    if ( oscCursorOpacity < 1 ) return;

    ofNoFill();
    ofSetColor(255,255,255, oscCursorOpacity);
    ofDrawLine((oscPosition.x), (oscPosition.y - 4.5f), (oscPosition.x), (oscPosition.y + 4.5f));
    ofDrawLine((oscPosition.x - 4.5f), (oscPosition.y), (oscPosition.x + 4.5f), (oscPosition.y));
}

void UnitBehaviorOSC::drawGui()
{
    if ( !strncmp(prefix, "", 64) ) {
        ImGui::Checkbox("Listen /play/xy OSC message", &enabled);
    } else {
        string p = "Listen /";
        p.append(string(prefix));
        p.append(string("/play/xy OSC message"));
        ImGui::Checkbox( p.c_str(), &enabled);
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_OSC_LISTEN);
    if ( enabled ) {
        ImGui::InputText("OSC Prefix", prefix, 64);
    }
}
