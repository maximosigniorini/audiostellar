#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

#include "UnitBehavior.h"

class UnitBehaviorOSC: public UnitBehavior{
private:
    ofVec2f oscPosition {0,0};
    int oscCursorOpacity = 255;
    bool enabled = false;

    void oscPlay(float volume);

    const string OSC_PLAY = "/play/xy";
    const string OSC_PLAY_X = "/play/x";
    const string OSC_PLAY_Y = "/play/y";

    char prefix[64] = "";

public:
    UnitBehaviorOSC(Unit * parentUnit);
    ~UnitBehaviorOSC();

    const string getName() { return "UnitBehaviorOSC"; }

    void onOSCMessage(ofxOscMessage &m);

    Json::Value save();
    void load( Json::Value jsonData );

    void update();
    void draw();
    void drawGui();
};
