#pragma once

#include "../Unit.h"

class Unit;

class UnitBehavior {
protected:
    Unit * parent;
public:
    UnitBehavior(Unit * parentUnit) {
        setParent(parentUnit);
    }
    virtual ~UnitBehavior() {}
    virtual void die() {}

    void setParent(Unit * parentUnit) {
        parent = parentUnit;
    }

    bool drawGUISeparate = false;

    virtual const string getName() { return ""; };

    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void drawGui(){}
    virtual void update() {}

    virtual Json::Value save() { return Json::nullValue; }
    virtual void load( Json::Value jsonData ) {}

    virtual void mousePressed(ofVec2f p, int button) {}
    virtual void mouseDragged(ofVec2f p, int button) {}
    virtual void mouseReleased(ofVec2f p){}
    virtual void mouseMoved(ofVec2f p){}
    virtual void keyPressed(ofKeyEventArgs & e) {}
};
