#pragma once

#include "UnitBehavior.h"

class UnitBehavior;

class UnitBehaviors {
private:
    vector<UnitBehavior> behaviors;
public:
    void add(UnitBehavior &behavior);

    void beforeDraw();
    void draw();
    void drawGui();
    void update();
    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);
    void mouseReleased(ofVec2f p);
    void mouseMoved(ofVec2f p);
    void keyPressed(ofKeyEventArgs & e);
};
