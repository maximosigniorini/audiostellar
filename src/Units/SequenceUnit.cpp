#include "SequenceUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"
#include "ofxMidiMessage.h"

int SequenceUnit::intBars[QTY_BARS] = {QTY_STEPS/4, QTY_STEPS /2, QTY_STEPS, QTY_STEPS*2, QTY_STEPS*3, QTY_STEPS*4};

SequenceUnit::SequenceUnit() {
    parameters.push_back(&probability);
    parameters.push_back(&offset);

    offset.format = "%.0f";
    offset.addListener(this, &SequenceUnit::onNeedToProcess);
    selectedBars.addListener(this, &SequenceUnit::onNeedToProcessInt);

    ofAddListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );

    behaviors.push_back( new UnitBehaviorDragToPlay(this) );
}

void SequenceUnit::setIndexExtra(int i)
{
    string name = "Bars##Unit" + ofToString(i);
    selectedBars.setName(name);
}

void SequenceUnit::onNeedToProcess(float &a){
    processSequence();
}
void SequenceUnit::onNeedToProcessInt(int &a){
    processSequence();
}

Json::Value SequenceUnit::save() {
    Json::Value sequence = Json::Value( Json::objectValue );
    Json::Value ids = Json::Value( Json::arrayValue );

    for ( unsigned int j = 0 ; j < secuencia.size() ; j++ ) {
        ids.append( secuencia[j]->id );
    }

    sequence["ids"] = ids;
    sequence["offset"] = offset.get();
    sequence["probability"] = probability.get();
    sequence["bars"] = selectedBars.get();

    return sequence;
}
void SequenceUnit::load(Json::Value jsonData) {
    if ( jsonData["ids"] != Json::nullValue ) {
        Json::Value ids = jsonData["ids"];
        for ( unsigned int i = 0 ; i < ids.size() ; i++ ) {
            toggleSound( Sounds::getInstance()->getSoundById( ids[i].asInt() ) , false);
        }
    }

    if ( jsonData["offset"] != Json::nullValue ) offset.set( jsonData["offset"].asInt() );
    if ( jsonData["probability"] != Json::nullValue ) probability.set( jsonData["probability"].asFloat() );
    if ( jsonData["bars"] != Json::nullValue ) selectedBars.set( jsonData["bars"].asInt() );

    processSequence();
    setSelectAllSounds(false);
}

void SequenceUnit::onSelectedUnit() {
    Unit::onSelectedUnit();
    setSelectAllSounds();
}
void SequenceUnit::onUnselectedUnit() {
    Unit::onUnselectedUnit();
    setSelectAllSounds(false);
}

void SequenceUnit::setSelectAllSounds(bool selected)
{
    for ( unsigned int i = 0 ; i < secuencia.size() ; i++ ) {
        secuencia[i]->selected = selected;
    }
}

void SequenceUnit::draw() {
//tss look for beforeDraw
}

void SequenceUnit::drawGui()
{
    if ( ExtraWidgets::Header("Parameters") ) {
        offset.draw();

        probability.draw();

        ofxImGui::AddCombo(selectedBars, strBars);
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_SEQUENCE_BARS);

        if ( secuencia.size() > 0 ) {
            ImGui::NewLine();
            if ( ImGui::Button("Clear sequence") ) {
                clearSequence();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_SEQUENCE_CLEAR);
        }

        ImGui::NewLine();
    }
}

void SequenceUnit::midiMessage(MIDIMessage m)
{
    if ( m.status == MIDI_START ) {
        currentStep = -1;
    }
}

void SequenceUnit::onTempo(int & frame)
{
    if ( isProcessing ) return;

    currentStep = frame % getCantSteps();

    if ( secuenciaEnTiempo != nullptr && secuenciaEnTiempo[currentStep] != nullptr ) {
        if ( ofRandom(1.f) <= probability.get() ) {
            playSound(secuenciaEnTiempo[currentStep]);
        }
    }
}

void SequenceUnit::processSequence()
{
    isProcessing = true;
    polyLine.clear();

    secuenciaEnTiempo = new shared_ptr<Sound>[getCantSteps()];
    vector<int> steps;

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempo[i] = nullptr;
    }

    unsigned int secuenciaSize = secuencia.size();

    if ( secuenciaSize == 0 ) {
        isProcessing = false;
        return;
    }  
    secuenciaEnTiempo[0] = secuencia[0];
    if ( secuenciaSize == 1 ) {
        isProcessing = false;
        return;
    }

    if ( getCantSteps() < secuencia.size() ) {
        secuenciaSize = getCantSteps();

        // If all steps are taken, then there is not much to calculate.
        // Just put each sound in each step
        for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
            steps.push_back(i);
        }
    } else { // There are more steps than sounds, where does each sound go ?

        // First we build a vector of distances and calculate the length of the whole path
        // Each position of this vector is the distance to ALL previous points.
        vector<float> distancias;
        float ultimaDistancia = -1;
        float distanciaAcumulada = 0;
        for ( unsigned int i = 1 ; i < secuenciaSize ; i++ ) {
            float distancia = secuencia[i-1]->getPosition().distance(secuencia[i]->getPosition());
            distanciaAcumulada += distancia;

            if ( ultimaDistancia != -1 ) {
                distancia += ultimaDistancia;
            }

            distancias.push_back(distancia);
            ultimaDistancia = distancia;
        }

        // We add the distance between the last point and the first one
        float distanciaUltimoConPrimero = secuencia[secuenciaSize-1]->getPosition()
                                            .distance(secuencia[0]->getPosition());
        distanciaAcumulada += distanciaUltimoConPrimero;

        // We map each distance with the step it should belong
        for ( unsigned int i = 0 ; i < distancias.size() ; i++ ) {
            distancias[i] = ofMap( distancias[i],
                                   0, distanciaAcumulada,
                                   1, getCantSteps()-1, true );

            // We discretize the mapping and assert there are no sounds
            // belonging to the same step
            int step = round (distancias[i]);

            while( find( steps.begin(), steps.end(), step ) != steps.end() ) {
                step++;
            }

            steps.push_back(step);
        }

        // This doesn't work right in some cases,
        // We assert that steps are correct, and if not, correct them
        bool hayError = false;
        do {
            hayError = false;
            for ( unsigned int i = 0 ; i < steps.size() ; i++ ) {
                // Assigned step is larger than the steps we actually have ?
                if ( steps[i] > getCantSteps() - 1 ) {
                    hayError = true;
                    steps[i]--;
                    // No problem, just move each sound one step less
                    for ( int j = i-1 ; j >= 0 ; j-- ) {
                        if ( steps[j] == steps[j+1] ) {
                            steps[j]--;
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
            // Do this until there is no error (hopefully)
        } while( hayError );
    }

    // Now we are ready to build our final vector.
    // It works just like a step sequencer:
    // each position is a fixed figure, sound pointer if there is a sound to play,
    // nullptr if silence.
    for ( unsigned int i = 0 ; i < steps.size() ; i++ ) {
        secuenciaEnTiempo[ steps[i] ] = secuencia[i+1];
    }

    for ( unsigned int i = 0 ; i < secuenciaSize ; i++ ) {
        polyLine.addVertex( secuencia[i]->getPosition().x,
                            secuencia[i]->getPosition().y, 0 );
    }

    polyLine.close();

    // Offset the whole vector if needed
    processSequenceOffset();

    isProcessing = false;
}

void SequenceUnit::processSequenceOffset()
{
    shared_ptr<Sound> * secuenciaEnTiempoOffset = new shared_ptr<Sound>[getCantSteps()];

    for ( int i = 0 ; i < getCantSteps() ; i++ ) {
        secuenciaEnTiempoOffset[ (i + int(offset.get()) ) % getCantSteps() ] = secuenciaEnTiempo[i];
    }

    secuenciaEnTiempo = secuenciaEnTiempoOffset;
}

void SequenceUnit::clearSequence()
{
    setSelectAllSounds(false);
    secuencia.clear();

    if ( secuenciaEnTiempo != nullptr ) {
        for ( int i = 0 ; i < getCantSteps() ; i++ ) {
            secuenciaEnTiempo[i] = nullptr;
        }
    }

    polyLine.clear();
    reset();
}

int SequenceUnit::getCantSteps()
{
    return intBars[selectedBars];
}

void SequenceUnit::toggleSound(shared_ptr<Sound> sound, bool doProcessSequence)
{
    if ( sound != nullptr ) {
        if ( !sound->selected ) {
            //TODO : infinite sequences
            if ( secuencia.size() < getCantSteps() ) {
                secuencia.push_back( sound );
                sound->selected = true;
            }
        } else {
            secuencia.erase(
                std::remove(secuencia.begin(),
                secuencia.end(), sound), secuencia.end());

            sound->selected = false;

        }

        // ofLog() << secuencia.size() << endl;
        if ( doProcessSequence ) {
            processSequence();
        }
    }
}



void SequenceUnit::beforeDraw() {
    if ( isProcessing ) {
        return;
    }

    ofSetLineWidth(1);

    if ( selected ) {
        ofSetColor(ofColor(255,255,255));
    } else {
        ofSetColor(ofColor(80,80,80));
    }

    polyLine.draw();

    if ( playing ) {
        ofSetColor(ofColor(255,255,255));
        ofFill();

        if ( secuencia.size() > 0 ) {
            ofPoint p;

            if ( secuenciaEnTiempo[currentStep] != NULL ) {
//                p = secuenciaEnTiempo[currentStep]->getPosition();
            } else {
                float offsetedStep = ( currentStep + ( getCantSteps() - int(offset.get()) ) ) % getCantSteps();
                p = polyLine.getPointAtPercent( offsetedStep / getCantSteps() );
            }
            if ( !(p.x == 0.0f && p.y == 0.0f)) {
                ofDrawEllipse( p.x , p.y, 5, 5 );
            }
        }
    }
}

void SequenceUnit::mousePressed(ofVec2f p, int button) {
    shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getHoveredSound();
    if ( hoveredSound != nullptr ) {
        if ( button == 0 ) {
            toggleSound( hoveredSound, true );
        }
    }
}

void SequenceUnit::mouseDragged(ofVec2f p, int button) {

}

void SequenceUnit::reset()
{
    probability.set(1.f);
    offset.set(0);
}

void SequenceUnit::die()
{
    ofRemoveListener( MasterClock::getInstance()->onTempo, this, &SequenceUnit::onTempo );
    clearSequence();
};
