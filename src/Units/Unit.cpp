#include "Unit.h"

ofEvent<Sound> Unit::eventPlayedSound = ofEvent<Sound>();

Unit::Unit() {
    pan.format = "L | R";
    pan.small = true;

    volume.addListener(this, &Unit::volumeListener);
    volume.set(DEFAULT_VOLUME);
    volume.format = VOLUME_FORMAT;

    pan.addListener(this, &Unit::panListener);
    pan.set(0.5f);

    uberMute.addListener(this, &Unit::muteListener);
    uberMute.color = uberMute.COLOR_MUTE;
    uberMute.small = true;

    uberSolo.addListener(this, &Unit::soloListener);
    uberSolo.color = uberSolo.COLOR_SOLO;
    uberSolo.small = true;

    parameters.push_back(&volume);
    parameters.push_back(&pan);
    parameters.push_back(&uberMute);
    parameters.push_back(&uberSolo);

    selectedOutput.addListener(this, &Unit::selectedOutputListener);

    patch();
}

Unit::~Unit() {
    unpatch();

    for ( UnitBehavior * b : behaviors ) {
        b->die();
        delete b;
    }
};

void Unit::playSound(shared_ptr<Sound> s, float volume) {
    if ( !s->mute ) {
        if ( restrictToCluster >= 0 && s->getCluster() != restrictToCluster ) {
            return;
        }

        Voice * v = voices.getVoice(s);
//        setupVoice(v);

        v->setVolume(volume);
        //TODO: speed, envelope, spread

        if ( !isMuted() && !isMuteForced() ) {
            ofNotifyEvent(eventPlayedSound, *s, this);
        }
    }
}

void Unit::setupVoice(Voice *v){

}

void Unit::volumeListener(float &v) {
    if (volume.get() == volume.getMin()) {
        volume.format = "-inf";
    } else {
        volume.format = VOLUME_FORMAT;
    }
    updateVolume();
    OscServer::getInstance()->mixerChangedVolume(index, volume.get());
}

void Unit::panListener(float &v) {
    panL = ofClamp((1.0f - v)*2, 0.0001f,1.0f);
    panR = ofClamp(v*2, 0.0001f,1.0f);
    updateVolume();
    OscServer::getInstance()->mixerChangedPan(index, pan.get());
}

void Unit::updateVolume() {
    if ( mute || forceMute || volume.get() == volume.getMin() ) {
        0.0001f >> outL.in_mod();
        0.0001f >> outR.in_mod();
    } else {
        panL >> outL.in_mod();
        panR >> outR.in_mod();
    }
}

void Unit::muteListener(float &v) {
    changeMute( uberMute.isActive() );
    OscServer::getInstance()->mixerChangedMute(index, uberMute.isActive());
}

void Unit::soloListener(float &v) {
    changeSolo( uberSolo.isActive() );
    OscServer::getInstance()->mixerChangedSolo(index, uberSolo.isActive());
}

void Unit::setVolume(float v)
{
    volume.set( ofMap(v, 0, 1, volume.getMin(), volume.getMax(), true) );
    updateVolume();
}

void Unit::muteSound(shared_ptr<Sound> s)
{
    voices.muteSound(s);
}

void Unit::changeMute(bool mute, bool forced) {
    if ( !forced ) {
        this->mute = mute;
    } else {
        this->forceMute = mute;
    }
    needsToProcessSolo = true;

    updateVolume();
}

void Unit::changeSolo(bool v) {
    solo = v;
    needsToProcessSolo = true;
}

ofRectangle Unit::getRestrictedClusterBB()
{
    return restrictedClusterBB;
}

void Unit::selectedOutputListener(int &v)
{
    AudioEngine::Output selectedOutputObj = AudioEngine::getInstance()->getEnabledOutput(v);

    if ( selectedOutputObj.label != "error" ) {
        unpatch();
        outChannel0 = selectedOutputObj.channel0;
        outChannel1 = selectedOutputObj.channel1;
        patch();
    } else {
        ofLog() << "Tried to set an audio output for unit that is not enabled from audio settings";
        selectedOutput.set(0);
    }

}

void Unit::patchVUmeter()
{
    outL >> vuRMSL >> AudioEngine::getInstance()->engine.blackhole();
    outR >> vuRMSR >> AudioEngine::getInstance()->engine.blackhole();
    outL >> vuPeakPreL >> AudioEngine::getInstance()->engine.blackhole();
    outR >> vuPeakPreR >> AudioEngine::getInstance()->engine.blackhole();
}

void Unit::remove()
{
    unpatch();
    voices.die();
    die();
}

void Unit::setIndex(int i) {
    string name = "Unit" + ofToString(i);
    effects.setIndexesToEffects(name);
    name = "##" + name;

    for ( unsigned int j = 0 ; j < parameters.size() ; j++ ) {
        parameters[j]->setName(parameters[j]->originalName + name);
    }

    setIndexExtra(i);
    index = i;
}

Json::Value Unit::saveBehaviors() {
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < behaviors.size(); i++) {
        UnitBehavior * behavior = behaviors[i];
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = behavior->getName();
        jsonUnit["params"] = behavior->save();
        root.append(jsonUnit);
    }
    return root;
}

void Unit::loadBehaviors( Json::Value jsonData )
{
    if ( jsonData != Json::nullValue ) {
        for ( UnitBehavior * b : behaviors ) {
            for ( unsigned int i = 0 ; i < jsonData.size() ; i++ ) {
                if ( jsonData[i]["name"] == b->getName() ) {
                    b->load(jsonData[i]["params"]);
                    break;
                }
            }
        }
    }
}

void Unit::setMute(bool v)
{
    uberMute.setActive(v);
}

void Unit::setSolo(bool v)
{
    uberSolo.setActive(v);
}

void Unit::patch(){

    addModuleInput("0", effects.in("0"));
    addModuleInput("1", effects.in("1"));

    for ( Voice &v : voices.voices ) {
        v.out("0") >> in("0");
        v.out("1") >> in("1");
    }

    effects.out("0") >> outGain.ch(0) >> outL;
    effects.out("1") >> outGain.ch(1) >> outR;

    outL >> Units::getInstance()->masterGain.ch( outChannel0 ) ;
    outR >> Units::getInstance()->masterGain.ch( outChannel1 ) ;

    updateVolume();
    patchVUmeter();
}

void Unit::unpatch() {
    // i don't think this is needed
    for ( Voice &v : voices.voices ) {
        v.disconnectOut();
    }

    outL.disconnectAll();
    outR.disconnectAll();
    outGain.disconnectAll();
    vuRMSL.disconnectAll();
    vuRMSR.disconnectAll();
    vuPeakPreL.disconnectAll();
    vuPeakPreR.disconnectAll();
}
