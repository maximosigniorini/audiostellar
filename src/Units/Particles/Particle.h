#pragma once

#include "ofMain.h"
#include "Sound.h"
#include "ofxPDSP.h"

class Particle {
private:

public:
    float lifeSpan;
    float age;
    ofVec2f position;
    float size;
    shared_ptr<Sound> lastPlayedSound = NULL;
    ofPolyline polygon;

    Particle() {
        age = 255;
        size = 1.5;
//        polygon = generateAsteroidPolygon(); NOT BEING USED AT THE MOMENT
    }

    ~Particle(){}

    virtual void customUpdate() {}
    virtual void customDraw() {}

    void update() {
        customUpdate();
        if (lifeSpan != 10.0f) {
            age -= ofMap(lifeSpan,0,10,10,0);
        }
    }

    void draw( ofColor color ) {
        ofPushStyle();
        ofPushMatrix();


            ofTranslate(position);
            ofNoFill();
            //ofSetColor(232,11,85, lifeSpan );
            //ofSetLineWidth(2);
            //polygon.draw();
            ofFill();
            ofSetColor(color.r,color.g,color.b, age );
            ofDrawCircle(0, 0, size);

        ofPopMatrix();
        ofPopStyle();
    };


    bool isDead() {
      return (age <= 0.0f);
    };

    void kill(){
        age = 0.0;
    };

    ofVec2f getPosition() {
        return position;
    }
    void setPosition(ofVec2f p) {
        position = p;
    }
    float getLifeSpan() {
        return lifeSpan;
    }

    bool crashes(ofVec2f pos){
       int threshold = 10;
       float dist = ofDist(position.x, position.y, pos.x, pos.y);

       return dist <= fabs(threshold);
   }

    ofPolyline generateAsteroidPolygon(){
        ofPolyline asteroid;
        /*
        int nVertex = (int)ofRandom(0,7);

        for(unsigned int i = 0; i < nVertex; i++){
            asteroid.addVertex(ofRandom(8), ofRandom(8));
        }
        */
        float i = 0;
        while(i < TWO_PI){
            float r = ofRandom(1,4);
            ofVec3f p = ofVec3f(cos(i)*r, sin(i)*r, 0);
            asteroid.addVertex(p);
            i+=0.6;
        }
        asteroid.close();
//        ofLogNotice("POLY", ofToString(asteroid.getVertices()));

       return asteroid;
    }

};
