#pragma once

#include "ofMain.h"
#include "ofxJSON.h"
#include "ofxPDSP.h"

#include "../Utils/Utils.h"
#include "../Utils/Tooltip.h"
#include "../Sound/Sound.h"
#include "../Sound/Voices.h"
#include "../Effects/AudioEffects.h"
#include "../Servers/MidiServer.h"
#include "../GUI/UberSlider.h"
#include "../GUI/UberToggle.h"
#include "Behaviors/UnitBehavior.h"

class UnitBehavior;

class Unit : public pdsp::Patchable {
private:
#define DEFAULT_VOLUME -10.f
#define VOLUME_FORMAT "%.2fdB"
#define VOLUME_MIN -47.99f
#define VOLUME_MAX 12.f

    int index = -1; //this is set by units and constructs unique labels

    pdsp::Amp outL;
    pdsp::Amp outR;
    pdsp::ParameterGain outGain;
    UberSlider volume { outGain.set("##volume", DEFAULT_VOLUME, VOLUME_MIN, VOLUME_MAX),
                        &Tooltip::UNIT_VOLUME };

    UberSlider pan {"##pan", 0.5f, 0.f, 1.f, &Tooltip::UNIT_PAN};
    float panL = 1.0;
    float panR = 1.0;

    bool mute = false; // user chose to mute the track
    bool forceMute = false; // forced by units (i.e other soloed track)
    bool solo = false;
    UberToggle uberMute {"M##ute", &Tooltip::UNIT_MUTE};
    UberToggle uberSolo {"S##olo", &Tooltip::UNIT_SOLO};
    bool needsToProcessSolo = false;
    void changeMute(bool mute, bool forced = false);
    void changeSolo(bool v);

    //VU meter
    pdsp::RMSDetector vuRMSL;
    pdsp::RMSDetector vuRMSR;
    pdsp::AbsoluteValue vuPeakPreL;
    pdsp::AbsoluteValue vuPeakPreR;

    Voices voices;
    AudioEffects effects;

    //Selected output
    ofParameter<int> selectedOutput = 0;
    void selectedOutputListener(int &v);

    void patchVUmeter();

    int outChannel0 = 0;
    int outChannel1 = 1;

    int restrictToCluster = -1;
    ofRectangle restrictedClusterBB;
    bool restrictToClusterSelecting = false;

    // this is call when removing by units
    // it won't fully delete the object
    void remove();

    friend class Units;
protected:
    //some units may require explorer mode dragging deactivated
    bool allowExplorerModeDrag = true;
    //Is unit selected by the user?
    bool selected = false;

    ////////////////////
    // Push your UberControls here
    // It will take care of defining unique IDs
    // NOT DOING THIS WILL BREAK OSC/MIDI MAPPING
    ////////////////////
    vector<UberControl *> parameters;
public:
    Unit();
    virtual ~Unit();

    // It is unlikely to need to reimplement these
    virtual void patch();
    virtual void unpatch();

    //All units MUST have a name
    virtual string getUnitName() = 0;

    ////////////////////
    // Methods Unit developers might want to override
    //
    // Declaring these on your .h but not implementing them
    // in your .cpp will result on a cryptic linker error.
    // Just declare AND implement the ones you need
    ////////////////////
    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void drawGui(){}
    virtual void update() {}
    virtual void reset() {}
    virtual void die() {} //for some reason, destructor won't remove notify from master clock //is this still happening ??

    virtual void mousePressed(ofVec2f p, int button) {}
    virtual void mouseDragged(ofVec2f p, int button) {}
    virtual void mouseReleased(ofVec2f p){}
    virtual void keyPressed(ofKeyEventArgs & e) {}
    virtual void mouseMoved(ofVec2f p){}
    virtual void midiMessage( MIDIMessage m){}

    // These MUST be defined
    virtual Json::Value save() = 0;
    virtual void load( Json::Value jsonData ) = 0;
    //////////////////////////////////////////////////

    // It is unlikely to need to reimplement playSound
    virtual void playSound(shared_ptr<Sound> s, float volume = 1.0f);
    static ofEvent<Sound> eventPlayedSound;
    void setupVoice(Voice * v);

    //Behaviors are functional modules shared between units
    vector<UnitBehavior *> behaviors;

    // Mute / solo / volume managment
    void volumeListener(float &v);
    void panListener(float &v);
    void updateVolume();
    void muteListener(float &v);
    void soloListener(float &v);
    void setVolume(float v);

    void muteSound( shared_ptr<Sound> s );

    ////////////////////
    // This sets unique IDs to almost every control.
    // All UberControls MUST be pushed to parameters vector,
    // otherwise MIDI/OSC won't work correctly
    //
    // Normally you shouldn't care about this,
    // but if you are using non uber controls
    // Then, implement setIndexExtra. (see SequenceUnit)
    ////////////////////
    void setIndex(int i);
    virtual void setIndexExtra(int i) {}

    Json::Value saveBehaviors();
    void loadBehaviors( Json::Value jsonData );

    // What happens when user selects a Unit ?
    // usually this should be enough
    virtual void onSelectedUnit() { selected = true; }
    virtual void onUnselectedUnit() { selected = false; }

    // Getters and setters
    bool isMuted() { return mute; }
    bool isMuteForced() { return forceMute; }
    bool isSolo() { return solo; }
    void setMute( bool v );
    void setSolo( bool v );

    ofRectangle getRestrictedClusterBB();
};
