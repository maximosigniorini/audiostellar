#include "UI.h"

Gui* Gui::instance = nullptr;

Gui::Gui() {
    font.load( OF_TTF_MONO, 10 );

    ofImage imgWelcomeImage;
    imgWelcomeImage.setUseTexture(false);

    if ( !imgWelcomeImage.load("assets/welcome.png") ) {
        ofLog() << "Welcome image not loaded";
    }

    ofTextureData texData;
    texData.width = imgWelcomeImage.getWidth();
    texData.height = imgWelcomeImage.getHeight();
    texData.textureTarget = GL_TEXTURE_2D;
    texData.bFlipTexture = true;
    imgWelcome.allocate(texData);
    imgWelcome.loadData(imgWelcomeImage.getPixels());
    
    gui.setup(new GuiTheme(), true);
    drawGui = true;
}

Gui* Gui::getInstance(){

    if( instance == nullptr ){
        instance = new Gui();
    }
    return instance;
}

void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
}

void Gui::drawProcessingFilesWindow()
{
    if ( isProcessingFiles ) {
        adr.checkProcess();

        if ( adr.progress == 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                //sessionManager->loadSession(resultJSONpath.c_str());
                SessionManager::datasetLoadOptions opts;
                opts.method = "RECENT";
                opts.path = resultJSONpath.c_str();
                SessionManager::getInstance()->loadSession(opts);

                string strSuccess = "Hooray! Your map is done. \n\n";
                if ( Sounds::getInstance()->getUseOriginalClusters() ) {
                    strSuccess += "Your dataset was inside different folders so\nwe are using them as \"original clusters\".\n\n"
                                  "If this is not what you want, then adjust clustering settings.\n";
                } else {
                    strSuccess += "Now adjust clustering settings.\n";
                }

                if ( !adr.failFiles.empty() ) {
                    strSuccess += "\n:( Some files couldn't be processed: ";
                    for ( string s : adr.failFiles ) {
                        strSuccess += "\n" + s;
                    }
                }

                showMessage( strSuccess, "Process ended successfully" );
                Sounds::getInstance()->setClusteringOptionsScreenFlags();
            } else {
                showMessage( "Unexpected error", "Error" );
            }
            AudioDimensionalityReduction::end();
        } else if ( adr.progress == -1.0f ) {
            isProcessingFiles = false;
            showMessage( adr.currentProcess, "Error" );
            AudioDimensionalityReduction::end();
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100));

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( adr.filesFound == 0 ) {
                windowTitle = "Processing files...";
            }

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {
                ImGui::Text("Grab a coffee, "
                            "this can take quite a while.");

                #ifndef TARGET_OS_WINDOWS
                ImGui::ProgressBar( adr.progress );
                #else
                ImGui::Text( ofToString(adr.progress).c_str() );
                #endif

                ImGui::Text( "%s", adr.currentProcess.c_str() );

                ImGui::TextDisabled("\n\n(monitoring your RAM usage is also a good idea)");
            }
            ImGui::End();
        }
    }
}

void Gui::draw() {

    gui.begin();

    //Menu Superior
    drawMainMenu();

    if(drawGui) {
        //Units window
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        auto mainSettings = ofxImGui::Settings();
        mainSettings.windowPos = ImVec2(10, 30);
        mainSettings.lockPosition = true;

//        if ( isUnitsWindowHovered )
//            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, 1.f);
//        else
//            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, .9f);

        ImGui::SetNextWindowSizeConstraints(ImVec2(UNITS_WINDOW_WIDTH,-1),
                                            ImVec2(-1,-1));

        if( ofxImGui::BeginWindow("Units", mainSettings, window_flags)) {
            Units::getInstance()->drawGui();
        }
        ofxImGui::EndWindow(mainSettings);
//        ImGui::PopStyleVar(); //commented alpha above

        if ( Units::getInstance()->currentUnits.size() > 0 ) {
            //Unit configuration window
            window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;

            mainSettings = ofxImGui::Settings();
            mainSettings.windowPos = ImVec2(ofGetWidth()-WINDOW_TOOLS_WIDTH-10, 30);
            mainSettings.lockPosition = true;

            if( ofxImGui::BeginWindow("Selected unit configuration", mainSettings, window_flags)){
                Units::getInstance()->drawSelectedUnitSettings();
                ImGui::SetWindowSize(ImVec2(WINDOW_TOOLS_WIDTH,0)); //updating height every frame, fixed width
            }

            ofxImGui::EndWindow(mainSettings);
        }



        Tooltip::drawGui();
    }

    Units::getInstance()->drawMasterVolume();

    drawProcessingFilesWindow();
    drawAboutScreen();
    drawDimReductScreen();
    drawTutorial();

    Sounds::getInstance()->drawClusteringOptionsScreen();

    drawAudioSettingsScreen();
    drawContextMenu();
    drawOscSettingsScreen();
    drawOSCLearnScreen();
    drawMIDILearnScreen();

    haveToHideCursor ? ofHideCursor() : ofShowCursor();

    if(toggleFullscreen != isFullscreen ){
        ofToggleFullscreen();
        isFullscreen = !isFullscreen;
    }

    drawWelcomeScreen();

    drawFPS();
    drawMessage();
    drawCrashMessage();

    gui.end();
}

void Gui::drawWelcomeScreen(){
   if(haveToDrawWelcomeScreen){
       ImGuiWindowFlags window_flags = 0;
       int w = 380;
       window_flags |= ImGuiWindowFlags_NoScrollbar;
       window_flags |= ImGuiWindowFlags_NoMove;
       window_flags |= ImGuiWindowFlags_NoResize;
       window_flags |= ImGuiWindowFlags_NoCollapse;
       window_flags |= ImGuiWindowFlags_NoNav;
       window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
       window_flags |= ImGuiWindowFlags_NoDecoration;

       ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
//       ImGui::setnextW

       ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

       if ( ImGui::Begin("Welcome to AudioStellar" , &haveToDrawWelcomeScreen, window_flags) ){
           ofxImGui::AddImage(imgWelcome, ofVec2f(353,80));
           ImGui::NewLine();

           ImGui::PushTextWrapPos(w);
           ImGui::TextWrapped("%s", welcomeScreenText.c_str());
           ImGui::Columns(2,NULL, false);

           if(ImGui::Button("Yes, please.")) {
              haveToDrawWelcomeScreen = false;
              startTutorial();
           }

           ImGui::NextColumn();

           if(ImGui::Button("No, I can handle this.")){
               haveToDrawWelcomeScreen = false;
           }

           ImGui::End();
       }

       ImGui::PopStyleVar();
   }
}

void Gui::drawMainMenu(){

    if( ImGui::BeginMainMenuBar() ) {
        if( ImGui::BeginMenu("File") ){

            if ( ImGui::MenuItem( "New", TARGET_OS_MAC ? "Cmd+N" : "Ctrl+N") ) {
                newSession();
            }

            if(ImGui::MenuItem("Open", TARGET_OS_MAC ? "Cmd+O" : "Ctrl+O")){
                SessionManager::datasetLoadOptions opts;
                opts.method = "GUI";
                SessionManager::getInstance()->loadSession(opts);
            }

            if ( !SessionManager::getInstance()->isDefaultSession ) {
                if(ImGui::MenuItem("Save", TARGET_OS_MAC ? "Cmd+S" : "Ctrl+S")){
                    SessionManager::getInstance()->saveSession();
                }
            } else {
                ImGui::PushStyleColor(ImGuiCol_Text,ImVec4(0.5,0.5,0.5,1.0));
                if ( ImGui::MenuItem("Save") ) {
                    showMessage("Cannot save default project, use \"Save as\" instead.");
                }
                ImGui::PopStyleColor();
            }

            if(ImGui::MenuItem("Save as...", TARGET_OS_MAC ? "Cmd+Shift+S" : "Ctrl+Shift+S")){
                SessionManager::getInstance()->saveAsNewSession();
            }

            if(ImGui::BeginMenu("Recent Projects")){
                vector<string> recentProjects = SessionManager::getInstance()->getRecentProjects();
                if(SessionManager::getInstance()->areAnyRecentProjects(recentProjects)){
                    for(unsigned int i = 0; i < recentProjects.size(); i++){
                        if(ImGui::MenuItem(recentProjects[i].c_str())){

                            SessionManager::datasetLoadOptions opts;
                            opts.method = "RECENT";
                            opts.path = recentProjects[i].c_str();
                            SessionManager::getInstance()->loadSession(opts);

                        }
                    }
                }

                if(ImGui::MenuItem("Default project")) {
                    SessionManager::getInstance()->loadDefaultSession();
                }

                ImGui::EndMenu();
            }

            if(ImGui::MenuItem("Quit")){
                exit(0);
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            Sounds::getInstance()->drawGui();
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &Sounds::getInstance()->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show stats", &haveToDrawFPS);
            ImGui::Checkbox("Hide Cursor", &haveToHideCursor);
            ImGui::Checkbox("Fullscreen", &toggleFullscreen);
            ImGui::Checkbox("Show Tooltips", &Tooltip::enabled);
            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){
            
            if ( ImGui::MenuItem("Audio Settings") ) {
                haveToDrawAudioSettingsScreen = true;
            }
            ImGui::Separator();

            if(ImGui::MenuItem("Clustering Settings")){
                Sounds::getInstance()->setClusteringOptionsScreenFlags();
            }
            ImGui::Separator();
            
            MidiServer::getInstance()->drawGui();

            if ( ImGui::MenuItem("OSC settings") ) {
               haveToDrawOscSettingsScreen = true;
            }


            ImGui::EndMenu();
        }

        if ( ImGui::BeginMenu("Tools") ) {
            if ( ImGui::MenuItem("Sounds to original positions") ) {
                Sounds::getInstance()->revertToOriginalPositions();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_ORIGINAL_POSITIONS);

            if(ImGui::MenuItem("Export clusters to folders")) {
               Sounds::getInstance()->exportFiles();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_EXPORT_FILES);

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
            }
            if(ImGui::MenuItem("Tutorial")){
                startTutorial();
            }
            if(ImGui::MenuItem("Source code")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
            if(ImGui::MenuItem("OSC Documentation")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/-/blob/units/OSC_Documentation.md";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
           ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }

    drawMainMenuExtra();
}

void Gui::drawMainMenuExtra()
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowPos = ImVec2(ofGetWidth()/2 - 85 , -8);
    mainSettings.lockPosition = true;

    if ( ofxImGui::BeginWindow("winTempo", mainSettings, window_flags) ) {
        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());
        MasterClock::getInstance()->drawGui();
    }
    ImGui::PopStyleColor(2);
    ofxImGui::EndWindow(mainSettings);
}


void Gui::drawAboutScreen(){
    if(haveToDrawAboutScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
        if(ImGui::Begin("About", &haveToDrawAboutScreen, window_flags)){
           ImGui::Text("Thanks for using AudioStellar v1.0.0 \n\n\nMore info at https://www.audiostellar.xyz");
        }
        ImGui::End();
    } else {
        haveToDrawAboutScreen = false;
    }
}

void Gui::drawDimReductScreen(){
    if(haveToDrawDimReductScreen) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin("Generate new session" , &haveToDrawDimReductScreen, window_flags)){
            //path
            ImGui::Text("Folder location");

            string folder;
            string displayFolder;

            if(adr.dirPath.size()) {
                folder = ofFilePath::getPathForDirectory(adr.dirPath);
                displayFolder = folder;
            }        

            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,3));
            ImGui::BeginChild("child", ImVec2(208, 19), true);
            ImGui::TextUnformatted( displayFolder.c_str() );
            ImGui::EndChild();
            ImGui::PopStyleVar();

            ImGui::SameLine();
            if(ImGui::Button("Select folder")){
                ofFileDialogResult dialogResult = ofSystemLoadDialog("Select Folder", true, ofFilePath::getUserHomeDir());
                string path;

                if(dialogResult.bSuccess) {
                    adr.dirPath = dialogResult.getPath();

                }
            }

            if(ImGui::TreeNode("Advanced settings")) {
                if(ImGui::TreeNode("Feature settings")) {
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FEATURE_SETTINGS);

                    ImGui::Checkbox("STFT", &adr.stft);
                    ImGui::Checkbox("MFCC", &adr.mfcc);
                    ImGui::Checkbox("Spectral-Centroid", &adr.spectralCentroid);
                    ImGui::Checkbox("Chromagram", &adr.chromagram);
                    ImGui::Checkbox("RMS", &adr.rms);

                    if ( !adr.stft && !adr.mfcc && !adr.spectralCentroid && !adr.chromagram && !adr.rms ) {
                        adr.stft = true;
                    }

                    ImGui::NewLine();

                    ImGui::InputFloat("Audio length", &adr.targetDuration, 0.1f, 0.5f, "%.1f");
                    adr.targetDuration = ofClamp(adr.targetDuration,0,500);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_LENGTH);

                    ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
                    adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);

                    ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
                    adr.stft_windowSize = window_size_values[window_size_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);

                    ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
                    adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
                    ImGui::NewLine();


                    ImGui::TreePop();
                }

                if(ImGui::TreeNode("Visualization settings")) {
                    ImGui::Combo("Algorithm", &viz_item_current, viz_items, IM_ARRAYSIZE(viz_items));
                    adr.viz_method = viz_values[viz_item_current];

                    if ( adr.viz_method == "tsne" ) {
                        ImGui::NewLine();

                        ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                        adr.metric = metric_values[metric_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                        ImGui::InputInt("Perplexity", &adr.tsne_perplexity);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);


                        ImGui::InputInt("Learning rate", &adr.tsne_learning_rate);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_LEARNING_RATE);

                        ImGui::InputInt("Iterations", &adr.tsne_iterations);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_ITERATIONS);
                    } else if ( adr.viz_method == "umap" ) {
                        ImGui::NewLine();

                        ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                        adr.metric = metric_values[metric_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                        ImGui::InputInt("N Neighbors", &adr.umap_n_neighbors);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_NEIGHBORS);

                        ImGui::InputFloat("Min distance", &adr.umap_min_dist, 0.05f, 0.1f, "%.2f");
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_MIN_DISTANCE);
                    }

                    ImGui::NewLine();


                    ImGui::TreePop();
                }

                if(ImGui::TreeNode("Preferences")){

                    ImGui::Checkbox("Save intermediate results", &adr.pca_results);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_SAVE_PCA_RESULTS);
                    ImGui::Checkbox("Force full process", &adr.force_full_process);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FORCE_FULL_PROCESS);

                    ImGui::TreePop();
                }


                ImGui::TreePop();
            }

            ImGui::NewLine();

            if(adr.dirPath.size()) {
                if(ImGui::Button("Run")) {
                    haveToDrawDimReductScreen = false;
                    isProcessingFiles = true;
                    haveToDrawWelcomeScreen = false;

                    adr.startThread();
                }
            }else{
                ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                ImGui::PushStyleColor(ImGuiCol_Text, textColor);
                ImGui::PushStyleColor(ImGuiCol_Button, color);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                ImGui::Button("Run");
                ImGui::PopStyleColor(4);
            }
            ImGui::End();
        }
        ImGui::PopStyleVar();

    }
}

void Gui::drawAudioSettingsScreen() {
    
    if(haveToDrawAudioSettingsScreen) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin("Audio Settings" , &haveToDrawAudioSettingsScreen, window_flags)) {
            // this can get better still
            vector <ofSoundDevice> devices = AudioEngine::getInstance()->listDevices();

            string selectedAudioDevice = "No audio";
            if ( AudioEngine::getInstance()->hasStarted() ) {
                selectedAudioDevice = devices[AudioEngine::getInstance()->selectedAudioOutDeviceID].name;
            }

            if (ImGui::BeginCombo("Audio Out", selectedAudioDevice.c_str())) {

                for (int i = 0; i < devices.size(); i++) {
                    if (ImGui::Selectable( devices[i].name.c_str())) {
                        AudioEngine::getInstance()->selectAudioOutDevice(i);
                    }
                    if (devices[i].deviceID == AudioEngine::getInstance()->selectedAudioOutDeviceID) {
                        ImGui::SetItemDefaultFocus();
                    }
                }
                ImGui::EndCombo();
            }

            if ( AudioEngine::getInstance()->hasStarted() ) {
                if (ImGui::BeginCombo( "Sample Rate", ofToString(AudioEngine::getInstance()->selectedSampleRate).c_str() )) {

                    vector <ofSoundDevice> devices = AudioEngine::getInstance()->listDevices();

                    for (int i = 0; i < devices[AudioEngine::getInstance()->selectedAudioOutDeviceID].sampleRates.size(); i++) {
                        if (ImGui::Selectable( ofToString(devices[AudioEngine::getInstance()->selectedAudioOutDeviceID].sampleRates[i]).c_str() )) {
                            AudioEngine::getInstance()->selectSampleRate(devices[AudioEngine::getInstance()->selectedAudioOutDeviceID].sampleRates[i]);
                        }
                    }
                    ImGui::EndCombo();
                }

                if (ImGui::BeginCombo( "Buffer Size", ofToString(AudioEngine::getInstance()->selectedBufferSize).c_str() )) {

                    for (int i = 0; i < AudioEngine::getInstance()->bufferSizes.size(); i++) {
                        if (ImGui::Selectable( ofToString(AudioEngine::getInstance()->bufferSizes[i]).c_str() )) {
                            AudioEngine::getInstance()->selectBufferSize(AudioEngine::getInstance()->bufferSizes[i]);
                        }
                    }
                    ImGui::EndCombo();
                }
            }

            ImGui::NewLine();

            // this should be change with Max Voices per channel
//            ImGui::InputInt("Max Voices",  &Voices::getInstance()->numVoices, NULL, NULL);
//            if(ImGui::IsItemDeactivatedAfterEdit()) {
//                Voices::getInstance()->reset();
//            }
//            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_PORT);

//            ImGui::NewLine();

            string countAudioChannels =
                    ofToString( AudioEngine::getInstance()->getSelectedDeviceAudioOutNumChannels() / 2 ) +
                    " stereo output channels";
            ImGui::Text("%s", countAudioChannels.c_str());

            // this should be readded in the future
            //
            //
//            ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, .0f);
//                ImGui::BeginChild("Child1", ImVec2(0,15), true);
//                    ImGui::Columns(2, "columns1", false);
//                    ImGui::Text("Stereo");
//                    ImGui::NextColumn();
//                    ImGui::Text("Mono");
//                ImGui::EndChild();
//            ImGui::PopStyleVar();

//            ImGui::Columns(1);

//            ImGui::BeginChild("Child2", ImVec2(0,300), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
//                ImGui::Columns(2, "columns2", false);

//                for (int i = 0; i < AudioEngine::getInstance()->outputs.size(); i++) {
//                    if (AudioEngine::getInstance()->outputs[i].type == "stereo") {
//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i].label.c_str(), &AudioEngine::getInstance()->outputs[i].enabled);
//                    }
//                }

//                ImGui::NextColumn();

//                for (int i = 0; i < AudioEngine::getInstance()->outputs.size(); i++) {
//                    if (AudioEngine::getInstance()->outputs[i].type == "mono") {

//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i].label.c_str(),
//                                          &AudioEngine::getInstance()->outputs[i].enabled,
//                                          NULL,
//                                          ImVec2(40.0f, .0f));

//                        ImGui::SameLine(80);

//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i + 1].label.c_str(),
//                                          &AudioEngine::getInstance()->outputs[i + 1].enabled,
//                                          NULL,
//                                          ImVec2(40.0f, .0f));

//                        i++; //skip next mono output
//                    }
//                }

//            ImGui::EndChild();

            ImGui::End();
        }
        ImGui::PopStyleVar();
    }
}

void Gui::drawOscSettingsScreen(){
    if(haveToDrawOscSettingsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        int w = 400;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3));
        if(ImGui::Begin("OSC settings",
                        &haveToDrawOscSettingsScreen,
                        window_flags)){

            OscServer::getInstance()->drawGui();
            ImGui::End();
        }
    }
}

void Gui::drawMIDILearnScreen()
{
    haveToDrawMIDILearnScreen = MidiServer::getInstance()->getMIDILearn();
    if ( haveToDrawMIDILearnScreen ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 300;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        if(ImGui::Begin("MIDI Learn", &haveToDrawMIDILearnScreen, window_flags)) {
//           ImGui::PushTextWrapPos(w);
           ImGui::Text("Use your MIDI controller to map a control");
           ImGui::NewLine();
           if ( ImGui::Button("Cancel") ) {
                MidiServer::getInstance()->setMIDILearnOff();
           }
        }
        ImGui::End();
    }
}

void Gui::drawOSCLearnScreen()
{
    haveToDrawOSCLearnScreen = OscServer::getInstance()->getOSCLearn();
    if ( haveToDrawOSCLearnScreen ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 300;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        if(ImGui::Begin("OSC Learn", &haveToDrawOSCLearnScreen, window_flags)) {
//           ImGui::PushTextWrapPos(w);
           ImGui::Text("Use your OSC controller to map a control");
           ImGui::NewLine();
           if ( ImGui::Button("Cancel") ) {
                OscServer::getInstance()->setOSCLearnOff();
           }
        }
        ImGui::End();
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
//        ofSetColor(255);
//        if ( Voices::getInstance()->numVoices == Voices::getInstance()->loadedVoices ) {
//            ofSetColor(255,0,0);
//        }
//        font.drawString(
//            "# Voices: " + ofToString(Voices::getInstance()->loadedVoices, 0),
//            ofGetWidth() - 110,
//            ofGetHeight() - 25);

        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::drawMessage()
{
    if ( haveToDrawMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 350;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, messageOpacity);

        if(ImGui::Begin( messageTitle.c_str() , &haveToDrawMessage, window_flags)) {
           ImGui::PushTextWrapPos(w);
           ImGui::Text("%s", messageDescription.c_str());
        }
        ImGui::End();

        ImGui::PopStyleVar();

        if ( messageFadeAway ) {
            messageOpacity -= 0.006f;
            if ( messageOpacity <= 0 ) {
                haveToDrawMessage = false;
            }
        }
    }
}

void Gui::drawCrashMessage()
{
    if ( haveToDrawCrashMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 430;
        int h = 225;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        if(ImGui::Begin( "Crash" , &haveToDrawCrashMessage, window_flags)) {
           ImGui::PushTextWrapPos(w);
           ImGui::Text( "Sorry, it looks like we've crashed last time. \n\n"
                        "This is usually caused by a wrong audio configuration. "
                        "Please, check that the sample rate and buffer size "
                        "matches your driver configuration.\n\n"
                        "As a safety measure we disabled the audio engine and opened "
                        "the default project. \n\n"
                        "If you think the crash is not related to this problem, please "
                        "report it." );

           ImGui::NewLine();

           ImGui::Dummy(ImVec2(25.f,1.f));
           ImGui::SameLine();

           if( ImGui::Button("Load recent project") ) {
               SessionManager::getInstance()->startAudioWithCurrentConfiguration();
               SessionManager::getInstance()->loadInitSession();
               haveToDrawCrashMessage = false;
           }
           ImGui::SameLine();
           if ( ImGui::Button("Reset all settings") ) {
               SessionManager::getInstance()->resetSettings();
               haveToDrawCrashMessage = false;
           }
           ImGui::SameLine();
           if ( ImGui::Button("Dismiss") ) {
               haveToDrawCrashMessage = false;
           }
        }
        ImGui::End();
    }
}

void Gui::drawTutorial()
{
    if(haveToDrawTutorial) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        //    window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2-w/2, 100), ImGuiCond_Appearing);

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin(tutorialCurrentPage->title.c_str() , &haveToDrawTutorial, window_flags)){
            ImGui::PushTextWrapPos(w);
            ImGui::TextWrapped("%s", tutorialCurrentPage->content.c_str());

            string buttonTitle = "Next";
            if ( tutorial.isLastPage() ) {
               buttonTitle = "Finish";
            }

            if(ImGui::Button(buttonTitle.c_str())){
                tutorialCurrentPage = tutorial.getNextPage();
                if ( tutorialCurrentPage == nullptr ) {
                    haveToDrawTutorial = false;
                }
            }

        }
        ImGui::End();

        ImGui::PopStyleVar();
   }
}

void Gui::drawContextMenu() {
    
    if (haveToDrawSoundContextMenu) {
        
        string selectedSoundPath = selectedSound->getFileName();
        string buttonText;
        string command;
        
        #ifdef TARGET_WIN32
        buttonText = "Show in Explorer";
        std::replace(selectedSoundPath.begin(), selectedSoundPath.end(), '/', '\\');
        command = "explorer /select," + selectedSoundPath;
        #endif

        #ifdef TARGET_OSX
        buttonText = "Show in Finder";
        command = "open -R \"" + selectedSoundPath + "\"";
        #endif

        #ifdef TARGET_LINUX
        buttonText = "Show in File Manager";
        string hoveredSoundEnclosingDirectory = ofFilePath::getEnclosingDirectory(selectedSoundPath);
        string script {
            "file_manager=$(xdg-mime query default inode/directory | sed 's/.desktop//g'); "
            "if [ \"$file_manager\" = \"nautilus\" ] || [ \"$file_manager\" = \"dolphin\" ] || [ \"$file_manager\" = \"nemo\" ]; "
            "then command=\"$file_manager\"; path=\"%s\"; "
            "else command=\"xdg-open\"; path=\"%s\"; "
            "fi; "
            "$command \"$path\""
        };


        char buffer[ script.size() + selectedSoundPath.size() + hoveredSoundEnclosingDirectory.size() ];
        std::sprintf( buffer, script.c_str(), selectedSoundPath.c_str() ,
                     hoveredSoundEnclosingDirectory.c_str()  );
        command = buffer;
        #endif
        
        //Sound context menu
        ImGui::OpenPopup("soundMenu");
        if (ImGui::BeginPopup("soundMenu")) {
            ImGui::TextDisabled("Sound Options");

            if (ImGui::MenuItem( "Mute", NULL, selectedSound->mute) ) {
                selectedSound->mute = !selectedSound->mute;
                if ( selectedSound->mute ) {
                    Units::getInstance()->muteSound(selectedSound);
                }
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
            }

            ImGui::Separator();

            if (ImGui::MenuItem( buttonText.c_str(), NULL, false) ) {
                ofSystem(command);
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
            }

            if ( selectedClusterID >= 0 ) {
                ImGui::NewLine();
                drawClusterContextMenu();
            }

            ImGui::EndPopup();
        }
    }
    
    if (haveToDrawClusterContextMenu) {
        ImGui::OpenPopup("clusterMenu");
        if (ImGui::BeginPopup("clusterMenu")) {
            drawClusterContextMenu();
            ImGui::EndPopup();
        } else {
            haveToDrawClusterContextMenu = false;
        }

    }
}

void Gui::drawClusterContextMenu()
{
    vector<shared_ptr<Sound>> cluster = Sounds::getInstance()->getSoundsByCluster(selectedClusterID);

    ImGui::TextDisabled("Cluster Options");
    ImGui::Dummy(ImVec2(0, 3.0f));

    ImGui::AlignTextToFramePadding();
    ImGui::Text("Name:");

    ImGui::SameLine();

    if (!isClusterContextMenuFirstOpen ) {
        for ( int i = 0 ; i < CLUSTERNAME_MAX_LENGTH ; i++ ) {
            Sounds::getInstance()->clusterNameBeingEdited[i] = Sounds::getInstance()->clusterNames[selectedClusterID][i];
        }

        isClusterContextMenuFirstOpen = true;
    }

    ImGui::PushItemWidth(150);
    if (ImGui::InputText("##ClusterName", Sounds::getInstance()->clusterNameBeingEdited, 64, ImGuiInputTextFlags_EnterReturnsTrue |
                                                     ImGuiInputTextFlags_AutoSelectAll )) {

        Sounds::getInstance()->nameCluster(selectedClusterID);
        haveToDrawClusterContextMenu = false;
        haveToDrawSoundContextMenu = false;
    }
    else if (ImGui::IsItemEdited()) {
        Sounds::getInstance()->nameCluster(selectedClusterID);
    }
    isClusterContextMenuFirstOpen = ImGui::IsItemActive();
//            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_SET_NAME);
    ImGui::Separator();

    // check if all sounds in cluster are muted
    bool allMuted = true;
    for (int i = 0; i < cluster.size(); i++) {
        if (!cluster[i]->mute) {
            allMuted = false;
            break;
        }
    }

    if (ImGui::MenuItem("Mute all", NULL, allMuted) ) {

        for (int i = 0; i < cluster.size(); i++) {
            cluster[i]->mute = !allMuted;

            if ( cluster[i]->mute ) {
                Units::getInstance()->muteSound(cluster[i]);
            }
        }

        ImGui::CloseCurrentPopup();
        haveToDrawClusterContextMenu = false;
        haveToDrawSoundContextMenu = false;
    }
}

void Gui::showMessage(string description, string title, bool fadeAway)
{
    haveToDrawMessage = true;
    messageTitle = title;
    messageDescription = description;
    messageOpacity = 1.0f;
    messageFadeAway = fadeAway;
}

void Gui::hideMessage()
{
    haveToDrawMessage = false;
}

void Gui::showCrashMessage()
{
    haveToDrawCrashMessage = true;
}

void Gui::showWelcomeScreen()
{
    haveToDrawWelcomeScreen = true;
}

void Gui::startTutorial()
{
    tutorialCurrentPage = tutorial.start();
    haveToDrawTutorial = true;
}

bool Gui::getShowStats()
{
    return haveToDrawFPS;
}

void Gui::setShowStats(bool v)
{
    haveToDrawFPS = v;
}

void Gui::keyPressed(ofKeyEventArgs & e) {

    auto &io = ImGui::GetIO();
    if ( io.WantTextInput ) {
        return;
    }

    Sounds::getInstance()->keyPressed(e);
    Units::getInstance()->keyPressed(e);
    SessionManager::getInstance()->keyPressed(e);

    if(e.key == 'g') {
        drawGui = !drawGui;
    }

    bool controlOrCommand = (e.hasModifier(OF_KEY_CONTROL)||e.hasModifier(OF_KEY_COMMAND));

#ifndef TARGET_OS_MAC
	if (e.key == 's' && controlOrCommand) //with shift
#else
	if (e.key == 's' && controlOrCommand && e.hasModifier(OF_KEY_SHIFT))

#endif
		SessionManager::getInstance()->saveAsNewSession();
	else if (e.keycode == 83 && controlOrCommand)
		if (!SessionManager::getInstance()->isDefaultSession) {
			SessionManager::getInstance()->saveSession();
		}
		else {
			showMessage("Cannot save default session, use \"Save as\" instead");
		}
	else if (e.keycode == 78 && controlOrCommand)
		newSession();
	else if (e.keycode == 79 && controlOrCommand) {
		SessionManager::datasetLoadOptions opts;
		opts.method = "GUI";
		SessionManager::getInstance()->loadSession(opts);
	}
}

void Gui::keyReleased(int key)
{
    auto &io = ImGui::GetIO();
    if ( io.WantTextInput ) {
        return;
    }

    Sounds::getInstance()->keyReleased(key);
}

void Gui::mousePressed(ofVec2f p, int button) {
    //this happens only when a popup is opened but the mouse is not hovering it
    if (isMouseHoveringGUI() && !ImGui::IsAnyWindowHovered()) {
//        if (button != 2) {

            // I think there is no need for these booleans
            // using openPopup like Units.cpp might be enough
            haveToDrawSoundContextMenu = false;
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuFirstOpen = false;
            selectedSlider = nullptr;

            // this feels like a workaround
            // maybe gui should manage input to other modules ??
            // right now ui is managing keyboard input
            Units::getInstance()->mousePressed(p.x, p.y, button);
            UberSlider::clickOutsidePopup();
//        }
    }
}

void Gui::mouseReleased(ofVec2f p, int button)
{
    if (!isMouseHoveringGUI()) {

        if (button == 2 && !Sounds::getInstance()->soundIsBeingMoved) {

            shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getHoveredSound();

            if ( hoveredSound != nullptr ) {
                haveToDrawSoundContextMenu = true;
                if (!isClusterContextMenuFirstOpen) {
                    selectedClusterID = Sounds::getInstance()->getHoveredClusterID();
                }
                selectedSound = hoveredSound;
            }
            else haveToDrawSoundContextMenu = false;


            if (hoveredSound == nullptr && Sounds::getInstance()->getHoveredClusterID() >= 0) {
                haveToDrawClusterContextMenu = true;
                if (!isClusterContextMenuFirstOpen) {
                    selectedClusterID = Sounds::getInstance()->getHoveredClusterID();
                }
            }
            else haveToDrawClusterContextMenu = false;

        }
        else {
            haveToDrawSoundContextMenu = false;
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuFirstOpen = false;
        }
    }
}

bool Gui::isMouseHoveringGUI() {    
    return ImGui::GetIO().WantCaptureMouse;
}
