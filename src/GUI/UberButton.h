#pragma once

#include "ofMain.h"
#include "UberControl.h"
#include "ofxImGui.h"
/**
 * @brief The UberButton class
 *
 * This is the button version of uberControl
 */
class UberButton : virtual public UberControl
{
public:
    UberButton( const std::string& name, const Tooltip::tooltip * tooltip );
    UberButton( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberButton();

    bool small = false;

    void draw();
};
