#include "UberToggle.h"

UberToggle::UberToggle(const string &name, const Tooltip::tooltip *tooltip, const float &v)
    : UberControl (name, v, 0.f, 1.f, tooltip)
{
}

UberToggle::UberToggle(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip)
    : UberControl (parameter, tooltip)
{

}

UberToggle::UberToggle() : UberControl ()
{

}

void UberToggle::setActive(bool active)
{
    if ( active ) {
        set(1.0f);
    } else {
        set(0.f);
    }
}

bool UberToggle::isActive()
{
    return get() >= 1.f;
}

void UberToggle::draw()
{
    float tmpRef = get();
    bool buttonState = false;

    if ( tmpRef >= 1.0f ) {
        ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 1.f);
        ImGui::PushStyleColor(ImGuiCol_Text, textColor);
        ImGui::PushStyleColor(ImGuiCol_Button, color);
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);

        if ( small ) {
            buttonState = ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
        } else {
            buttonState = ImGui::Button( ofxImGui::GetUniqueName(getName()) );
        }

        ImGui::PopStyleColor(4);
    } else {
        if ( small ) {
            buttonState = ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
        } else {
            buttonState = ImGui::Button( ofxImGui::GetUniqueName(getName()) );
        }
    }

    if ( buttonState ) {
        if ( tmpRef >= 1.0f ) {
            tmpRef = 0.f;
        } else {
            tmpRef = 1.f;
        }
    }

    addUberFeatures(buttonState, tmpRef);
}
