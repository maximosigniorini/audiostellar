#include "UberButton.h"

UberButton::UberButton(const string &name, const Tooltip::tooltip *tooltip)
    : UberControl (name, 0.f, 0.f, 1.f, tooltip)
{

}

UberButton::UberButton(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip)
    : UberControl (parameter, tooltip)
{

}

UberButton::UberButton() : UberControl ()
{

}

void UberButton::draw()
{
    float tmpRef = get();
    bool needsUpdate = false;

    if ( small ) {
        ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
    } else {
        ImGui::Button( ofxImGui::GetUniqueName(getName()) );
    }

    if ( ImGui::IsItemHovered() ) {
        if ( ImGui::IsMouseDown(0) && tmpRef == 0.f ) {
            tmpRef = 1.f;
            needsUpdate = true;
        } else if ( ImGui::IsMouseReleased(0) && tmpRef == 1.f ) {
            tmpRef = 0.f;
            needsUpdate = true;
        }
    }

    addUberFeatures(needsUpdate, tmpRef);
}
