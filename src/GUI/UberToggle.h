#pragma once

#include "ofMain.h"
#include "UberControl.h"
#include "ofxImGui.h"
/**
 * @brief The UberButton class
 *
 * This is the button version of uberControl
 */
class UberToggle : virtual public UberControl
{
public:
    UberToggle( const std::string& name, const Tooltip::tooltip * tooltip, const float& v = 0.f );
    UberToggle( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberToggle();

    const ImVec4 COLOR_SOLO = ImVec4(0.919f, 0.814f, 0.240f, 1.0f);
    const ImVec4 COLOR_MUTE = ImVec4(1.0f, 0.f, 0.f, 1.0f);
    const ImVec4 COLOR_DEFAULT = ImVec4(1.0f, 0.f, 0.f, 1.0f);

    bool small = false;
    ImVec4 color = COLOR_DEFAULT;

    void setActive(bool active = true);
    bool isActive();

    void draw();
};
