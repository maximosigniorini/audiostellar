#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Sounds.h"
#include "../Servers/MidiServer.h"
#include "../Units/Units.h"
#include "SessionManager.h"
#include "../ML/AudioDimensionalityReduction.h"
#include "GuiTheme.h"
#include "ofxJSON.h"
#include "Tooltip.h"
#include "../Servers/OscServer.h"
#include "../Servers/MasterClock.h"
#include "../Sound/AudioEngine.h"
#include "../Sound/Voices.h"
#include "../Utils/Tutorial.h"

#define WINDOW_TOOLS_WIDTH 330
#define WINDOW_TOOLS_HEIGHT 600

#define WINDOW_UNITS_WIDTH 350
#define WINDOW_UNITS_HEIGHT 600

class SessionManager;
class MidiServer;

class Gui {

private:
    static Gui* instance;
    Gui();

    bool drawGui = true;

    #ifndef TARGET_OS_MAC
    bool TARGET_OS_MAC = false;
    #endif

    ofxJSONElement jsonFile;

    //this logic could probably be better
    bool isClusterContextMenuFirstOpen = false;

    bool haveToDrawWelcomeScreen = false;
    bool haveToDrawTutorial = false;
    bool haveToDrawAboutScreen = false;
    bool haveToDrawDimReductScreen = false;
    bool haveToDrawAudioSettingsScreen = false;
    bool haveToDrawSoundContextMenu = false;
    bool haveToDrawClusterContextMenu = false;
    bool haveToDrawMessage = false;
    bool haveToDrawCrashMessage = false;
    bool haveToDrawFPS = true;
    bool haveToHideCursor = false;
    bool haveToDrawOscSettingsScreen = false;
    bool haveToDrawMIDILearnScreen = false;
    bool haveToDrawOSCLearnScreen = false;

    // i need to know which parameter need the options displayed
    ofParameter<float> * selectedSlider = nullptr;

    bool toggleFullscreen = false;
    bool isFullscreen = false;
    
    string messageTitle = "";
    string messageDescription = "";
    float messageOpacity = 1.0f;
    bool messageFadeAway = false;
    
    shared_ptr<Sound> selectedSound; //for sound context window
    int selectedClusterID; // initialize
    
    AudioDimensionalityReduction adr;

    const char* sample_rate_items[4] = { "11025", "22050", "44100", "48000" };
    int sample_rate_values[4] = { 11025, 22050, 44100, 48000 };
    int sample_rate_item_current = 1;
    
    const char* window_size_items[5] = { "512", "1024", "2048", "4096", "8192" };
    int window_size_values[5] = { 512, 1024, 2048, 4096, 8192 };
    int window_size_item_current = 2;
    
    const char* hop_size_items[5] = { "Window size ", "Window size / 2", "Window size / 4", "Window size / 8", "Window size / 16" };
    int hop_size_values[5] = { 1, 2, 4, 8, 16 };
    int hop_size_item_current = 2;

    const char* viz_items[3] = { "t-SNE", "UMAP", "PCA" };
    string viz_values[5] = { "tsne", "umap", "pca" };
    int viz_item_current = 0;

    const char* metric_items[5] = { "cosine", "euclidean", "canberra", "cityblock", "braycurtis" };
    string metric_values[5] = { "cosine", "euclidean", "canberra", "cityblock", "braycurtis"  };
    int metric_item_current = 0;

    const string welcomeScreenText =
            "Commander, the default session has been loaded.\n\n"
            "Are you up for a fast tutorial to get you started ?\n\n";

    bool isProcessingFiles = false;

    void drawDimReductScreen();
    void drawProcessingFilesWindow();
    void drawWelcomeScreen();
    void drawMainMenu();
    void drawMainMenuExtra();
    void drawAboutScreen();
    void drawFPS();
    void drawMessage();
    void drawCrashMessage();
    void drawTutorial();
    void drawAudioSettingsScreen();
    void drawContextMenu();
    void drawClusterContextMenu();
    void drawOscSettingsScreen();
    void drawMIDILearnScreen();
    void drawOSCLearnScreen();

    ofTrueTypeFont font;
    Tutorial tutorial;
    Tutorial::TutorialPage * tutorialCurrentPage = nullptr;
    ofTexture imgWelcome;
public:

    static Gui* getInstance();

    ofxImGui::Gui gui;

    void newSession();

    void draw();
    void keyPressed(ofKeyEventArgs & e);
    void keyReleased(int key);
    void mousePressed(ofVec2f p, int button);
    void mouseReleased(ofVec2f p, int button);

    bool isMouseHoveringGUI();

    void showMessage(string description, string title = "Information", bool fadeAway = false);
    void hideMessage();
    void showCrashMessage();

    void showWelcomeScreen();
    void startTutorial();

    bool getShowStats();
    void setShowStats( bool v );
};
