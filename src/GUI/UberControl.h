#pragma once

#include "ofMain.h"
#include "../Utils/Tooltip.h"
/**
 * @brief The UberControl class
 *
 * This class is basically the same as an ofParameter<float> but stores
 * originalName for easier unique id
 * knows how to learn OSC and MIDI
 * knows how to draw itself
 */
class UberControl : public ofParameter<float>
{
protected:
    static UberControl * selectedControl;
    void checkUnknownMappings();
    void addUberFeatures( bool activated, const float &tmpRef );

    const Tooltip::tooltip * tooltip = nullptr;

    float defaultValue = 0.0f;
    bool defaultValueSet = false;
public:
    UberControl( const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip * tooltip );
    UberControl( ofParameter<float>& parameter, const Tooltip::tooltip *tooltip );
    UberControl();
    ~UberControl();

    static ofEvent<UberControl> uberControlIsDead;

    string originalName = "";

    bool hasOSCMapping = false;
    bool hasMIDIMapping = false;

    //this could be pure virtual but
    //abstract classes cannot be instanceted
    virtual void draw() { }

    void setName( const string& name );
    string getName() const;
    virtual UberControl & set( float v );
    virtual const float & get() const;
    float getMin() const;
    float getMax() const;
    void setDefaultValue( float v );

    static void clickOutsidePopup();

    template<class ListenerClass, typename ListenerMethod>
    void addListener( ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP ) {
        ofParameter<float>::addListener(listener, method, prio);
    }

    ofParameter<float> * getParameter();
};
