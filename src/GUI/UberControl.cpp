#include "UberControl.h"
#include "../Servers/OscServer.h"
#include "../Servers/MidiServer.h"
#include "../Utils/SessionManager.h"

UberControl * UberControl::selectedControl = nullptr;
ofEvent<UberControl> UberControl::uberControlIsDead = ofEvent<UberControl>();

void UberControl::checkUnknownMappings()
{
    // when loading a session osc and midi server will have
    // only labels, not the float reference
    if ( OscServer::getInstance()->hasUnknownMappings() ) {
        if ( OscServer::getInstance()->fixUnknownMapping(this) ) {
            OscServer::getInstance()->broadcastUberControlChange(this);
            hasOSCMapping = true;
        }
    }
    if ( MidiServer::getInstance()->hasUnknownMappings() ) {
        if ( MidiServer::getInstance()->fixUnknownMapping(this) ) {
            //TODO :Midi broadcastSliderChange
            hasMIDIMapping = true;
        }
    }
}

UberControl::UberControl(const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip *tooltip)
    : ofParameter<float> (name, v, min, max)
{
    setDefaultValue(v);
    originalName = name;
    this->tooltip = tooltip;
}

UberControl::UberControl(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip) : ofParameter<float>(parameter)
{
    setDefaultValue( parameter.get() );
    originalName = parameter.getName();
    this->tooltip = tooltip;
}

UberControl::UberControl() : ofParameter<float> ()
{

}

UberControl::~UberControl()
{
  ofNotifyEvent(UberControl::uberControlIsDead, *this);
}

void UberControl::setName(const string &name)
{
    ofParameter<float>::setName(name);
    if ( originalName.empty() ) {
        originalName = name;
    }

    checkUnknownMappings();
}

string UberControl::getName() const
{
    return ofParameter<float>::getName();
}

UberControl &UberControl::set(float v)
{
    ofParameter<float>::set(v);
    return *this;
}

const float &UberControl::get() const
{
    return ofParameter<float>::get();
}

float UberControl::getMin() const
{
    return ofParameter<float>::getMin();
}

float UberControl::getMax() const
{
    return ofParameter<float>::getMax();
}

void UberControl::setDefaultValue(float v)
{
    defaultValue = v;
    defaultValueSet = true;
}

void UberControl::clickOutsidePopup()
{
    selectedControl = nullptr;
}

ofParameter<float> *UberControl::getParameter()
{
    return this;
}

void UberControl::addUberFeatures(bool activated, const float &tmpRef)
{
    if ( activated ) {
        if ( defaultValueSet && ofGetKeyPressed(OF_KEY_SHIFT) ) {
            set(defaultValue);
        } else {
            set(tmpRef);
            OscServer::getInstance()->broadcastUberControlChange(this);
            //TODO: MidiServer broadcastSliderChange
        }
    }

    checkUnknownMappings();

    if(ImGui::IsItemHovered() && this->tooltip != nullptr) Tooltip::setTooltip(*this->tooltip);

    if ( ImGui::IsItemHovered() &&
         ImGui::IsMouseReleased(1) &&
         selectedControl == nullptr &&
         !MidiServer::getInstance()->getMIDILearn()) {
         selectedControl = this;
    }
    if ( selectedControl == this ) {
        ImGui::OpenPopup("controlOptions");
        if (ImGui::BeginPopup("controlOptions")) {
            if ( !hasMIDIMapping ) {
                if (ImGui::MenuItem("MIDI Learn")) {
                    if ( MidiServer::getInstance()->getCurrentMidiDevice() != MIDI_NOT_SET ) {
                        MidiServer::getInstance()->setMIDILearnOn( this );
                    } else {
                        Gui::getInstance()->showMessage("No MIDI device is configured. \n\n"
                                                        "Use the Settings menu.", "Error");
                    }
                    selectedControl = nullptr;
                    ImGui::CloseCurrentPopup();
                }
            } else {
                if (ImGui::MenuItem("Remove MIDI mapping")) {
                    MidiServer::getInstance()->removeMapping( this );
                    selectedControl = nullptr;
                    hasMIDIMapping = false;
                    ImGui::CloseCurrentPopup();
                }
            }

            if ( !hasOSCMapping ) {
                if (ImGui::MenuItem("OSC Learn")) {
                    OscServer::getInstance()->setOSCLearnOn( this );
                    selectedControl = nullptr;
                    ImGui::CloseCurrentPopup();
                }
            } else {
                if (ImGui::MenuItem("Remove OSC mapping")) {
                    OscServer::getInstance()->removeMapping( this );
                    selectedControl = nullptr;
                    hasOSCMapping = false;
                    ImGui::CloseCurrentPopup();
                }
            }

            ImGui::EndPopup();
        }
    }
}
