#pragma once

#include "ofMain.h"
#include "UberControl.h"
/**
 * @brief The UberSlider class
 *
 * This is the slider version of uberControl
 */
class UberSlider : virtual public UberControl
{
public:
    UberSlider( const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip * tooltip );
    UberSlider( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberSlider();

    string format = "%.2f";
    bool small = false;

    void draw();
};
