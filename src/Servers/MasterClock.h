#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "../Sound/AudioEngine.h"
#include "../Utils/Utils.h"
#include "../GUI/UI.h"
#include "../Servers/MidiServer.h"

#include "../GUI/UberSlider.h"

class MasterClock
{
private:
    pdsp::Function * pdspFunction = nullptr;
    void initPDSPFunctionClock();
    void destroyPDSPFunctionClock();

    bool useMIDIClock = false;
    bool playing = true;

    UberSlider bpm = { "BPM", 120, 10, 300, &Tooltip::NONE };
//    ofParameter<int> bpm;
    void bpmListener(float &v);

    MasterClock();
    static MasterClock* instance;
public:
    static MasterClock* getInstance();

    ofEvent<int> onTempo;

    void setUseMidiClock(bool v);
    bool getUseMidiClock();

    void onMIDIClockTick( MIDIMessage m );

    void drawGui();

    void play();
    void stop();
    bool isPlaying();

    int getBPM();
    void setBPM( int theBPM );
};
