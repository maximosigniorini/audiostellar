#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

#include "Tooltip.h"
#include "../GUI/UberControl.h"
#include "../Sound/Sound.h"

#define OSC_LOG false

struct oscMapping {
   string oscRoute;
   UberControl * parameter = nullptr;
   string label; //for saving to file
};

class OscServer {
    
private:
    const string ROUTE_UNITS = "units";
    // /units/<index>/volume [float 0-1]
    const string ROUTE_UNITS_VOLUME = "volume";
    // /units/<index>/pan [float 0-1]
    const string ROUTE_UNITS_PAN = "pan";
    const string ROUTE_UNITS_MUTE = "mute";
    const string ROUTE_UNITS_SOLO = "solo";

    const string OSC_GET_NUM_SOUNDS = "/get/soundCount";
    const string OSC_GET_NEIGHBORS_ID = "/get/neighborsByID";
    const string OSC_GET_NEIGHBORS_XY = "/get/neighborsByXY";
    const string OSC_GET_POSITION_ID = "/get/positionByID";

    const string OSC_GET_CLUSTER_ID = "/get/clusterIDByID";
    const string OSC_GET_CLUSTER_NAME = "/get/clusterNameByID";
    const string OSC_GET_CLUSTER_NAME_BY_CLUSTER_INDEX = "/get/clusterNameByClusterID";


    const string OSC_GET_PLAYED_SOUND_ID = "/get/playedSound";
    bool oscGetPlayedSoundIDEnabled = false;

    static int receivePort;
//    static char receiveHost[64];
    string receiveHost;
    static int sendPort;
    static char sendHost[64];
    
    static char hostname[128];

    OscServer();
    static OscServer* instance;

    oscMapping * lastControlMoved = nullptr;
    vector<oscMapping> oscMappings;
    bool unknownMappings = false;

    bool oscLearn = false;
public:
    static OscServer* getInstance();

    ofxOscReceiver oscReceiver;
    static ofxOscSender oscSender;
    
    static ofEvent<ofxOscMessage> oscEvent;
    static bool enable;
    
    void update();
    bool processFixedAddresses(ofxOscMessage &m);
    
    void start();
    void stop();
    
    void drawGui();
    void getInterfaceIP();

    void setOSCLearnOn( UberControl *parameter );
    void setOSCLearnOff();
    bool getOSCLearn();
    void broadcastUberControlChange(UberControl *parameter);
    // when loads from file it will be true
    // then uberslider will take care of this
    bool hasUnknownMappings();
    //if label is in mappings array it will set it
    bool fixUnknownMapping(UberControl *parameter);
    void removeMapping( UberControl *parameter);

    Json::Value save();
    void load(Json::Value jsonData);
    void reset();

    void onUberControlDead( UberControl & deadControl );
    void onPlaySound( Sound & sound );

    void mixerChangedVolume(unsigned int unitIndex, float value);
    void mixerChangedPan(unsigned int unitIndex, float value);
    void mixerChangedMute(unsigned int unitIndex, float value);
    void mixerChangedSolo(unsigned int unitIndex, float value);

    void static sendMessage(ofxOscMessage message);
    void static sendMessage(string address, float value);
    void static sendMessage(string address, int value);
    void static sendMessage(string address, string value);

    string static getSendHost();
    int static getReceivePort();
    int static getSendPort();
    void static setSendHost( string v );
    void static setReceivePort( int v );
    void static setSendPort( int v );


};





