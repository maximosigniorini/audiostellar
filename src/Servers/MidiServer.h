#pragma once

#include "ofMain.h"

#include "ofxImGui.h"
#include "ofxMidi.h"
#include "ofxMidiClock.h"
#include "ofxJSON.h"

#include "../Utils/MIDIMessage.h"
#include "../Units/Units.h"
#include "./MasterClock.h"
#include "../GUI/UberControl.h"

#define MIDI_NOT_SET "Not set"

class Units;

struct midiCCMapping {
   int cc = -1;
   int pitch = -1;
   UberControl * parameter = nullptr;
   string label; //for saving to file
};

class MidiServer: public ofxMidiListener {
private:
    MidiServer();
    static MidiServer* instance;

    ofxMidiIn midiIn;
    ofxMidiClock clock;
    bool useMIDIClock = false;

    vector<string> midiDevices;
    string currentMidiDevice = MIDI_NOT_SET;

    int currentBeat = -1;

    void checkForNewMidiDevices();
    void selectMidiDevice(int idx);
    void newMidiMessage(ofxMidiMessage& eventArgs);

    //MIDI CC
    midiCCMapping * lastControlMoved = nullptr;
    vector<midiCCMapping> ccMappings;
    bool unknownMappings = false;

    bool midiLearn = false;
public:
    static MidiServer* getInstance();
    void init();

    ~MidiServer() {}

    //For handling play/stop from external device
    bool midiClockPlaying = true;
    bool handleMIDIPlayStop = false;

    void setMIDILearnOn(UberControl * parameter);
    void setMIDILearnOff();
    bool getMIDILearn();
    // when loads from file it will be true
    // then uberslider will take care of this
    bool hasUnknownMappings();
    //if label is in mappings array it will set it
    bool fixUnknownMapping(UberControl *parameter);
    void removeMapping( UberControl *parameter);

    Json::Value save();
    void load(Json::Value jsonData);
    void drawGui();
    void reset();

    void onUberControlDead( UberControl & deadControl );

    string getCurrentMidiDevice();
    void setCurrentMidiDevice(string device);
};
