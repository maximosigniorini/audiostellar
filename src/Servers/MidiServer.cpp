#include "MidiServer.h"

MidiServer* MidiServer::instance = nullptr;

MidiServer::MidiServer() {
    midiDevices = midiIn.getInPortList();
    ofAddListener(UberControl::uberControlIsDead, this, &MidiServer::onUberControlDead);
}

void MidiServer::init(){
//    midiIn.setVerbose(true);
    midiIn.addListener(this);
}

void MidiServer::drawGui(){
    ImGui::Text("MIDI Devices");
    if(ImGui::BeginCombo("##MIDI Devices", currentMidiDevice.c_str())){
       checkForNewMidiDevices();

       for(int i = 0; i < midiDevices.size(); i++){

           bool isSelected = (currentMidiDevice == midiDevices[i]);

           if(ImGui::Selectable(midiDevices[i].c_str() )){
               selectMidiDevice(i);
           }

           if(isSelected){
               ImGui::SetItemDefaultFocus();
           }

       }
       ImGui::EndCombo();
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_DEVICES);

    if ( currentMidiDevice != MIDI_NOT_SET ) {
        if ( ImGui::Checkbox("MIDI Clock", &useMIDIClock) ) {
            MasterClock::getInstance()->setUseMidiClock(useMIDIClock);
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_CLOCK);

        if ( useMIDIClock ) {
            ImGui::Checkbox("Use external Play/Stop", &handleMIDIPlayStop);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_HANDLE_PLAY_STOP);
        }
    }

    ImGui::Separator();
}

void MidiServer::setMIDILearnOn(UberControl *parameter)
{
    midiLearn = true;
    lastControlMoved = new midiCCMapping();
    lastControlMoved->parameter = parameter;
}

void MidiServer::setMIDILearnOff()
{
    midiLearn = false;
    if ( lastControlMoved != nullptr ) {
        delete lastControlMoved; //should i delete it??
        lastControlMoved = nullptr;
    }
}

bool MidiServer::getMIDILearn()
{
    return midiLearn;
}

bool MidiServer::hasUnknownMappings()
{
    if ( unknownMappings ) {
        for(unsigned int i = 0; i < ccMappings.size(); i++){
            if(ccMappings[i].parameter == nullptr ) {
                return true;
            }
        }
    }
    unknownMappings = false;
    return false;
}

bool MidiServer::fixUnknownMapping(UberControl *parameter)
{
    for(unsigned int i = 0; i < ccMappings.size(); i++){
        if(ccMappings[i].label == parameter->getName()) {
            ccMappings[i].parameter = parameter;
            return true;
        }
    }
    return false;
}

void MidiServer::removeMapping(UberControl *parameter)
{
    for(unsigned int i = 0; i < ccMappings.size(); i++) {
        if ( ccMappings[i].parameter == parameter ) {
            ccMappings.erase( ccMappings.begin() + i );
        }
    }
}

void MidiServer::checkForNewMidiDevices(){
    midiDevices = midiIn.getInPortList();
}

void MidiServer::selectMidiDevice(int idx) {
    currentMidiDevice = midiDevices[idx];
    //primero cerrá el puerto del device anterior
    midiIn.closePort();
    //abrí el nuevo puerto
    midiIn.openPort(currentMidiDevice);
    midiIn.ignoreTypes(true,false,true); //no ignores los de clock
}


void MidiServer::newMidiMessage(ofxMidiMessage& msg) {
    MIDIMessage m;

    m.status = msg.status;
    m.pitch =  msg.pitch;
    m.cc = msg.control;
    m.ccVal = msg.value;
    m.beats = -1;
    m.bpm = -1;

    // is this ever called ? (not from Bitwig)
    if ( m.status == MIDI_START ) {
        midiClockPlaying = true;
        clock.reset();
    }
    if ( m.status == MIDI_STOP ) {
        midiClockPlaying = false;
    }
    if ( m.status == MIDI_CONTINUE ) {
        midiClockPlaying = true;

        if ( handleMIDIPlayStop ) {
            m.beats = currentBeat;
            MasterClock::getInstance()->onMIDIClockTick(m);
        }
    }

    if(clock.update(msg.bytes)) {
        unsigned int beats = clock.getBeats();

        if ( beats != currentBeat ) {
            currentBeat = beats;
            m.beats = currentBeat;

            if ( !handleMIDIPlayStop || midiClockPlaying ) {
                MasterClock::getInstance()->onMIDIClockTick(m);
            }
        }

        return;
    }

    if(msg.status == MIDI_CONTROL_CHANGE ||
       msg.status == MIDI_PITCH_BEND ||
       msg.status == MIDI_NOTE_ON ||
       msg.status == MIDI_NOTE_OFF ) {

        //Nasty workaroud, PitchBend as a CC message
        if(msg.status == MIDI_PITCH_BEND){
            m.cc = 128;
        }

        if(midiLearn){
            if(lastControlMoved != nullptr){
                if ( msg.status == MIDI_CONTROL_CHANGE || msg.status == MIDI_PITCH_BEND ) {
                    lastControlMoved->cc = m.cc;
                } else {
                    lastControlMoved->pitch = m.pitch;
                }
                ccMappings.push_back(*lastControlMoved);
                lastControlMoved->parameter->hasMIDIMapping = true;

                midiLearn = !midiLearn;
                lastControlMoved = nullptr;
            }
        } else {
            for(unsigned int i = 0; i < ccMappings.size(); i++){
                if ( msg.status == MIDI_CONTROL_CHANGE || msg.status == MIDI_PITCH_BEND ) {
                    if(ccMappings[i].cc == m.cc) {
                        if ( ccMappings[i].parameter != nullptr ) {
                            int maxInputVal = (msg.status == MIDI_PITCH_BEND) ? 16383 : 127;
                            float middleValue = ccMappings[i].parameter->getMax() + ccMappings[i].parameter->getMin();

                            // This is mainly for ParticleUnit velocity
                            // If the parameter is centered on 0, then 63 and 64 will map to 0
                            // we lose a little resolution but ensure the center is fine
                            // for using it with joysticks
                            if ( middleValue == 0 &&
                                 (m.ccVal == floor(maxInputVal/2.0f) ||
                                  m.ccVal == ceil(maxInputVal/2.0f) ) ) {
                                    ccMappings[i].parameter->set( middleValue );
                            } else {
                                float setValue = ofMap(m.ccVal,
                                                       0,
                                                       maxInputVal,
                                                       ccMappings[i].parameter->getMin(),
                                                       ccMappings[i].parameter->getMax(),
                                                       true);

                                ccMappings[i].parameter->set( setValue );
                            }

                        } else {
                            ofLogError("MIDI Server", "Parameter is null");
                        }
                    }
                } else {
                    if(ccMappings[i].pitch == m.pitch) {
                        if ( ccMappings[i].parameter != nullptr ) {
                            int val = msg.status == MIDI_NOTE_ON ? 127 : 0;
                            ccMappings[i].parameter->set( ofMap(val,
                                                                0,
                                                                127,
                                                                ccMappings[i].parameter->getMin(),
                                                                ccMappings[i].parameter->getMax(),
                                                                true));
                        } else {
                            ofLogError("MIDI Server", "Parameter is null");
                        }
                    }
                }
            }
        }
    } else {
        Units::getInstance()->midiMessage(m);
    }

}

MidiServer *MidiServer::getInstance()
{
    if ( instance == nullptr ) {
        instance = new MidiServer();
    }

    return instance;
}

Json::Value MidiServer::save(){
   Json::Value root = Json::Value(Json::arrayValue);
   for(unsigned int i = 0; i < ccMappings.size(); i++) {
       if ( ccMappings[i].parameter == nullptr ) {
           ofLog() << "MIDI parameter is null, is this a bug?";
           continue;
       }

       Json::Value val = Json::Value(Json::objectValue);
       val["cc"] = ccMappings[i].cc;
       val["pitch"] = ccMappings[i].pitch;
       val["label"] = ccMappings[i].parameter->getName();

       root.append(val);
   }
   return root;
}

void MidiServer::load(Json::Value jsonData){
    if(jsonData != Json::nullValue){
        for(unsigned int i = 0; i < jsonData.size(); i++) {
           Json::Value c = jsonData[i];
           midiCCMapping mapping;
           mapping.label = c["label"].asString();
           mapping.cc = c["cc"].asInt();
           mapping.pitch = c["pitch"].asInt();

           ofStringReplace(mapping.label, "\"", "");
           ofStringReplace(mapping.label, "\n", "");
           //ofStringReplace(mapping.label, " ", "");

           ccMappings.push_back(mapping);
        }
        if ( jsonData.size() > 0 ) unknownMappings = true;
    }
}

void MidiServer::reset(){
    ccMappings.clear();
}

void MidiServer::onUberControlDead(UberControl &deadControl)
{
    for(unsigned int i = 0; i < ccMappings.size(); i++) {
        if ( ccMappings[i].parameter == &deadControl ) {
            ccMappings.erase( ccMappings.begin() + i );
        }
    }
}

string MidiServer::getCurrentMidiDevice(){
  return currentMidiDevice;
}

void MidiServer::setCurrentMidiDevice(string device){
    int port = -1;

    for(unsigned int i = 0; i < midiIn.getNumInPorts(); ++i) {
        string name = midiIn.getInPortName(i);
        if(name == device) {
            port = i;
            break;
        }
    }

    if(port == -1) {
        ofLogNotice("WARNING", "The last midi device used is not connected");
    } else {
        midiIn.closePort();
        midiIn.openPort(device);
        midiIn.ignoreTypes(true,false,true); //no ignores los de clock
        currentMidiDevice = device;
    }
}
