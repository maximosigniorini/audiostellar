#include "OscServer.h"
#include "../Units/Units.h"

ofEvent<ofxOscMessage> OscServer::oscEvent = ofEvent<ofxOscMessage>();
bool OscServer::enable = false;
ofxOscSender OscServer::oscSender;

int OscServer::receivePort = 8000;
//char OscServer::receiveHost[64] = "127.0.0.1";
int OscServer::sendPort = 9000;
char OscServer::sendHost[64] = "127.0.0.1";
char OscServer::hostname[128];

OscServer* OscServer::instance = nullptr;

OscServer::OscServer() {
    getInterfaceIP();

    ofAddListener(UberControl::uberControlIsDead, this, &OscServer::onUberControlDead);
    ofAddListener(Unit::eventPlayedSound, this, &OscServer::onPlaySound);
}

OscServer *OscServer::getInstance()
{
    if ( instance == nullptr ) {
        instance = new OscServer();
    }
    return instance;
}


void OscServer::update() {
    if (enable) {
        if (oscReceiver.hasWaitingMessages()) {
            ofxOscMessage m;

            while(oscReceiver.hasWaitingMessages()) {
                oscReceiver.getNextMessage(m);
//                ofLog() << "OSC: " << m.getAddress();
                if ( processFixedAddresses(m) ) return;
                ofNotifyEvent(oscEvent, m, this);

                if ( oscLearn ) {
                    if ( lastControlMoved != nullptr ) {
                        lastControlMoved->oscRoute = m.getAddress();
                        oscMappings.push_back(*lastControlMoved);
                        lastControlMoved->parameter->hasOSCMapping = true;
                        setOSCLearnOff();
                    }
                } else {
                    for(unsigned int i = 0; i < oscMappings.size(); i++){
                        if(oscMappings[i].oscRoute == m.getAddress()) {
                            if ( m.getNumArgs() < 1 ) continue;
                            //should check for argtype ?

                            if ( oscMappings[i].parameter != nullptr ) {
                                oscMappings[i].parameter->set(
                                            ofMap(m.getArgAsFloat(0),
                                                  0.f,
                                                  1.f,
                                                  oscMappings[i].parameter->getMin(),
                                                  oscMappings[i].parameter->getMax(),
                                                  true));
                            } else {
                                ofLogError("OSC Server", "Parameter is null");
                            }

                        }
                    }
                }
            }
        }
    }
}

bool OscServer::processFixedAddresses(ofxOscMessage &m)
{
//    TidalOscListener::getInstance()->onOscMsg(m);

    vector<string> vecAddress = ofSplitString(m.getAddress(),"/", true, true);

    // Mixer

    if ( vecAddress.size() == 3 ) {
        /*
         * /units/<index>/volume [float 0-1]
         * /units/<index>/pan [float 0-1]
         * /units/<index>/mute [float 0-1]
         * /units/<index>/solo [float 0-1]
         */
        if ( vecAddress[0] == ROUTE_UNITS ) {
            if ( isdigit(vecAddress[1][0]) ) {
                int index = ofToInt(vecAddress[1]);
                if ( m.getNumArgs() == 1 ) {
                    // We should definitely implement setUnitPan, Mute, etc
                    // this is looks hacky and low level
                    if ( vecAddress[2] == ROUTE_UNITS_VOLUME ) {
                        float val = m.getArgAsFloat(0);
                        Units::getInstance()->setUnitVolume(index, val);
                        return true;
                    } else if ( vecAddress[2] == ROUTE_UNITS_PAN ) {
                        float val = m.getArgAsFloat(0);
                        UberSlider * slider = Units::getInstance()->getUnitPan(index);
                        if ( slider != nullptr ) {
                            slider->set( ofClamp(val, 0.f, 1.f) );
                            return true;
                        }
                    } else if ( vecAddress[2] == ROUTE_UNITS_MUTE ) {
                        float val = m.getArgAsFloat(0);
                        UberToggle * toggle = Units::getInstance()->getUnitMute(index);
                        if ( toggle != nullptr ) {
                            toggle->set( ofClamp(val, 0.f, 1.f) );
                            return true;
                        }
                    } else if ( vecAddress[2] == ROUTE_UNITS_SOLO ) {
                        float val = m.getArgAsFloat(0);
                        UberToggle * toggle = Units::getInstance()->getUnitSolo(index);
                        if ( toggle != nullptr ) {
                            toggle->set( ofClamp(val, 0.f, 1.f) );
                            return true;
                        }
                    }
                }
            }
        }
    }

    // API starts

    /////////
    // Get sound count
    /////////

    if ( m.getAddress() == OSC_GET_NUM_SOUNDS ) {
        OscServer::sendMessage( OSC_GET_NUM_SOUNDS,
                                static_cast<int>(Sounds::getInstance()->getSoundCount()) );
        return true;
    }

    /////////
    // Get position by ID
    /////////

    else if ( m.getAddress() == OSC_GET_POSITION_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            shared_ptr<Sound> s = Sounds::getInstance()->getSoundById( m.getArgAsInt(0) );
            ofxOscMessage msg;

            msg.setAddress( m.getAddress() );

            if ( s != nullptr ) {
                // between 0 and 1
                msg.addFloatArg( s->getPosition().x / Sounds::getInstance()->getInitialWindowWidth() );
                msg.addFloatArg( s->getPosition().y / Sounds::getInstance()->getInitialWindowHeight() );
            } else {
                msg.addStringArg("ID not found");
            }
            OscServer::sendMessage( msg );
        }
        return true;
    }

    /////////
    // Get neighbors
    /////////

    else if ( m.getAddress() == OSC_GET_NEIGHBORS_ID ) {
        if ( m.getNumArgs() >= 2 ) {
            shared_ptr<Sound> s = Sounds::getInstance()->getSoundById( m.getArgAsInt(0) );
            ofxOscMessage msg;
            msg.setAddress( OSC_GET_NEIGHBORS_ID );

            vector<shared_ptr<Sound>> neighbors;

            if ( s != nullptr ) {
                //threshold between 0 and 1, square distance is cheaper than regular
                double threshold = pow(m.getArgAsFloat(1) *
                                      Sounds::getInstance()->getInitialWindowWidth(),2);
                neighbors = Sounds::getInstance()->getNeighbors(s, static_cast<float>(threshold));

                for ( unsigned int i = 0 ; i < neighbors.size() ; i++ ) {
                    msg.addIntArg( neighbors[i]->id );
                }
            } else {
                msg.addStringArg("ID not found");
            }

            OscServer::sendMessage( msg );
        } else {
            ofLog() << "Not enough arguments for OSC: " << m.getAddress();
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }

        return true;
    }
    else if ( m.getAddress() == OSC_GET_NEIGHBORS_XY ) {
        if ( m.getNumArgs() >= 3 ) {
            ofxOscMessage msg;
            msg.setAddress( OSC_GET_NEIGHBORS_XY );

            ofVec2f pos;
            pos.x = m.getArgAsFloat(0) * Sounds::getInstance()->getInitialWindowWidth();
            pos.y = m.getArgAsFloat(1) * Sounds::getInstance()->getInitialWindowHeight();

            vector<shared_ptr<Sound>> neighbors;

            //threshold between 0 and 1, square distance is cheaper than regular
            double threshold = pow(m.getArgAsFloat(2) * Sounds::getInstance()->getInitialWindowWidth(),2);
            neighbors = Sounds::getInstance()->getNeighbors( pos , static_cast<float>(threshold) );

            for ( unsigned int i = 0 ; i < neighbors.size() ; i++ ) {
                msg.addIntArg( neighbors[i]->id );
            }

            OscServer::sendMessage( msg );
        } else {
            ofLog() << "Not enough arguments for OSC: " << m.getAddress();
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }

        return true;
    }

    /////////
    // Enable Get played sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_PLAYED_SOUND_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            oscGetPlayedSoundIDEnabled = m.getArgAsInt(0);
        }
        return true;
    }


    /////////
    // Get cluster ID by sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_ID ) {
        if ( m.getNumArgs() >= 1 ) {
            int soundID = m.getArgAsInt(0);

            shared_ptr<Sound> s = Sounds::getInstance()->getSoundById(soundID);
            if ( s != nullptr ) {
                OscServer::sendMessage( m.getAddress(), s->getCluster() );
            } else {
                OscServer::sendMessage( m.getAddress(), "Sound ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }

        return true;
    }

    /////////
    // Enable Get cluster name by sound ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_NAME ) {
        if ( m.getNumArgs() >= 1 ) {
            int soundID = m.getArgAsInt(0);

            shared_ptr<Sound> s = Sounds::getInstance()->getSoundById(soundID);
            if ( s != nullptr ) {
                int clusterID = s->getCluster();
                if ( clusterID > 0 && clusterID < Sounds::getInstance()->clusterNames.size() ) {
                    OscServer::sendMessage( m.getAddress(), Sounds::getInstance()->clusterNames[clusterID] );
                } else {
                    OscServer::sendMessage( m.getAddress(), "Cluster ID not found" );
                }
            } else {
                OscServer::sendMessage( m.getAddress(), "Sound ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }

        return true;
    }

    /////////
    // Enable Get cluster name by cluster ID
    /////////

    else if ( m.getAddress() == OSC_GET_CLUSTER_NAME_BY_CLUSTER_INDEX ) {
        if ( m.getNumArgs() >= 1 ) {
            int clusterID = m.getArgAsInt(0);
            if ( clusterID > 0 && clusterID < Sounds::getInstance()->clusterNames.size() ) {
                OscServer::sendMessage( m.getAddress(), Sounds::getInstance()->clusterNames[clusterID] );
            } else {
                OscServer::sendMessage( m.getAddress(), "Cluster ID not found" );
            }
        } else {
            OscServer::sendMessage( m.getAddress(), "Not enough arguments" );
        }

        return true;
    }


    return false;
}

void OscServer::start() {

    oscReceiver.setup(receivePort);
    oscSender.setup(sendHost, sendPort);
    enable = true;
}

void OscServer::stop() {

    oscReceiver.stop();
    oscSender.clear();
    enable = false;
}

void OscServer::drawGui() {
    
    if(ImGui::Checkbox("Enable", &enable)) enable ? start() : stop();
    
    ImGui::InputInt("Receive Port", &receivePort);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscReceiver.setup(receivePort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_PORT);
    
    ImGui::Text("%s", receiveHost.c_str());
//    ImGui::InputText("Receive address", receiveHost, 64, ImGuiInputTextFlags_ReadOnly);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_ADDRESS);
    
    ImGui::NewLine();
    
    ImGui::InputInt("Send Port", &sendPort);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscSender.setup(sendHost, sendPort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_SEND_PORT);
    
    ImGui::InputText("Send address", sendHost, 64);
    if(ImGui::IsItemDeactivatedAfterEdit()) oscSender.setup(sendHost, sendPort);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_SEND_ADDRESS);

}

void OscServer::getInterfaceIP() {
    vector<string> result;

    #ifdef TARGET_WIN32

        string commandResult = ofSystem("ipconfig");
        //ofLogVerbose() << commandResult;

        for (int pos = 0; pos >= 0; )
        {
            pos = commandResult.find("IPv4", pos);

            if (pos >= 0)
            {
                pos = commandResult.find(":", pos) + 2;
                int pos2 = commandResult.find("\n", pos);

                string ip = commandResult.substr(pos, pos2 - pos);

                pos = pos2;

                if (ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif
    #ifdef TARGET_LINUX
        string commandResult = ofSystem("ip addr");

        for(int pos = 0; pos >= 0; )
        {
           pos = commandResult.find("inet ", pos);

            if(pos >= 0)
            {
                int pos2 = commandResult.find("brd", pos);

                string ip = commandResult.substr(pos+5, pos2-pos-9);

                pos = pos2;

                if(ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif
    #ifdef TARGET_OSX
        string commandResult = ofSystem("ifconfig");

        for(int pos = 0; pos >= 0; )
        {
           pos = commandResult.find("inet ", pos);

            if(pos >= 0)
            {
                int pos2 = commandResult.find("netmask", pos);

                string ip = commandResult.substr(pos+5, pos2-pos-6);

                pos = pos2;

                if(ip.substr(0, 3) != "127") // let's skip loopback addresses
                {
                    result.push_back(ip);
                    //ofLogVerbose() << ip;
                }
            }
        }

    #endif

    if ( result.size() > 0 ) {
        receiveHost = "IP: 127.0.0.1 or " + result[0];
    } else {
        receiveHost = "IP: 127.0.0.1";
    }
}


void OscServer::setOSCLearnOn(UberControl *parameter)
{
    oscLearn = true;
    lastControlMoved = new oscMapping();
    lastControlMoved->parameter = parameter;
}

void OscServer::setOSCLearnOff()
{
    oscLearn = false;
    if ( lastControlMoved != nullptr ) {
        delete lastControlMoved;
        lastControlMoved = nullptr;
    }
}

bool OscServer::getOSCLearn()
{
    return oscLearn;
}

void OscServer::broadcastUberControlChange(UberControl *parameter)
{
    if ( !enable ) { return; }

    for(unsigned int i = 0; i < oscMappings.size(); i++){
        if(oscMappings[i].parameter == parameter ) {
            float v = ofMap(parameter->get(), parameter->getMin(), parameter->getMax(),0,1);
            sendMessage( oscMappings[i].oscRoute, v);
            break;
        }
    }
}

bool OscServer::hasUnknownMappings()
{
    if ( unknownMappings ) {
        for(unsigned int i = 0; i < oscMappings.size(); i++){
            if(oscMappings[i].parameter == nullptr ) {
                return true;
            }
        }
    }
    unknownMappings = false;
    return false;
}

bool OscServer::fixUnknownMapping(UberControl  *parameter)
{
    for(unsigned int i = 0; i < oscMappings.size(); i++){
        if(oscMappings[i].label == parameter->getName()) {
            oscMappings[i].parameter = parameter;
            return true;
        }
    }
    return false;
}

void OscServer::removeMapping(UberControl  *parameter)
{
    for(unsigned int i = 0; i < oscMappings.size(); i++) {
        if ( oscMappings[i].parameter == parameter ) {
            oscMappings.erase( oscMappings.begin() + i );
        }
    }
}

Json::Value OscServer::save()
{
    Json::Value root = Json::Value(Json::arrayValue);
    for(unsigned int i = 0; i < oscMappings.size(); i++) {
        if ( oscMappings[i].parameter == nullptr ) {
            ofLog() << "OSC parameter is null, is this a bug?";
            continue;
        }

        Json::Value val = Json::Value(Json::objectValue);
        val["oscRoute"] = oscMappings[i].oscRoute;
        val["label"] = oscMappings[i].parameter->getName();

        root.append(val);
    }
    return root;
}

void OscServer::load(Json::Value jsonData)
{
    if(jsonData != Json::nullValue){
        for(unsigned int i = 0; i < jsonData.size(); i++) {
           Json::Value c = jsonData[i];
           oscMapping mapping;
           mapping.label = c["label"].asString();
           mapping.oscRoute = c["oscRoute"].asString();

           ofStringReplace(mapping.label, "\"", "");
           ofStringReplace(mapping.label, "\n", "");
           //ofStringReplace(mapping.label, " ", "");

           oscMappings.push_back(mapping);
        }
        if ( jsonData.size() > 0 ) unknownMappings = true;
    }
}

void OscServer::reset()
{
    oscMappings.clear();
}

void OscServer::onUberControlDead(UberControl &deadControl)
{
    for(unsigned int i = 0; i < oscMappings.size(); i++) {
        if ( oscMappings[i].parameter == &deadControl ) {
            oscMappings.erase( oscMappings.begin() + i );
        }
    }
}

void OscServer::onPlaySound(Sound &sound)
{
    if ( oscGetPlayedSoundIDEnabled ) {
        ofxOscMessage message;

        message.addIntArg(sound.id);
        message.addIntArg(sound.getCluster());
        message.addFloatArg(sound.getPosition().x / Sounds::getInstance()->getInitialWindowWidth());
        message.addFloatArg(sound.getPosition().y / Sounds::getInstance()->getInitialWindowHeight());

        message.setAddress(OSC_GET_PLAYED_SOUND_ID);

        sendMessage(message);
    }
}

void OscServer::mixerChangedVolume(unsigned int unitIndex, float value)
{
    const string address = "/" + ROUTE_UNITS + "/" + ofToString(unitIndex) + "/volume";
    sendMessage(address, ofMap(value, VOLUME_MIN, VOLUME_MAX, 0, 1 ) );
}

void OscServer::mixerChangedPan(unsigned int unitIndex, float value)
{
    const string address = "/" + ROUTE_UNITS + "/" + ofToString(unitIndex) + "/pan";
    sendMessage(address, value);
}

void OscServer::mixerChangedMute(unsigned int unitIndex, float value)
{
    const string address = "/" + ROUTE_UNITS + "/" + ofToString(unitIndex) + "/mute";
    sendMessage(address, value);
}

void OscServer::mixerChangedSolo(unsigned int unitIndex, float value)
{
    const string address = "/" + ROUTE_UNITS + "/" + ofToString(unitIndex) + "/solo";
    sendMessage(address, value);
}

void OscServer::sendMessage(ofxOscMessage message)
{
    if ( enable ) {
        #if OSC_LOG
        string arguments = "";
        for ( unsigned int i = 0 ; i < message.getNumArgs() ; i++ ) {
            arguments += message.getArgAsString(i) + " ";
        }
        ofLog() << "Sending message: " << message.getAddress() << " " << arguments;
        #endif
        oscSender.sendMessage(message);
    }
}
void OscServer::sendMessage(string address, float value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addFloatArg(value);
    sendMessage(message);
}
void OscServer::sendMessage(string address, int value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addIntArg(value);
    sendMessage(message);
}
void OscServer::sendMessage(string address, string value)
{
    ofxOscMessage message;
    message.setAddress(address);
    message.addStringArg(value);
    sendMessage(message);
}


string OscServer::getSendHost()
{
    return sendHost;
}

int OscServer::getReceivePort()
{
    return receivePort;
}

int OscServer::getSendPort()
{
    return sendPort;
}

void OscServer::setSendHost(string v)
{
    strcpy(sendHost, v.c_str());
}

void OscServer::setReceivePort(int v)
{
    receivePort = v;
}

void OscServer::setSendPort(int v)
{
    sendPort = v;
}
