#include "MasterClock.h"

MasterClock* MasterClock::instance = nullptr;

MasterClock::MasterClock()
{
    bpm.addListener(this, &MasterClock::bpmListener);
    bpm.format = "%.0f";

    initPDSPFunctionClock();
}

MasterClock *MasterClock::getInstance()
{
    if ( instance == nullptr ) {
        instance = new MasterClock();
    }
    return instance;
}

void MasterClock::setUseMidiClock(bool v)
{
    useMIDIClock = v;

    if ( useMIDIClock ) {
        destroyPDSPFunctionClock();
    } else {
        initPDSPFunctionClock();
    }
}

bool MasterClock::getUseMidiClock()
{
    return useMIDIClock;
}

void MasterClock::onMIDIClockTick(MIDIMessage m)
{
    if (!useMIDIClock) return;
    if (!playing) return;
    ofNotifyEvent(onTempo, m.beats);
}

void MasterClock::initPDSPFunctionClock()
{
    pdspFunction = new pdsp::Function();
    // this assignable function is executed each 16th
    pdspFunction->code = [&]() noexcept {
        int frame = pdspFunction->frame();
        if ( playing ) {
            ofNotifyEvent(onTempo, frame);
        }
    };
}

void MasterClock::destroyPDSPFunctionClock()
{
    if ( pdspFunction != nullptr ) {
        delete pdspFunction;
        pdspFunction = nullptr;
    }
}

void MasterClock::drawGui()
{
    ImGui::PushItemWidth( 100.f );
    if ( !useMIDIClock ) {
        bpm.draw();
    } else {
        ImGui::Button("MIDI Clock");
    }
}

void MasterClock::play()
{
    playing = true;
}

void MasterClock::stop()
{
    playing = false;
}

bool MasterClock::isPlaying()
{
    return playing;
}

int MasterClock::getBPM()
{
    return round(bpm);
}

void MasterClock::setBPM( int theBPM )
{
    bpm.set(theBPM);
}

void MasterClock::bpmListener(float &v) {
    AudioEngine::getInstance()->engine.sequencer.setTempo(round(v));
}
